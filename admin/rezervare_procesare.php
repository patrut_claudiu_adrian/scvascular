<?php
session_start();

include('checklogin.php');

if (!$logged_in) {

    header('Location: index.php');

    exit();

}

$titlu = mysqli_real_escape_string($conn, $_POST['nume']);
$subtitlu = mysqli_real_escape_string($conn, $_POST['nume1']);
$continut = $_POST['editor1'];
if (!$titlu || !$continut) {
    $_SESSION['err'] = alert('Check all required fields!', 2);
    header('Location: oferta_noua.php');
    exit();
}
$target_path = 'upload/';

$target_path .= basename($_FILES['file_upload']['name']);

move_uploaded_file($_FILES['file_upload']['tmp_name'], $target_path);
$name = basename($_FILES['file_upload']['name']);
$data = time();

$q = "INSERT INTO  `oferte` (`id` ,`titlu` ,`subtitlu` ,`continut` ,`poza` ,`data` ,`activ`, `autor`)
VALUES (NULL ,  '$titlu', '$subtitlu', '$continut',  '$name',  '$data',  '1', '')";
$resq = mysqli_query($conn, $q);

$_SESSION['err'] = alert('The article was saved successfully', 1);
header('Location: oferta_noua.php');
