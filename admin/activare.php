<?php
session_start();

include('checklogin.php');

if (!$logged_in) {

    header('Location: index.php');

    exit();

}

$key_rez = mysqli_real_escape_string($conn, $_GET['key']);

$q = "SELECT `id` FROM `oferte` WHERE `id`='$key_rez' LIMIT 1";
$resq = mysqli_query($conn, $q);
$numrowq = mysqli_num_rows($resq);
if ($numrowq !== 1) {

    $_SESSION['err'] = alert('Select at least one article!', 4);
    header('Location: home.php');
    exit();

}

$key = mysqli_real_escape_string($conn, $_GET['key']);
$activx = mysqli_real_escape_string($conn, $_GET['activ']);

if ((int)$activx === 0) {
    $q = "UPDATE `oferte` SET `activ`='1' WHERE `id`='$key'";
    $res = mysqli_query($conn, $q);
    $_SESSION['err'] = alert('Article is now enabled', 4);
    header('Location: home.php');
    exit();
}
if ((int)$activx === 1) {
    $q1 = "UPDATE `oferte` SET `activ`='0' WHERE `id`='$key'";
    $resz = mysqli_query($conn, $q1);

    $_SESSION['err'] = alert('Article is now disabled', 4);
    header('Location: home.php');
    exit();
}

