<?php
session_start();
include('checklogin.php');

if (!$logged_in) {
    header('Location: index.php');
    exit();
}

$activ = 1;
$q = 'SELECT * FROM `oferte` ';

$res_rez = mysqli_query($conn, $q);

?>
<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">

    <title><?php echo $page_title_general; ?> </title>

    <meta name="description" content="">
    <!-- Google Font and style definitions -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
    <link rel="stylesheet" href="css/style.css">

    <!-- include the skins (change to dark if you like) -->
    <link rel="stylesheet" href="css/light/theme.css" id="themestyle">

    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

    <!-- Apple iOS and Android stuff - don't remove! -->
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

    <!-- Use Google CDN for jQuery and jQuery UI -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

    <!-- Loading JS Files this way is not recommended! Merge them but keep their order -->

    <!-- some basic functions -->
    <script src="js/functions.js"></script>

    <!-- all Third Party Plugins -->
    <script src="js/plugins.js"></script>
    <script src="js/editor.js"></script>
    <script src="js/calendar.js"></script>
    <script src="js/flot.js"></script>
    <script src="js/elfinder.js"></script>
    <script src="js/datatables.js"></script>

    <!-- all Whitelabel Plugins -->
    <script src="js/wl_Alert.js"></script>
    <script src="js/wl_Autocomplete.js"></script>
    <script src="js/wl_Breadcrumb.js"></script>
    <script src="js/wl_Calendar.js"></script>
    <script src="js/wl_Chart.js"></script>
    <script src="js/wl_Color.js"></script>
    <script src="js/wl_Date.js"></script>
    <script src="js/wl_Editor.js"></script>
    <script src="js/wl_File.js"></script>
    <script src="js/wl_Dialog.js"></script>
    <script src="js/wl_Fileexplorer.js"></script>
    <script src="js/wl_Form.js"></script>
    <script src="js/wl_Gallery.js"></script>
    <script src="js/wl_Number.js"></script>
    <script src="js/wl_Password.js"></script>
    <script src="js/wl_Slider.js"></script>
    <script src="js/wl_Store.js"></script>
    <script src="js/wl_Time.js"></script>
    <script src="js/wl_Valid.js"></script>
    <script src="js/wl_Widget.js"></script>

    <!-- configuration to overwrite settings -->
    <script src="js/config.js"></script>

    <!-- the script which handles all the access to plugins etc... -->
    <script src="js/script.js"></script>
</head>

<body> <?php include("header_meniu.php"); ?>

<div class="clearfix"></div>
<?php include('header.php'); ?>
<?php include('meniu.php'); ?>

<section id="content">
    <div class="g12 nodrop">
        <?php if (isset($_SESSION['err'])) {
            echo $_SESSION['err'];
            unset($_SESSION['err']);
        }
        ?>
        <h2>Articole - ImPULS</h2>
        <hr>
        <table class="datatable">
            <thead>
            <tr>
                <th>Titlu</th>
                <th>Data</th>
                <th>Status</th>
                <th>Optiuni</th>
            </tr>
            </thead>
            <tbody>
            <?php for ($i = 0, $iMax = mysqli_num_rows($res_rez); $i < $iMax; $i++) {

                $rows = mysqli_fetch_array($res_rez) ?>
                <tr class="odd gradeX">
                    <td><a style="color:#000;" href="editare.php?key=<?php echo htmlspecialchars($rows['id']); ?>"><?php echo $rows['titlu']; ?><br><?php echo $rows['subtitlu']; ?></a>
                    </td>

                    <td><?php echo date('d-m-Y', $rows['data']); ?></td>
                    <td><?php if ($rows['activ'] === 1) {
                            echo 'Activ';
                        } else {
                            echo 'Neafisat';
                        } ?></td>
                    <td>
                        <?php if ($rows['activ'] === 0) { ?>
                            <a href="activare.php?key=<?php echo htmlspecialchars($rows['id']); ?>&activ=0"><img title="Enable" src="css/images/icons/dark/cross.png"></a>
                        <?php } else { ?> <a href="activare.php?key=<?php echo htmlspecialchars($rows['id']); ?>&activ=1"><img title="Disable" src="css/images/icons/dark/tick.png">
                            </a><?php } ?>

                        <a onClick="return confirm('Are you sure you want to permanently delete this article?');"
                           href="delete.php?key=<?php echo htmlspecialchars($rows['id']); ?>"><img title="Delete" src="css/images/icons/dark/trashcan.png"></a>

                        <a href="editare.php?key=<?php echo htmlspecialchars($rows['id']); ?>"><img title="Edit" src="css/images/icons/dark/pencil.png"></a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>

        </table>


    </div>

</section>
<?php include(__DIR__ . 'footer.php'); ?>

</body>
</html>