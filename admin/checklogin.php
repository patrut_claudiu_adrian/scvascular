<?php
include(__DIR__ . 'database.php');

$startora = 8;
$endora = 22;

$logged_in = false;
//$tipdemo=mysql_real_escape_string($_GET['demo']);

if (isset($_SESSION['username'], $_SESSION['password'])) {

    $usernamex = mysqli_real_escape_string($conn, $_SESSION['username']);

    $passx = mysqli_real_escape_string($conn, $_SESSION['password']);

    $q = "SELECT * FROM `admin` WHERE `user`='$usernamex' AND `parola`='$passx' LIMIT 1";

    $res_log = mysqli_query($conn, $q);

    if (mysqli_num_rows($res_log) === 1) {

        $logged_in = true;

        $row_log = mysqli_fetch_array($res_log);

        $usernume = htmlspecialchars($row_log['user']);
        //	$useremail=htmlspecialchars($row_log['email']);
        $userkey = $row_log['id'];
        //	$usertype=$row_log['tip'];
        $user = htmlspecialchars($row_log['user']);

    } else {
        $logged_in = false;
    }

}

function alert($msg, $type) {
    $styles[1] = 'alert success';
    $styles[2] = 'alert warning';
    $styles[3] = 'alert note';
    $styles[4] = 'alert info';

    return '<div class="' . $styles[$type] . '">' . $msg . '</div>';

}

$luni = array('Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie');

$culori = array(
    '#77bcd0',
    '#10bb16',
    '#99c105',
    '#f2a9e8',
    '#97dff8',
    '#ffaac4',
    '#bbcfdd',
    '#d8fc7d',
    '#4cf2d6',
    '#d8c2ba',
    '#ef4e21',
    '#8f99ae',
    '#b99595',
    '#ebcfa5',
    '#e0e641',
    '#f44ee6',
    '#91beb5',
    '#64f997',
    '#c1c2db',
    '#44f9f6',
    '#fea343',
    '#dbdcc3',
    '#f56f4f',
    '#fd1b4b',
    '#27f633',
    '#ea4435',
    '#68fe33',
);

shuffle($culori);
