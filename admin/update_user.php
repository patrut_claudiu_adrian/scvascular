<?php
session_start();

include('checklogin.php');

if (!$logged_in) {

    header('Location: index.php');

    exit();

}

$parola_conf = mysqli_real_escape_string($conn, $_POST['parola2']);
$parolax = mysqli_real_escape_string($conn, $_POST['parola1']);

if ($parolax !== $parola_conf) {
    $_SESSION['err'] = alert('Error! Check passwords!', 2);
    header('Location: contul_meu.php');
    exit();
}

if (!$parolax || !$parola_conf) {
    $_SESSION['err'] = alert('All fields are required.', 2);
    header('Location: contul_meu.php');
    exit();
}

$parola = md5($parolax);
$_SESSION['password'] = $parola;

$q = "UPDATE  `admin` SET  `parola` =  '$parola' WHERE  `id` ='1' LIMIT 1 ";
$resq = mysqli_query($conn, $q);

$_SESSION['err'] = alert('The password was successfully updated.', 1);
header('Location: contul_meu.php');
