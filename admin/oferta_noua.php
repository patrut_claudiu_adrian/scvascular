<?php 
session_start();

include('checklogin.php');

if (!$logged_in) 

	{ 

		header("Location: index.php"); 

		exit();

	}
	
$activ=2;
?>
<!doctype html>
<html lang="en-us">
<head>

	<meta  charset="utf-8">
	
<title><?php echo $page_title_general;?> </title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" class="theme"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins -->
	<script src="js/plugins.js"></script>
	<script src="js/editor.js"></script>
	<script src="js/calendar.js"></script>
	<script src="js/flot.js"></script>
	<script src="js/elfinder.js"></script>
	<script src="js/datatables.js"></script>
	
	<!-- all Whitelabel Plugins -->
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Autocomplete.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Calendar.js"></script>
	<script src="js/wl_Chart.js"></script>
	<script src="js/wl_Color.js"></script>
	<script src="js/wl_Date.js"></script>
	<script src="js/wl_Editor.js"></script>
	<script src="js/wl_Fil e.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Fileexplorer.js"></script>
	<script src="js/wl_Form.js"></script>
	<script src="js/wl_Gallery.js"></script>
	<script src="js/wl_Number.js"></script>
	<script src="js/wl_Password.js"></script>
	<script src="js/wl_Slider.js"></script>
	<script src="js/wl_Store.js"></script>
	<script src="js/wl_Time.js"></script>
	<script src="js/wl_Valid.js"></script>
	<script src="js/wl_Widget.js"></script>
	
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/script.js"></script>
	</script>
	<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	
</head>
<body>

<?php include("header_meniu.php"); ?>
				<div class="clearfix"> </div>
				<?php include("header.php"); ?>
				<?php include("meniu.php"); ?> 
					
		<section id="content">
		<div class="g12 nodrop">
		
		</div><?php if (isset($_SESSION['err'])) 
			   		{
						echo $_SESSION['err'];
						unset($_SESSION['err']);
					}
			    ?>
        <form id="form" action="rezervare_procesare.php" method="post"  enctype="multipart/form-data">
					<fieldset>
						<label>Articol nou</label>
						<section>
							<label for="text_field">Titlu *</label>
							<div><input type="text" id="nume" name="nume"></div>
						</section>
                        
                        <section style="display:none">
							<label for="text_field">Subtitle *</label>
							<div><input type="text" id="nume1" name="nume1"></div>
						</section>
                         
                        <section><label for="predefined_time">Continut *</label>
							<div><textarea id="editor1" name="editor1" class="ckeditor" rows="12"></textarea>
							</div>
						</section>
                        <script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'editor1' );

			//]]>
			</script>
                          <section><label for="predefined_time">Imagine</label>
							<div><input type="file" id="file_upload" name="file_upload">
							</div>
						</section>
                        <section>
							<label for="text_field"></label>
							<div><button onClick="document.getElementById('form').submit(); return false;" >Salveaza</button></div>
						</section>
		  </fieldset>
          
                    </form>
</section>
		<?php include("footer.php"); ?>
</body>
</html>