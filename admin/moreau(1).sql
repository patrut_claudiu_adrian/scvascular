-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 08, 2017 at 03:15 PM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `scvascular`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `user` varchar(300) NOT NULL,
  `parola` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user`, `parola`) VALUES
(1, 'admin', '68668fa9b5b52b367c357fd84f3686b3');

-- --------------------------------------------------------

--
-- Table structure for table `oferte`
--

CREATE TABLE IF NOT EXISTS `oferte` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `titlu` varchar(500) NOT NULL,
  `subtitlu` varchar(500) NOT NULL,
  `continut` text NOT NULL,
  `poza` varchar(300) NOT NULL,
  `data` varchar(30) NOT NULL,
  `activ` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `oferte`
--

INSERT INTO `oferte` (`id`, `titlu`, `subtitlu`, `continut`, `poza`, `data`, `activ`) VALUES
(25, 'USA 01/2004 â€“ 08/2005', 'Projet: RÃ©alisation nouvelle usine ', '<p>\r\n	Contexte G&eacute;n&eacute;ral&nbsp;: Acteur majeur sur son secteur, d&eacute;veloppement sur le march&eacute; am&eacute;ricain<br />\r\n	Sujet du projet&nbsp;: R&eacute;alisation nouvelle usine de production<br />\r\n	Responsabilit&eacute;s occup&eacute;es&nbsp;: Coordinateur &eacute;tudes process<br />\r\n	<br />\r\n	T&acirc;che effectu&eacute;e n&deg;1 P&amp;ID<br />\r\n	* Etablissement des P&amp;ID<br />\r\n	T&acirc;che effectu&eacute;e N&deg;2 B&acirc;timent<br />\r\n	* Etablissement plan masse et modification structure existante pour adaptation au process<br />\r\n	T&acirc;che effectu&eacute;e N&deg;3 Equipements<br />\r\n	* Etablissement des plans de principes pour constructeurs<br />\r\n	* V&eacute;rification plans constructeurs<br />\r\n	T&acirc;che effectu&eacute;e N&deg;4 Process<br />\r\n	* R&eacute;alisation du mod&egrave;le 3D des cheminements principaux<br />\r\n	* Coordination et v&eacute;rification des plans 2D, cahiers isom&eacute;triques, cahiers supportage<br />\r\n	T&acirc;che effectu&eacute;e N&deg;5 Programmation<br />\r\n	* R&eacute;alisation macros en visual basic 6 pour liste de mat&eacute;riels et accessoires pour isom&eacute;triques<br />\r\n	<br />\r\n	Environnement techniques&nbsp;: ANSI - ISO &ndash; 35 P&amp;ID &ndash; 85000 pouces soudure - AUTOCAD<br />\r\n	Environnement linguistique&nbsp;: Anglais, Espagnol</p>\r\n', '', '1441193842', 1),
(26, 'UKRAINE 09/2005 â€“ 08/2007', 'Projet : Modernisation', '<p>\r\n	R&eacute;alisation de P&amp;ID en fonction cahier des charges<br />\r\n	Etudes de tuyauteries (plans 2D + Isos + Liste mat + supportages)<br />\r\n	Relev&eacute;s sur site</p>\r\n', '', '1441193901', 1),
(27, 'ESPAGNE 09/2005 â€“ 08/2007', 'Projet : RÃ©alisation dâ€™une nouvelle usine ', '<p>\r\n	R&eacute;alisation de P&amp;ID en fonction cahier des charges&nbsp; (20 P&amp;ID)</p>\r\n', '', '1441193936', 1),
(28, 'HONGRIE 09/2005 â€“ 08/2007', 'Projet : Augmentation de capacitÃ©', '<p>\r\n	&eacute;tudes de tuyauteries (plans 2D+Isos+Liste mat)</p>\r\n', '', '1441193967', 1),
(29, 'ESPAGNE 09/2007 - 05/2008', 'Projet : RÃ©alisation dâ€™une nouvelle usine', '<p>\r\n	Sujet du projet&nbsp;: R&eacute;alisation d&rsquo;une nouvelle usine<br />\r\n	Responsabilit&eacute;s occup&eacute;es&nbsp;: Coordinateur &eacute;tudes process<br />\r\n	<br />\r\n	T&acirc;che effectu&eacute;e n&deg;1 P&amp;ID<br />\r\n	* V&eacute;rification des P&amp;ID en fonction du cahier des charges<br />\r\n	T&acirc;che effectu&eacute;e N&deg;2 Cheminement<br />\r\n	* Routine pr&eacute;liminaire de tuyauteries<br />\r\n	<br />\r\n	Environnement techniques&nbsp;: ISO &ndash; SMS &ndash; 30 P&amp;ID &ndash; 55000 pouces soudures - AUTOCAD<br />\r\n	Environnement linguistique&nbsp;: Anglais, Espagnol</p>\r\n', '', '1441194051', 1),
(30, 'TURQUIE 09/2008 â€“ 09/2008', 'Projet : Modernisation ', '<p>\r\n	Mise &agrave; jour P&amp;ID en fonction cahier des charges</p>\r\n', '', '1441263529', 1),
(31, 'RUSSIE 02/2010 â€“ 05/2010', 'Projet : Modernisation ', '<p>\r\n	&eacute;tudes de tuyauteries (plans 2D+Isos+Liste mat)</p>\r\n', '', '1441263557', 1),
(32, 'USA 11/2009 â€“ 12/2009', 'Projet : RÃ©alisation dâ€™une nouvelle usine ', '<p>\r\n	R&eacute;alisation isometriques &agrave; partir de plans 3D</p>\r\n', '', '1441263618', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
