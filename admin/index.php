<?php 
session_start();
include('checklogin.php');

if ($logged_in==TRUE) 
	{ 
		header("Location: home.php"); 
		exit();
	}
?>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title><?php echo $page_title_general;?> </title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/light/theme.css" class="theme">
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins -->
	<script src="js/plugins.js"></script>
		
	<!-- Whitelabel Plugins -->
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Dialog.js"></script>
	<script src="js/wl_Form.js"></script>
		
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/login.js"></script>
</head>
<body id="login">
		<header>
			<div id="logo">
			<a href="index.php"></a></div>
</header>
		<section id="content">
		<form action="login.php" id="loginform" method="post">
			<fieldset>
				<section><label for="username"><strong>Username</strong></label>
					<div><input type="text" id="username" name="username" autofocus></div>
				</section>
				<section><label for="password"><strong>Password</strong></label>
					<div><input type="password" id="password" name="password"></div>
					<br>

				</section>
				<section>
					<div ><button class="fr" onClick="document.getElementById('loginform').submit(); return false;">LogIn</button></div>
				</section>
			</fieldset><?php if (isset($_SESSION['err'])) 
			   		{
						echo $_SESSION['err'];
						unset($_SESSION['err']);
					}
			    ?>
		</form>
		</section>
		<footer><strong>© 2019 ImPULS</strong></footer>
		
</body>
</html>