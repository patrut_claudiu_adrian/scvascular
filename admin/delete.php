<?php
session_start();

include('checklogin.php');

if (!$logged_in) {

    header('Location: index.php');

    exit();

}

$key_rez = mysqli_real_escape_string($conn, $_GET['key']);

$q = "SELECT `id` FROM `oferte` WHERE `id`='$key_rez' LIMIT 1";
$resq = mysqli_query($conn, $q);
$numrowq = mysqli_num_rows($resq);
if ($numrowq !== 1) {

    $_SESSION['err'] = alert('Select at least one article!', 4);
    header('Location: home.php');
    exit();

}
$q = "DELETE FROM `oferte` WHERE `id`='$key_rez' LIMIT 1";
$res = mysqli_query($conn, $q);

$_SESSION['err'] = alert('Article was successfully deleted!', 1);
header('Location: home.php');
exit();

