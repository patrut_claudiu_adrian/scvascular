<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>Accesul vascular | ImPULS</title>
<meta charset="utf-8">
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
<style>
::placeholder {
 color: #fff !important;
 opacity: 1;
}
:-ms-input-placeholder {
 color: #fff !important;
}
::-ms-input-placeholder {
 color: #fff !important;
}
</style>
</head>
<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
<?php include("modal.php") ?>
<div id="canvas">
  <div id="box_wrapper">
    <?php include("meniu.php") ?>
  </div>
  <?php include("b3.php") ?>
  <section class="ls s-py-20 pb-90" style="background:#fafafa">
    <div class="container">
    <div class="divider-80 divider-xl-70"></div>
    <div class="row c-mb-30">
      <div class="col-lg-12 blog-featured-posts">
        <div class="row justify-content-center">
          <div class="col-xl-4 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="accesul-vascular-pentru-chimioterapie.htm"> <img src="images/accesul-vascular/accesul-vascular-pentru-chimioterapie.jpg" alt="accesul vascular pentru chimioterapie"> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="accesul-vascular-pentru-chimioterapie.htm"> Accesul vascular pentru chimioterapie </a> </h4>
                </header>
                <div class="entry-content">
                  <p>O cameră-port este un dispozitiv mic de acces venos, aproximativ de mărimea unei monede, folosit pentru a administra medicamente în sânge ... </p>
                </div>
              </div>
            </article>
          </div>
          <div class="col-xl-4 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="accesul-vascular-pentru-hemodializa.htm"> <img src="images/accesul-vascular/hemodializa.jpg" alt="hemodializa"> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="accesul-vascular-pentru-hemodializa.htm"> Accesul vascular pentru hemodializă </a> </h4>
                </header>
                <div class="entry-content">
                  <p>Hemodializa este un tratament pentru filtrarea deșeurilor și a apei din sânge, așa cum rinichii fac când sunt sănătoși. Hemodializa ajută la ...</p>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("bottom_contact.php") ?>
  <?php include("footer.php") ?>
</div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script> 
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>