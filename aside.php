<aside class="col-lg-4 col-xl-4 oswald affixx">

    <div class="widget widget_tag_cloud">
        <h2 class="widget-title">Categorii articole</h2>


        <div class="tagcloud">

            <a href="blog.htm" class="tag-cloud-link">
                Medicina
            </a>
        </div>
    </div>

    <div>
        <div class="widget widget_recent_posts">

            <h2 class="widget-title">Articole recente</h2>
            <ul class="list-unstyled">
                <?php
                $q = 'SELECT * FROM `oferte` WHERE `activ`=1 ORDER BY ID DESC LIMIT 10 ';
                $res_rez = mysqli_query($conn, $q);

                for ($i = 0, $iMax = mysqli_num_rows($res_rez); $i < $iMax; $i++) {
                    $row_con = mysqli_fetch_array($res_rez); ?>


                    <?php
                    $titluxx = $row_con['titlu'];

                    $normalizeChars = array(
                        'Š' => 'S',
                        'š' => 's',
                        'Ð' => 'Dj',
                        'Ž' => 'Z',
                        'ž' => 'z',
                        'À' => 'A',
                        'Á' => 'A',
                        'Â' => 'A',
                        'Ã' => 'A',
                        'Ä' => 'A',
                        'Å' => 'A',
                        'Æ' => 'A',
                        'Ç' => 'C',
                        'È' => 'E',
                        'É' => 'E',
                        'Ê' => 'E',
                        'Ë' => 'E',
                        'Ì' => 'I',
                        'Í' => 'I',
                        'Î' => 'I',
                        'Ï' => 'I',
                        'Ñ' => 'N',
                        'Ń' => 'N',
                        'Ò' => 'O',
                        'Ó' => 'O',
                        'Ô' => 'O',
                        'Õ' => 'O',
                        'Ö' => 'O',
                        'Ø' => 'O',
                        'Ù' => 'U',
                        'Ú' => 'U',
                        'Û' => 'U',
                        'Ü' => 'U',
                        'Ý' => 'Y',
                        'Þ' => 'B',
                        'ß' => 'Ss',
                        'à' => 'a',
                        'á' => 'a',
                        'â' => 'a',
                        'ã' => 'a',
                        'ä' => 'a',
                        'å' => 'a',
                        'æ' => 'a',
                        'ç' => 'c',
                        'è' => 'e',
                        'é' => 'e',
                        'ê' => 'e',
                        'ë' => 'e',
                        'ì' => 'i',
                        'í' => 'i',
                        'î' => 'i',
                        'ï' => 'i',
                        'ð' => 'o',
                        'ñ' => 'n',
                        'ń' => 'n',
                        'ò' => 'o',
                        'ó' => 'o',
                        'ô' => 'o',
                        'õ' => 'o',
                        'ö' => 'o',
                        'ø' => 'o',
                        'ù' => 'u',
                        'ú' => 'u',
                        'û' => 'u',
                        'ü' => 'u',
                        'ý' => 'y',
                        'þ' => 'b',
                        'ÿ' => 'y',
                        'ƒ' => 'f',
                        'ă' => 'a',
                        'ș' => 's',
                        'ț' => 't',
                        'Ă' => 'A',
                        'Ș' => 'S',
                        'Ț' => 'T',
                    );

                    $titluxx = strtr($titluxx, $normalizeChars);
                    $titluxx = strtolower($titluxx);

                    $titluxx = preg_replace('/[^A-Za-z0-9\-]/', '-', $titluxx);
                    $titluxx = substr_replace($titluxx, "", -1);

                    ?>

                    <li class="media">
                        <a href="articol.php?id=<?php echo htmlspecialchars($row_con['id']); ?>">
                            <img src="admin/upload/<?php echo htmlspecialchars($row_con['poza']); ?>">
                        </a>
                        <div class="media-body">

                            <a href="<?php echo htmlspecialchars($row_con['id']); ?>-<?php echo $titluxx; ?>"><?php echo htmlspecialchars($row_con['titlu']); ?></a>

                        </div>
                    </li>
                <?php } ?>


            </ul>
        </div>
    </div>

    <div class="widget">
        <h2 class="widget-title">Facebook</h2>

        <div class="fb-like" data-href="https://www.facebook.com/SacreCoeurVascular/" data-mobile="mobile" data-width="350" data-layout="standard" data-action="like"
             data-size="large" data-show-faces="true" data-share="true"></div>

    </div>
    <div class="widget widget_mailchimp">
        <p> Abonare Newsletter </p>
        <form class="signup" action="">
            <label for="mailchimp_email"> <span class="screen-reader-text">Abonare:</span> </label>
            <input id="mailchimp_email" name="email" type="email" class="form-control mailchimp_email" placeholder="Tasteaza email-ul">
            <button type="submit" class="search-submit"><span class="screen-reader-text"> Ma abonez</span></button>
            <div class="response"></div>
        </form>
    </div>




</aside>

<div class="divider-30 divider-lg-100"></div>