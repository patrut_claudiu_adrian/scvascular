<section id="information-block" class="ds s-pt-xl-90 s-pb-xl-94 s-pt-60 s-pb-60">
    <div class="container">
      <div class="row c-gutter-50">
        <div class="divider-10 divider-lg-10 divider-xl-5"></div>
        <div class="col-md-9 col-lg-6 col-sm-12">
          <h3 class="after-title oswald">Doriți <span>o consultație?</span></h3>
          <p class="subtitle">Suntem aici pentru a vă asculta, pentru a înțelege cum boala dumneavoastră vă afectează viața și pentru a vă îngriji folosind cele mai noi și eficiente tehnici chirurgicale. </p>
          <div class="mt-45">
            <a href="contact.htm" class="btn btn-outline-darkgrey small fw-400">Programează-te online</a>
          </div>
          <div class="divider-20 divider-lg-20 divider-xl-5"></div>
        </div>
      </div>
    </div>
  </section>