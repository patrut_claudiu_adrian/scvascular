<header class="page_header ds justify-nav-end">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-xl-3 col-lg-4 col-md-5 col-11"> <a href="/" class="logo"> <img src="images/logo.png" alt="ImPULS"> </a> </div>
      <div class="col-xl-9 col-lg-8 col-md-7 col-1">
        <div class="nav-wrap"> 
          <nav class="top-nav">
            <ul class="nav sf-menu">
              <li class="active"> <a href="/">Acasă</a> </li>
              <li> <a href="despre-noi.htm">Asociația</a> </li>
              <li> <a href="bolile-arterelor.htm">Bolile arterelor</a>
                <ul>
                  <li> <a href="ateroscleroza.htm">Ateroscleroza </a> </li>
                  <li> <a href="boala-carotidiana.htm">Boala carotidiană </a> </li>
                  <li> <a href="boala-arteriala-periferica-a-membrelor-inferioare.htm">Boala arterială periferică </a> </li>
                  <li> <a href="ischemia-critica-a-membrelor-inferioare.htm">Ischemia critică </a> </li>
                  <li> <a href="anevrismul-aortei-abdominale.htm">Anevrismul aortei abdominale </a> </li>
                  <li> <a href="boala-buerger-trombangeita-obliteranta.htm">Trombangeita obliterantă (boala Buerger) </a> </li>
                </ul>
              </li>
              <li> <a href="bolile-venelor.htm">Bolile venelor</a> 
			   <ul>
                  <li> <a href="insuficienta-venoasa-cronica-varice.htm">Insuficiența venoasă cronică</a> </li>
                  <li> <a href="tromboza-venoasa-profunda.htm">Tromboza venoasă profundă</a> </li>
                  <li> <a href="sindromul-posttrombotic.htm">Sindromul posttrombotic</a> </li>
                  <li> <a href="ulcerul-venos.htm">Ulcerul venos</a> </li>
                </ul>
			  </li>
              <li> <a href="accesul-vascular.htm">Accesul vascular</a> 
			  <ul>
			  <li> <a href="accesul-vascular-pentru-chimioterapie.htm">Accesul vascular pentru chimioterapie</a> </li>
			  <li> <a href="accesul-vascular-pentru-hemodializa.htm">Accesul vascular pentru hemodializă</a> </li>
			  </ul>
			  </li>
              <li> <a href="resurse-pacienti.htm">Pacienți </a>
                <ul>
                  <li> <a href="bolile-arterelor.htm">Bolile arterelor </a> </li>
                  <li> <a href="bolile-venelor.htm">Bolile venelor </a> </li>
                  <li> <a href="accesul-vascular.htm">Accesul vascular </a> </li>
                  <li> <a style="cursor:pointer">Informații preoperatorii </a> 
				  <ul>
                    <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-aorto-iliaca.pdf" target="_blank">Angioplastia cu balon și stentarea aorto-iliaca</a></li>
                    <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-femoro-poplitee.pdf" target="_blank">Angioplastia cu balon și stentarea femoro-poplitee</a></li>
                    <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-arterelor-gambiere.pdf" target="_blank">Angioplastia cu balon și stentarea arterelor gambiere</a></li>
                    <li><a href="informatii-preoperatorii/bypass-ul-aorto-femural.pdf" target="_blank">Bypass-ul aorto-femural</a></li>
                    <li><a href="informatii-preoperatorii/bypass-ul-femuro-popliteu-gambier.pdf" target="_blank">Bypass-ul femuro-popliteu / gambier </a></li>
                    <li><a href="informatii-preoperatorii/endarterectomia-carotidiana.pdf" target="_blank">Endarterectomia carotidiană</a></li>
                    <li><a href="informatii-preoperatorii/tratamentul-chirurgical-deschis-al-anevrismului-de-aorta-abdominala.pdf" target="_blank">Tratamentul chirurgical deschis al anevrismului de aortă abdominală</a></li>
                    <li><a href="informatii-preoperatorii/tratamentul-endovascular-al-anevrismului-de-aorta-abdominala.pdf" target="_blank">Tratamentul endovascular al anevrismului de aortă abdominală</a></li>
                    <li><a href="informatii-preoperatorii/tratamentul-chirurgical-al-varicelor.pdf" target="_blank">Tratamentul chirurgical al varicelor</a></li>
                    <li><a href="informatii-preoperatorii/scleroterapia-venelor-reticulare.pdf" target="_blank">Scleroterapia venelor reticulare</a></li>
                    <li><a href="informatii-preoperatorii/accesul-vascular-pentru-hemodializa.pdf" target="_blank">Accesul vascular pentru hemodializă</a></li>
                    <li><a href="informatii-preoperatorii/accesul-vascular-pentru-chimioterapie.pdf" target="_blank">Accesul vascular pentru chimioterapie</a></li>
                  </ul>
				  </li>
                  <li> <a style="cursor:pointer">Informații postoperatorii </a> 
				  <ul>
                    <li><a href="instructiuni-postoperatorii/angioplastia-endovasculara.pdf" target="_blank">Angioplastia endovasculară</a></li>
                    <li><a href="instructiuni-postoperatorii/bypass-ul-suprainghinal.pdf" target="_blank">Bypass-ul suprainghinal</a></li>
                    <li><a href="instructiuni-postoperatorii/bypass-ul-infrainghinal.pdf" target="_blank">Bypass-ul infrainghinal</a></li>
                    <li><a href="instructiuni-postoperatorii/endarterectomia-carotidiana.pdf" target="_blank">Endarterectomia carotidiană</a></li>
                    <li><a href="instructiuni-postoperatorii/ablatia-varicelor.pdf" target="_blank">Ablația varicelor</a></li>
                    <li><a href="instructiuni-postoperatorii/scleroterapia.pdf" target="_blank">Scleroterapia</a></li>
                    <li><a href="instructiuni-postoperatorii/accesul-vascular-pentru-hemodializa.pdf" target="_blank">Accesul vascular pentru hemodializă</a></li>
                    <li><a href="instructiuni-postoperatorii/accesul-vascular-pentru-chimioterapie.pdf" target="_blank">Accesul vascular pentru chimioterapie</a></li>
                  </ul>
				  </li>
                </ul>
              </li>
              <li> <a href="blog.htm">Blog</a> </li>
              <li> <a href="contact.htm">Contact</a> </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <span class="toggle_menu"><span></span></span> </header>