<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>Resurse pacienti | ImPULS</title>
<meta charset="utf-8">
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
<style>
::placeholder {
 color: #fff !important;
 opacity: 1;
}

:-ms-input-placeholder {
 color: #fff !important;
}

::-ms-input-placeholder {
 color: #fff !important;
}
</style>
</head>

<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

<?php include("modal.php") ?>
<div id="canvas">
  <div id="box_wrapper">
    <?php include("meniu.php") ?>
  </div>
  <?php include("b5.php") ?>
  <section class="ls s-py-20 pb-90" style="background:#fafafa">
    <div class="container">
    <div class="divider-80 divider-xl-70"></div>
    <div class="row c-mb-30">
      <div class="col-lg-12 blog-featured-posts">
        <div class="row justify-content-center">
          <div class="col-xl-4 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="bolile-arterelor.htm"> <img src="images/bolile-arterelor/anevrismul-aortei.jpg" alt=""> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="bolile-arterelor.htm"> Bolile arterelor </a> </h4>
                </header>
                <div class="entry-content oswald">
                  <p>
                  <ul>
                  <li> <a href="ateroscleroza.htm">Ateroscleroza </a> </li>
                  <li> <a href="boala-carotidiana.htm">Boala carotidiană </a> </li>
                  <li> <a href="boala-arteriala-periferica-a-membrelor-inferioare.htm">Boala arterială periferică </a> </li>
                  <li> <a href="ischemia-critica-a-membrelor-inferioare.htm">Ischemia critică </a> </li>
                  <li> <a href="anevrismul-aortei-abdominale.htm">Anevrismul aortei abdominale </a> </li>
                  <li> <a href="boala-buerger-trombangeita-obliteranta.htm">Trombangeita obliterantă (boala Buerger) </a> </li>
                  </ul>
                  </p>
                </div>
              </div>
            </article>
          </div>
          <div class="col-xl-4 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="anevrismul-aortei-abdominale.htm"> <img src="images/bolile-venelor/insuficieta-venoasa.jpg" alt=""> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="bolile-venelor.htm"> Bolile venelor </a> </h4>
                </header>
                <div class="entry-content oswald">
                  <p>
                  <ul>
                  <li> <a href="insuficienta-venoasa-cronica-varice.htm">Insuficiența venoasă cronică</a> </li>
                  <li> <a href="tromboza-venoasa-profunda.htm">Tromboza venoasă profundă</a> </li>
                  <li> <a href="sindromul-posttrombotic.htm">Sindromul posttrombotic</a> </li>
                  <li> <a href="ulcerul-venos.htm">Ulcerul venos</a> </li>
                  </ul>
                  </p>
                </div>
              </div>
            </article>
          </div>
          <div class="col-xl-4 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="accesul-vascular.htm"> <img src="images/accesul-vascular/accesul-vascular-pentru-chimioterapie.jpg" alt=""> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="accesul-vascular.htm"> Accesul vascular </a> </h4>
                </header>
                <div class="entry-content oswald">
                  <p>
                  <ul>
                 <li> <a href="accesul-vascular-pentru-chimioterapie.htm">Accesul vascular pentru chimioterapie</a> </li>
			  <li> <a href="accesul-vascular-pentru-hemodializa.htm">Accesul vascular pentru hemodializă</a> </li>
                  </ul>
                  </p>
                </div>
              </div>
            </article>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-xl-5 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="#"> <img src="images/pacienti/informatii-preoperatorii.jpg" alt="informatii preoperatorii"> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="#"> Informații preoperatorii </a> </h4>
                </header>
                <div class="entry-content oswald">
                  <p>
                  <ul>
                    <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-aorto-iliaca.pdf" target="_blank">Angioplastia cu balon și stentarea aorto-iliaca</a></li>
                    <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-femoro-poplitee.pdf" target="_blank">Angioplastia cu balon și stentarea femoro-poplitee</a></li>
                    <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-arterelor-gambiere.pdf" target="_blank">Angioplastia cu balon și stentarea arterelor gambiere</a></li>
                    <li><a href="informatii-preoperatorii/bypass-ul-aorto-femural.pdf" target="_blank">Bypass-ul aorto-femural</a></li>
                    <li><a href="informatii-preoperatorii/bypass-ul-femuro-popliteu-gambier.pdf" target="_blank">Bypass-ul femuro-popliteu / gambier </a></li>
                    <li><a href="informatii-preoperatorii/endarterectomia-carotidiana.pdf" target="_blank">Endarterectomia carotidiană</a></li>
                    <li><a href="informatii-preoperatorii/tratamentul-chirurgical-deschis-al-anevrismului-de-aorta-abdominala.pdf" target="_blank">Tratamentul chirurgical deschis al anevrismului de aortă abdominală</a></li>
                    <li><a href="informatii-preoperatorii/tratamentul-endovascular-al-anevrismului-de-aorta-abdominala.pdf" target="_blank">Tratamentul endovascular al anevrismului de aortă abdominală</a></li>
                    <li><a href="informatii-preoperatorii/tratamentul-chirurgical-al-varicelor.pdf" target="_blank">Tratamentul chirurgical al varicelor</a></li>
                    <li><a href="informatii-preoperatorii/scleroterapia-venelor-reticulare.pdf" target="_blank">Scleroterapia venelor reticulare</a></li>
                    <li><a href="informatii-preoperatorii/accesul-vascular-pentru-hemodializa.pdf" target="_blank">Accesul vascular pentru hemodializă</a></li>
                    <li><a href="informatii-preoperatorii/accesul-vascular-pentru-chimioterapie.pdf" target="_blank">Accesul vascular pentru chimioterapie</a></li>
                  </ul>
                  </p>
                </div>
              </div>
            </article>
          </div>
          <div class="col-xl-5 col-md-6">
            <article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
              <div class="item-media post-thumbnail"> <a href="#"> <img src="images/pacienti/informatii-postoperatorii.jpg" alt=""> </a> </div>
              <div class="item-content">
                <header class="entry-header">
                  <h4 class="entry-title text-uppercase"> <a href="#"> Informații postoperatorii </a> </h4>
                </header>
                <div class="entry-content oswald">
                  <p>
                  <ul>
                    <li><a href="instructiuni-postoperatorii/angioplastia-endovasculara.pdf" target="_blank">Angioplastia endovasculară</a></li>
                    <li><a href="instructiuni-postoperatorii/bypass-ul-suprainghinal.pdf" target="_blank">Bypass-ul suprainghinal</a></li>
                    <li><a href="instructiuni-postoperatorii/bypass-ul-infrainghinal.pdf" target="_blank">Bypass-ul infrainghinal</a></li>
                    <li><a href="instructiuni-postoperatorii/endarterectomia-carotidiana.pdf" target="_blank">Endarterectomia carotidiană</a></li>
                    <li><a href="instructiuni-postoperatorii/ablatia-varicelor.pdf" target="_blank">Ablația varicelor</a></li>
                    <li><a href="instructiuni-postoperatorii/scleroterapia.pdf" target="_blank">Scleroterapia</a></li>
                    <li><a href="instructiuni-postoperatorii/accesul-vascular-pentru-hemodializa.pdf" target="_blank">Accesul vascular pentru hemodializă</a></li>
                    <li><a href="instructiuni-postoperatorii/accesul-vascular-pentru-chimioterapie.pdf" target="_blank">Accesul vascular pentru chimioterapie</a></li>
                  </ul>
                  </p>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("bottom_contact.php") ?>
  <?php include("footer.php") ?>
</div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script> 
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>