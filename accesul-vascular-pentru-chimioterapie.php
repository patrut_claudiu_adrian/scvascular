<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Accesul vascular pentru chimioterapie | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b3.1.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>



O cameră-port este un dispozitiv mic de acces venos, aproximativ de mărimea unei monede, folosit pentru a administra medicamente în sânge. Este implantată sub piele, de obicei în partea superioară a pieptului.






								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-marius-manda.jpg" alt="Dr. Marius Manda">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. Marius Manda</h5>
                                                        <p class="bl-dr">
                                                           “Porturile sunt utilizate la pacienții care necesită administrarea frecventă de chimioterapie, transfuzii de sânge, antibiotice, hrănire intravenoasă sau recoltări de sânge frecvente pentru analize.”





                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">CAMERA IMPLANTABILĂ PENTRU CHIMIOTERAPIE</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/accesul-vascular/accesul-vascular-pentru-chimioterapie.jpg" alt="accesul vascular pentru chimioterapie">
                                    </div>

                                    <div class="entry-content">

                           

<p>Dispozitivul este format din corpul portului și un cateter. Corpul portului are una sau mai multe camere distincte care sunt sigilate cu un capac moale (sept), siliconic, care poate fi străpuns cu ace speciale. Un cateter moale, din plastic, se conectează la corpul portului și vârful cateterului este plasat într-una din vene centrale mari din piept, care transporta sânge în inimă. </p><p>Un port elimină necesitatea înțepării repetate a venelor brațului. Deoarece portul livrează medicamente în vene centrale mari, medicamentele se amestecă cu sângele, diluându-se, astfel încât sunt mai puțin dăunătoare pereților venoși.

</p>

<p><strong>Un port are următoarele avantaje:</strong></p>















<ul class="list-styled">
										<li>rămâne implantat luni sau ani, în funcție de cât timp este necesar</li>
<li>reduce numărul de înțepături în vene pentru a recolta sânge sau a infuza medicamente sau lichide</li>
<li>reduce durerea și timpul petrecut în încercarea de a accesa o venă adecvată</li>
<li>medicamentele administrate sunt diluate de sânge, lezând mai puțin peretele venos</li>
<li>mai multe tipuri de medicamente sau alte tratamente pot fi administrate în același timp cu ajutorul unui port cu două camere</li>
</ul>








                                                                 

                                        

                                        
                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">DESPRE IMPLANTARE</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-50">
                                            <img src="images/accesul-vascular/camera-implantabila.jpg" alt="camera implantabila">
                                        </div>
                                        <p>Operația de implantare a camerei-port este o procedură scurtă efectuată de un chirurg vascular, sub anestezie locală și în condiții sterile. Zona în care va fi inserat portul este sterilizată, apoi amorțită cu un anestezic local. În timpul procedurii, chirurgul face o mică incizie deasupra claviculei și o altă incizie sub claviculă. </p><p>Un tunel va fi creat sub piele între cele două deschideri. Cateterul va fi trecut prin acest tunel și apoi va fi introdus ușor într-o venă centrală mare - jugulară sau subclavie. Apoi chirurgul face un buzunar sub piele, în care portul va fi plasat și securizat. Acest buzunar și inciziile vor fi închise cu suturi resorbabile. Bandaje mici vor acoperi inciziile.<p>
										
                                           
  <div class="text-center mb-0 mt-90">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:24px;">RISCURILE ȘI COMPLICAȚIILE POSIBILE 
</h4>
                                        </div>
										<p>Orice tip de acces vascular poate dezvolta complicații, cele mai frecvente fiind:</p>
<ul class="list-styled">
										<li>infecția plăgii operatorii sau a camerei port;</li>
<li>flux sanguin scăzut sau blocaj total, cauzate de un cheag de sânge sau de cicatrice</li>


		</ul>		
<p>Aceste probleme pot afecta tratamentul, fiind posibil să aveți nevoie de mai multe proceduri pentru a înlocui sau repara dispozitivul pentru ca acesta să funcționeze corect.</p>		
                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>o cameră-port este un dispozitiv mic de acces venos, implantat sub piele, folosit pentru a administra medicamente în sânge



</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>cel mai frecvent sunt implantate pacienților care au nevoie de chimioterapie sau hrănire intravenoasă pe perioade lungi de timp





</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>operația de implantare a camerei-port este o procedură scurtă, efectuată de un chirurg vascular, sub anestezie locală și în condiții sterile






</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>infecția plăgii operatorii și înfundarea cateterului cu un cheag de sânge sunt complicații rare, dar posibile




</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>