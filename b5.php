<section class="page_title about padding-mobile cs s-pt-50 s-pb-40 cover-background2" style="background-image: url('images/bg/bg9.jpg');">
	<div class="container">
		<div class="row">


			<div class="col-md-6 text-left animate head-title" data-animation="fadeInUp">
				<div class="col-xl-9 col-md-12 pl-0 text-uppercase">
					<p>Educatie in sanatate</p>
				</div>
				<div class="col-xl-8 col-md-12 pl-0">
					<h1 class="text-uppercase">Resurse pacienți</h1>
				</div>
				<div class="col-xl-9 col-md-12 pl-0">
					<p>The most common population pathologies of the venous system include: venous insufficiency of the lower limbs, venous thrombosis, pelvic venous insufficiency and others.</p>
				</div>
			</div>

			<div class="col-md-6 text-center animate d-none d-xl-block" data-animation="fadeInUp">
				<div class="head-elements row">

					<div class="col-lg-3">
						<a href="bolile-arterelor.htm">
							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Bolile arterelor</h6>

							</div>
						</a>
					</div>

					<div class="col-lg-3">
						<a href="bolile-venelor.htm">
							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Bolile venelor</h6>

							</div>
						</a>
					</div>

					<div class="col-lg-3">
						<a href="accesul-vascular.htm">
							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Accesul vascular</h6>

							</div>
						</div>
						<div class="col-lg-3">

							<div class="icon-box text-centerbox-shadow">



							</div>

						</div>


					</div>
					<div class="head-elements row">

						<div class="col-lg-3">

							<div class="icon-box text-center box-shadow">



							</div>

						</div>

						<div class="col-lg-3">

						</div>

						<div class="col-lg-3">

							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Informații preoperatorii</h6>

							</div>

						</div>
						<div class="col-lg-3">

							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Informații postoperatorii</h6>

							</div>

						</div>


					</div>
				</div>
			</div>
		</div>
	</section>