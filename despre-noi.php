<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>Despre noi | ImPULS</title>
<meta charset="utf-8">
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
</head>
<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->
<?php include("modal.php") ?>
<div id="canvas">
  <div id="box_wrapper">
    <?php include("meniu.php") ?>
  </div>
  <section class="page_title about padding-mobile cs s-pt-150 s-pb-130 cover-background">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center oswald animate" data-animation="fadeInUp">
          <h2>Asociația Sacré-Coeur Vascular</h2>
        </div>
      </div>
    </div>
  </section>

  <section id="welcome" class="ls">
  <div class="container">
    <div class="row">
      <div class="divider-30 d-none d-xl-block"></div>
      <div class="col-md-12 header-parallax mb-60">
        <div class="row align-items-center">
          <div class="col-lg-12 animate mt-60" data-animation="fadeInUp">
           	<div class="text-center">
								<h3>O echipă <span class="color-main"> de doi</span><br>chirurgi vasculari</h3>
								<p class="after-title subtitle">Am hotărât la începutul anului 2019 să punem bazele asociației Sacre-Coeur Vascular pentru a răspunde nevoilor de tratament medical de specialitete ale populației din vestul și sud-vestul țării.</p>
							</div>

							
								


								


							<div class="col-lg-12 col-xs-12 mx-auto mt-60">

							 <img src="images/echipa/echipa-medicala.jpg" alt="echipa medicala ImPULS">
							 
</div>
            <div class="divider-20 divider-sm-30 divider-md-50"></div>
            <hr>
            <div class="c-gutter-30 text-center">
              <p class="after-title subtitle">Chirurgia vasculară este o specialitate chirurgicală care tratează bolile sistemului vascular - arterele, venele și vasele limfatice. Aceasta a evoluat din chirurgia generală și cardiacă, precum și din tehnicile minim invazive pionierate de radiologi intervenționiști. </p>
			  <div class="col-lg-8 col-xs-12 mx-auto mt-40">
			  <img src="images/vena.jpg" alt="vena">
			  </div>
			  
              <p class="after-title subtitle">Astăzi, chirurgul vascular este instruit în a diagnostica și trata bolile care afectează toate părțile sistemului vascular, cu excepția celor ale inimii și ale creierului. În acest scop el poate apela fie la operații complexe deschise, fie la proceduri endovasculare minim invazive. Multe probleme vasculare pot fi tratate cu medicamente sau exerciții fizice. </p>
			  
              <div class="divider-30 d-lg-none d-md-block"></div>
              <a href="resurse-pacienti.htm" class="btn btn-maincolor large">Descoperiți bolile tratate</a> </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<section id="information-block" class="ds ds1 s-pt-xl-90 s-pb-xl-94 s-pt-60 s-pb-60">
  <div class="container">
    <div class="row c-gutter-50">
      <div class="divider-10 divider-lg-10 divider-xl-5"></div>
      <div class="col-md-6 col-lg-6 col-sm-12 animate" data-animation="fadeInUp" >
        <h3 class="after-title oswald" style="font-weight:400; font-size:30px">Mai mult decât chirurgie</h3>
        <p>Un chirurg vascular face mai mult decât chirurgie. El se asigură că pacienții cu probleme de sănătate vasculară își înțeleg boala și toate opțiunile de tratament posibile. Unii pacienți au nevoie de un tip de intervenție, alții de alta, în timp ce mulți nu au nevoie de nicio operație. </p>
        <p>Pacienții pot fi siguri că vor primi cel mai bun tratament pentru nevoile lor specifice. Bolile vasculare sunt, de obicei, afecțiuni cronice, care evoluează pe termen lung. De aceea, chirurgul vascular este un medic cu care veți lega relații profesionale pe termen lung, îngrijindu-se de sănătatea circulației dumneavoastră decenii la rând. </p>
        <div class="divider-20 divider-lg-20 divider-xl-5"></div>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-12 animate" data-animation="fadeInUp">
        <h3 class="after-title oswald" style="font-weight:400; font-size:30px">Scopul asociației</h3>
        <p>Ținând cont de complexitatea specialității și de gravitatea bolilor pe care le tratăm, cu mare potențial de infirmitate, am hotărât să ne asociem, făcând astfel posibil:
        <ul>
          <li>asigurarea permanenței în acordarea îngrijirilor medicale de specialitate, 12 ore pe zi, 5 zile pe săptămână;</li>
          <li>realizarea de intervenții chirurgicale complexe, care necesită prezența a cel puțin doi chirurgi;</li>
          <li>supravegherea evoluției pacienților pe o perioadă îndelungată de timp, menținând o legătură strânsă atât cu pacientul cât și cu medicul acestuia de familie.</li>
        </ul>
        </p>
        <div class="divider-20 divider-lg-20 divider-xl-5"></div>
      </div>
    </div>
  </div>
</section>
<section class="ls s-py-65 s-py-xl-141 animate" data-animation="fadeInUp">
  <div class="container">
    <div class="row c-gutter-135 mobile-padding-normal">
      <div class="col-md-12">
        <div class="text-center mb-45">
          <h3 class="oswald"><span class="color-main ">Metodele </span> de tratament</h3>
          <p class="subtitle width-100 width-xl-60" style="font-size:19px;">Chirurgii vasculari dezvoltă planuri de tratament care se pliază pe nevoile fiecărui pacient în parte. În funcție de boala dvs., acestea pot include:</p>
        </div>
        <div class="row">
		<div class="col-md-4 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <h6 class="color-dark mt-28 after-title2">Chirurgie deschisă</h6>
            <p>Chirurgie deschisă pentru repararea, înlocuirea sau îndepărtarea vaselor de sânge bolnave. </p>
          </div>
		   <div class="col-md-4 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <h6 class="color-dark mt-28 after-title2">Proceduri endovasculare</h6>
            <p>Procedurile bazate pe cateterism, cum ar fi: ablația, angioplastia sau stentarea </p>
          </div>
		   <div class="col-md-4 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <h6 class="color-dark mt-28 after-title2">Medicamente</h6>
            <p>Medicamente care scad riscul formării cheagurilor, deschid vasele de sânge și corectează factorii de risc</p>
          </div>
       
         
         
        </div>
        <div class="row">
		   <div class="col-md-4 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <h6 class="color-dark mt-28 after-title2">Stilul de viață</h6>
            <p>Modificări ale stilului de viață, cum ar fi renunțarea la fumat, creșterea activității fizice și alegerea unei alimentații sănătoase; </p>
          </div>
		     <div class="col-md-4 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <h6 class="color-dark mt-28 after-title2">Tromboliza</h6>
            <p>Tromboliza, procedură ce dizolvă cheaguri de sânge formate în diverse părți din organism </p>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <h6 class="color-dark mt-28 after-title2">Scleroterapia</h6>
            <p>Procedura minim-invaziva (injecții) ce închid în siguranță venele varicoase </p>
          </div>
       
        </div>
        <div class="row">
          
		  
          <div class="col-md-8 col-sm-12">
            <div class="divider-30 divider-md-0 divider-xl-0"></div>
            <a href="resurse-pacienti.htm" class="btn btn-maincolor large mt-40">Metodele de tratament</a> </div>
        </div>
        <h5 class="oswald">Asigurăm consultații de chirurgie vasculară în: Timisoara, Arad si Deva</h5>
        <a href="contact.htm" class="btn btn-outline-maincolor mt-10">Programați-vă la o consultație</a> </div>
      <div class="divider-5 d-lg-block d-xl-5"></div>
    </div>
  </div>
</section>
<?php include("footer.php") ?>
</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas --> 
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>