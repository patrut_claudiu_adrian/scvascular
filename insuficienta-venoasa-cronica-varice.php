<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Insuficiența venoasă cronică (varice) | ImPULS</title>
    
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b2.1.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>
Numărul de persoane care suferă de insuficiență venoasă a membrelor inferioare este în continuă creștere. Motivul principal este stilul de viață sedentar.

Uneori unicul simptom este reprezentat de senzația de picioare grele după statul în picioare sau așezat pe scaun timp îndelungat. În alte cazuri, poate fi vorba de vene mici cu aspect de pânză de păianjen și, odată cu evoluția bolii - vene varicoase mari, care influențează nu numai aspectul estetic al picioarelor, dar mai ales pot avea consecințe grave cum ar fi ulcerul venos sau tromboza venoasă.



								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-marius-manda.jpg" alt="Dr. Marius Manda">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. Marius Manda</h5>
                                                        <p class="bl-dr">
                                                            “Senzațiile de picioare grele, dureroase și umflate după o zi de lucru la birou pot fi primele simptome de insuficiență venoasă a membrelor inferioare.”


                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <a href="" data-toggle="modal" data-target="#map1_modal"><img src="images/bolile-venelor/insuficieta-venoasa.jpg" alt="varice"></a>
                                    </div>

                                    <div class="entry-content">

                                        <p>Factorul declanșator al insuficienței venoase simptomatice este reprezentat de creșterea presiunii în patul venos, apărută în urma statului prelungit în picioare sau perioadelor lungi de stat pe scaun. Boala se manifestă inițial prin apariția pe piele, mai ales în partea de jos a picioarelor, a venelor mici, cu aspect de pânză de păianjen, de culoare roșu-violaceu.


</p><p>

Cu timpul, schimbările în piele și țesuturile subcutanate, cauzate de această presiune crescută, devin persistente, venele umflându-se și mai mult se transformă în varice. Sângele nu se mai scurge spre inimă, stagnează în aceste vene dilatate, iar apa din sânge iasă în țesuturi. Picioarele se umflă, pielea nu mai primește destul oxigen și începe să moară. Astfel apar ulcerele venoase. </p><p>

<strong>Cauzele insuficienței venoase a membrelor inferioare:</strong>
<ul class="list-styled">
										<li>fumatul</li>
<li>vene varicoase primare - congenitale</li>
<li>vene varicoase secundare - cauzate de postură: statul timp îndelungat în picioare sau așezat pe scaun</li>
<li>istoric de tromboză venoasă profundă cu sindrom post-trombotic</li>
<li>valvule venoasele defecte (congenitale sau dobândite)</li>
<li>presiune asupra venelor din exterior (de exemplu, în timpul sarcinii, uterul crește în dimensiuni și apasă pe venele din abdomen)</li></ul>








                                        </p>

                                        

                                        <h4 class="oswald">Factorii de risc pentru apariția insuficienței venoase</h4>
                                        <ul class="list-styled">
										
     
     
     
     
     
     
     
     

										<li>sexul feminin</li>
<li>înaintarea în vârstă</li>
<li>istoricul familial (în cele mai multe cazuri boala este ereditară)</li>
<li>sarcina</li>
<li>obezitatea</li>
<li>statul în picioare sau așezat pe scaun mai multe ore consecutive</li>
<li>constipația cronică</li>
<li>terapie hormonală (contraceptive orale, terapie de substituție hormonală)
</li>
<li>înălțimea mare</li>
                                        </ul>
                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-venelor/clasificare-varice.jpg" alt="clasificare varice picioare">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Care sunt simptomele insuficienței venoase?</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Insuficiența venoasă a membrelor inferioare se caracterizează printr-un spectru larg de simptome, în funcție de mecanismul de formare, sensibilitatea individuală a pacientului și severitatea bolii.



</p>
<div><i class="fa fa-check-circle"></i> disconfort și senzație de greutate și tensiune în musculatura inferioară a picioarelor după mai multe ore de stat în picioare sau ședere pe scaun, ameliorate prin repausul în pat cu picioarele ușor ridicate.  </div>
                                                            <div><i class="fa fa-check-circle"></i> crampe în mușchii gambelor, mai ales noaptea </div>
															<div><i class="fa fa-check-circle"></i> prurit (mâncărime) </div>
															<div><i class="fa fa-check-circle"></i> sindromul picioarelor neliniștite </div>
															 <div><i class="fa fa-check-circle"></i> durere de-a lungul venelor dilatate</div>


                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														








<p>Insuficiența avansată a venelor membrelor inferioare apare, de obicei, la câțiva ani după primele simptome, cum ar fi venele în pânză de păianjen și se manifestă prin:
</p>


 














                                                            <div><i class="fa fa-check-circle"></i> ramurile dilatate ale venelor, numite varice, vizibile prin piele</div>
                                                            <div><i class="fa fa-check-circle"></i> umflarea picioarelor </div>
                                                            <div><i class="fa fa-check-circle"></i> modificări de culoare ale pielii, spre brun închis, în special în zona gleznei </div>
															<div><i class="fa fa-check-circle"></i> răni care nu se vindecă și ulcere </div>
															<div><i class="fa fa-check-circle"></i> eczeme venostatice  </div>


															
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a>
                                                            <li>Diagnosticarea insuficienței venoase</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Punctul de pornire pentru diagnosticarea exactă a insuficienței venoase îl reprezintă o examinare clinică amănunțită - evaluarea simptomelor pacientului și decelarea semnelor de boală. Confirmarea diagnosticului se realizează printr-o ecografie Doppler a sistemului venos, efectuată de un ultrasonografist cu experiență, care lucrează în principal în domeniul flebologiei. 



                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                    
                              
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Tratamentul insuficienței venoase</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Medicina modernă a dezvoltat o gamă largă de metode pentru tratarea insuficienței venoase a membrelor inferioare. Fiecare pacient ar trebui să înceapă schimbându-și obișnuința de zi cu zi de a sta perioade îndelungate așezat pe scaun sau în picioare.</p><p>

Când este deja prea târziu pentru profilaxie, terapia de compresie este recomandată. Ciorapi sau șosete elastice alese de profesioniști, în mod special pentru fiecare pacient, vor aduce o ușurare a simptomelor și o îmbunătățire a circulației venoase.
</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     
<p>Dacă vene varicoase s-au dezvoltat deja, este necesar un tratament mai avansat. <strong>Tehnicile pe care le aplicăm includ:</strong></p>

<div><i class="fa fa-check-circle"></i> microscleroterapie cu radiofrecvență</div>
                                                            <div><i class="fa fa-check-circle"></i> ablația endovenoasă cu laser  </div>
                                                            <div><i class="fa fa-check-circle"></i> proceduri chirurgicale (miniflebectomii, stripping) </div>




                                                                
                                                              

                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-venelor/tratament-cu-varice-cu-laser.jpg" alt="tratament laser varice">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>disconfortul, senzația de picioare grele, crampele nocturne, sindromul picioarelor neliniștite sunt primele simptome ale insuficienței venoase


</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>cea mai importantă este profilaxia: terapia prin compresie, evitarea statului în picioare sau pe scaun pentru mai multe ore, evitarea băilor fierbinți 


</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>investigația de bază efectuată înainte de orice procedură venoasă, chiar și cele minim invazive, este egografia Doppler



</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>metodele moderne de tratament nu necesită perioade lungi de imobilizare sau spitalizare și sunt, de obicei, efectuate în anestezie locală 



</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>