<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Accesul vascular pentru hemodializă | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b3.2.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>




Hemodializa este un tratament pentru filtrarea deșeurilor și a apei din sânge, așa cum rinichii fac când sunt sănătoși. Hemodializa ajută la controlul tensiunii arteriale și la echilibrarea mineralelor importante din sânge, precum potasiu, sodiu și calciu.







								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. George Pătruț">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. George Pătruț</h5>
                                                        <p class="bl-dr">
                                                          “Un pas important înainte de începerea tratamentului de hemodializă este efectuarea unei intervenții chirurgicale minore pentru a crea un acces vascular.”





                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">DESPRE HEMODIALIZĂ</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/accesul-vascular/hemodializa.jpg" alt="hemodializa">
                                    </div>

                                    <div class="entry-content">

                           

<p>În timpul hemodializei, sângele trece printr-un filtru, numit dializor, în afara corpului. Un dializator este denumit uneori și "rinichi artificial". La începutul procedurii de hemodializă, o asistentă medicală plasează două ace în venele de la braț. Fiecare ac este atașat la un tub conectat la aparatul de dializă. </p><p>

Aparatul de dializă pompează sângele prin filtru, apoi îl întoarce în corp. În timpul procesului, aparatul de dializă verifică tensiunea arterială și controlează cât de repede sângele trece prin filtru. Sângele intră la un capăt al filtrului și este împins forțat în multe tuburi goale, foarte subțiri. Pe măsură ce sângele trece prin tuburi, soluția de dializă trece în direcția opusă pe exteriorul acestor conducte. Produsele uzate din sânge se deplasează în soluția de dializă. Sângele filtrat rămâne în interiorul tubulețelor și se întoarce în corp. </p><p>Nefrologul va prescrie o soluție de dializă în funcție de analizele de sânge. Soluția de dializă conține apă și substanțe chimice care sunt adăugate pentru a elimina în siguranță deșeurile, sare suplimentară etc. Medicul poate ajusta cantitatea substanțelor chimice din soluție dacă analizele arată că sângele pacientului are prea mult sau prea puțin din anumite minerale, cum ar fi potasiul sau calciul.


</p>










                                                                 

                                        

                                        
                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">DESPRE ACCESUL VASCULAR</h4>
                                        </div>

                                        
                                        <p>Accesul vascular este punctul de legătură dintre corp și dializor, locul unde acele sunt inserate pentru a realiza circuitul. Aparatul de dializă lucrează la un debit foarte ridicat. El extrage și returnează în organism aproape jumătate de litru de sânge în fiecare minut. Există trei tipuri de acces vascular:
<p>
   <ul class="list-styled">                                  
<li>fistula arterio-venoasă (FAV)</li>
<li>grefonul sintetic arterio-venos</li>
<li>cateterul temporar de hemodializă</li>
</ul>


									 
  <div class="text-center mb-0 mt-90">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">FISTULA ARTERIO-VENOASĂ
</h4>
                                        </div>
										
										<p>Cel mai bun tip de acces vascular pe termen lung este o fistulă arterio-venoasă. Chirurgul conectează o arteră la o venă, de obicei în braț, pentru a crea o fistulă AV. O arteră este un vas de sânge care transporta sângele oxigenat dinspre inimă spre restul corpului, iar o venă este un vas de sânge care readuce sânge înapoi spre inimă. </p><p>Atunci când chirurgul conectează o arteră la o venă, venele devin mai largi și mai groase, făcând mai ușor plasarea acelor pentru dializă. Fistula AV are, de asemenea, un diametru mare care permite sângelui să curgă din corp în aparatul de dializă și înapoi în corp rapid. Scopul este de a permite un flux sanguin ridicat, astfel încât cea mai mare cantitate de sânge să treacă prin dializor.
</p>		
<div class="item-media post-thumbnail alignwide bd-t mb-50">
                                            <img src="images/accesul-vascular/fistula-arteriovenoasa.jpg" alt="fistula arteriovenoasa">
                                        </div>
										<p><strong>Fistula AV este considerată cea mai bună opțiune deoarece aceasta:</strong></p>
<ul class="list-styled">                                  
<li>asigură fluxul cel mai ridicat de sânge pentru dializă</li>
<li>are un risc mai mic să se infecteze sau să se blocheze</li>
<li>are o durată de viață mai lungă</li></ul>

<p>Majoritatea pacienților pot merge acasă imediat după intervenție, în aceeași zi. Operația se realizează de obicei în anestezie locală, fiind amorțită doar zona în care chirurgul creează fistula AV, dar, în funcție de situația pacientului, se poate opta pentru anestezie generală.</p>
<div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">GREFONUL SINTETIC ARTERIO-VENOS</h4>
                                        </div>
										<p>Dacă venele nu permit realizarea unei fistule AV, se poate opta pentru implantarea unui grefon AV. În acest caz, chirurgul utilizează un tub artificial pentru a conecta o arteră la o venă. Un grefon AV pentru dializă poate fi utilizat la scurt timp după intervenția chirurgicală. </p><p>Cu toate acestea, există un risc mai ridicat de infecție și de formare a cheagurilor de sânge în grefon. Cheagurile pot bloca fluxul de sânge prin grefon și pot face dificilă sau imposibilă dializa.
</p>
<div class="item-media post-thumbnail alignwide bd-t mb-50">
                                            <img src="images/accesul-vascular/grefonul-sintetic-arterio-venos.jpg" alt="grefonul sintetic arterio-venos">
                                        </div>
										 <div class="text-center mb-0 mt-90">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">CATETERUL TEMPORAR DE HEMODIALIZĂ
</h4>
                                        </div>
										<p>Dacă boala de rinichi progresează rapid și nu este timp pentru efectuarea unei proceduri de acces vascular înainte de dializă, este necesară implantarea unui cateter venos - un tub mic și moale inserat într-o venă în gât, piept sau picior, ca acces temporar. 

</p>
<div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/accesul-vascular/cateter-de-hemodializa.jpg" alt="cateter de hemodializa">
                                        </div>
										 <div class="text-center mb-0 mt-90">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">POSIBILE COMPLICAȚII
</h4>
                                        </div>
										<p>Orice tip de acces vascular poate dezvolta complicații, cele mai frecvente fiind:</p>
<ul class="list-styled">
										<li>infecția plăgii operatorii sau a grefonului/cateterului temporar</li>
<li>flux sanguin scăzut sau blocaj total, cauzate de un cheag de sânge sau de cicatrice.</li>


		</ul>		
<p>Aceste probleme pot afecta tratamentul prin hemodializă, fiind nevoie de mai multe proceduri pentru a înlocui sau repara accesul pentru ca acesta să funcționeze corect.</p>	

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>hemodializa este un tratament pentru filtrarea deșeurilor și a apei din sânge, așa cum rinichii fac când sunt sănătoși




</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>accesul vascular este punctul de legătură dintre corp și aparatul de dializă, asigurând un flux sangvin crescut, necesar pentru succesul procedurii






</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>există trei tipuri de acces vascular: fistula arterio-venoasă, grefonul sintetic arterio-venos și cateterul temporar de hemodializă







</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>infecția plăgii operatorii și înfundarea accesului cu un cheag de sânge sunt complicații rare, dar posibile 





</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>