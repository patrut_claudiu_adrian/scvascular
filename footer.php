<footer class="page_footer ds s-pt-50 s-pb-60 c-gutter-60">
  <div class="container">
    <div class="row">
      <div class="divider-20 d-none d-xl-block"></div>
      <div class="col-md-6 col-xl-3 ">
        <div class="widget widget_text "> <img src="images/logo.png" alt="logo scvascular chirurgie vasculara timisoara">
          <p>"O singură durere este ușor de suportat, cea a altora" - Rene Leriche </p>
        </div>
      </div>
      <div class="col-md-6 col-xl-2 ">
        <div class="widget widget_working_hours">
          <ul class="list-not-style">
            <li> <a href="despre-noi.htm">Despre Noi</a> </li>
            <li> <a href="bolile-arterelor.htm">Bolile arterelor</a> </li>
            <li> <a href="bolile-venelor.htm">Bolile venelor</a> </li>
            <li> <a href="accesul-vascular.htm">Accesul vascular</a> </li>
          </ul>
        </div>
      </div>
      <div class="col-md-6 col-xl-1">
        <div class="widget widget_working_hours">
          <ul class="list-not-style">
            <li> <a href="resurse-pacienti.htm">Pacienti</a> </li>
            <li> <a href="blog.htm">Blog</a> </li>
            <li> <a href="contact.htm">Contact</a> </li>
          </ul>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 ">
        <div class="widget widget_icons_list">
          <div class="media side-icon-box">
            <div class="icon-styled fs-14"> <i class="ico icon-facebook-placeholder-for-locate-places-on-maps"></i> </div>
            <p class="media-body">Timisoara, Arad, Deva, Oradea</p>
          </div>
          <div class="media side-icon-box">
            <div class="icon-styled fs-14"> <i class="ico icon-phone-receiver"></i> </div>

			 <p class="media-body">Dr. George Pătruț - 0754 099 761 </p>
          </div>
          <div class="media side-icon-box">
            <div class="icon-styled fs-14"> <i class="fa fa-envelope"></i> </div>
            <p class="media-body"> contact@scvascular.ro </p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-3 ">
        <div class="widget widget_mailchimp">
          <p> Abonare Newsletter </p>
          <form class="signup" method="post">
            <label for="mail_email"> <span class="screen-reader-text">Abonare:</span> </label>
            <input id="mail_email" name="email" required type="email" class="form-control mail_email"  placeholder="Tasteaza email-ul">
            <button type="submit" class="search-submit"> <span class="screen-reader-text"> Ma abonez</span> </button>
            <div class="response"></div>
          </form>
        </div>
        <div class="row c-gutter-30">
          <div class="col-sm-12 col-xl-6"> <a href="https://www.facebook.com/SacreCoeurVascular/" target="_blank" class="fa fa-facebook" title="facebook"><span>Facebook</span></a> </div>
          <div class="col-sm-12 col-xl-6"> <a href="#" class="fab fa-linkedin-in" title="linkedin"><span>Linkedin</span></a> </div>
        </div>
      </div>
      <div class="col-md-12 col-xl-12 pt-50 bd-t">
        <p class="copyright"><span class="copyright_year">&copy; 2019</span> ImPULS. Toate drepturile sunt rezervate. <a href="http://www.webstage.ro">Site de prezentare realizate de Webstage</a>.</p>
      </div>
    </div>
  </div>
</footer>
