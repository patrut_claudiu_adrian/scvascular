<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Boala arterială periferică a membrelor inferioare | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b1.4.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>



Boala arterială periferică a membrelor inferioare (denumită și arteriopatie cronică obliterantă) este o problemă circulatorie frecventă, apărută odată cu înaintarea în vârstă. Pacienții suferinzi de boală arterială periferică au un flux sanguin alterat către membrele inferioare. Acest lucru cauzează simptome, mai ales dureri în mușchii piciorului la mers (claudicație).


								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-marius-manda.jpg" alt="Dr. Marius Manda">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. Marius Manda</h5>
                                                        <p class="bl-dr">
                                                            “Boala arterială periferică este un semn al acumulării pe scară largă a colesterolului în arterele corpului (ateroscleroza) putând fi afectate și vasele din inimă și creier.”

                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-arterelor/boala-arteriala-periferica-a-membrelor-inferioare.jpg" alt="dureri picioare boala arteriala periferica">
                                    </div>

                                    <div class="entry-content">

                                        <p>Boala arterială periferică este adesea cauzată de ateroscleroză. În ateroscleroză, depozite de colesterol (plăcile) se acumulează pe pereții arterelor și reduc fluxul de sânge.</p><p>

Deși discuțiile despre ateroscleroză se concentrează de obicei asupra inimii, boala afectează frecvent arterele din întregul corp. Când ateroscleroza apare în arterele care alimentează cu sânge membrele inferioare provoacă boală arterială periferică.</p><p>

Mai puțin frecvent, cauza bolii arterei periferice poate fi inflamația vaselor de sânge, leziuni traumatice ale membrelor, anatomia neobișnuită a ligamentelor sau a mușchilor sau expunerea la radiații. 


                                        </p>
                                      

                                        

                                        <h4 class="oswald">Factorii de risc</h4>
                                        <ul class="list-styled">
										<li>Fumatul</li>
<li>Diabetul zaharat</li>
<li>Obezitatea (un indice de masă corporală de peste 30)</li>
<li>Tensiune arterială crescută</li>
<li>Colesterolul ridicat</li>
<li>Înaintarea în vârstă, mai ales după 50 de ani</li>
<li>Istoric familial de boală arterială periferică, boli cardiace sau accident vascular cerebral</li>
<li>Niveluri ridicate de homocisteină, o componentă proteică necesară în formarea și repararea țesuturilor
</li>
                                        </ul>
										<p>Persoanele care fumează sau suferă de diabet au cel mai mare risc de a dezvolta boală arterială periferică.
</p>

                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-arterelor/boala-arteriala-periferica.jpg" alt="doctor picior boala arteriala periferica">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Simptome</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Cel mai frecvent apar dureri musculare sau crampe, care sunt declanșate de activitate, cum ar fi mersul pe jos, dar dispar după câteva minute de odihnă. Localizarea durerii depinde de localizarea arterei înfundate sau îngustate. Durerea din gambă este cea mai obișnuită localizare.


</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														








<p>Semnele și simptomele bolii arteriale periferice includ:</p>


 









                                                            <div><i class="fa fa-check-circle"></i> Crampe dureroase la nivelul unuia sau a ambelor șolduri, coapse sau gambe după anumite activități, cum ar fi urcatul scărilor sau mersul pe jos (claudicație);</div>
                                                            <div><i class="fa fa-check-circle"></i> Amorțeli </div>
                                                            <div><i class="fa fa-check-circle"></i> Răceală a labei piciorului </div>
															<div><i class="fa fa-check-circle"></i> Răni pe degetele de la picioare, pe laba piciorului sau pe gambă care nu se vindecă; </div>
															<div><i class="fa fa-check-circle"></i> O schimbare a culorii picioarelor spre vinețiu;  </div>
															 <div><i class="fa fa-check-circle"></i> Căderea părului sau creșterea lentă a părului pe picioare;</div>
                                                            <div><i class="fa fa-check-circle"></i> Creșterea mai lentă a unghiilor; </div>
                                                            <div><i class="fa fa-check-circle"></i> Piele strălucitoare pe picioare; </div>
															<div><i class="fa fa-check-circle"></i> Lipsa pulsului sau un puls slab la arterele de la picioare; </div>
															<div><i class="fa fa-check-circle"></i> Disfuncția erectilă la bărbați.  </div><br>
															<p>Dacă boala arterială periferică progresează, durerea poate apărea chiar și atunci când stați sau când dormiți (durere ischemică de repaus). </p>

															
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse02" class="collapsed">
                                                            <li>Diagnosticarea bolii</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Unele dintre testele la care medicul dumneavoastră poate apela pentru a diagnostica boala arterială periferică sunt:


                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">










<div><i class="fa fa-check-circle"></i> <strong>Examenul clinic.</strong> Medicul poate găsi semne ale bolii în cursul unui examen fizic, cum ar fi un puls slab sau absent sub o zonă îngustă a arterei, sunete (sufluri) pe artere și scăderea tensiunii arteriale în membrele afectate.</div>
<div><i class="fa fa-check-circle"></i> <strong>Indicele gleznă-braț (IGB)</strong> este un test comun folosit pentru a diagnostica boala. Testul compară tensiunea arterială la nivelul gleznei cu tensiunea arterială din braț. </div>
<div><i class="fa fa-check-circle"></i> <strong>Ecografia Doppler</strong> evaluează fluxul sanguin prin vasele de sânge și identifică arterele blocate sau îngustate.</div>
<div><i class="fa fa-check-circle"></i> <strong>Angiografia.</strong> Folosind un colorant special (material de contrast) injectat într-o venă de la mână, acest test va permite medicului să vizualizeze direct arterele folosind tehnici de imagistică numite angiografie prin rezonanță magnetică (Angio-RMN) sau  tomografie computerizată(Angio-CT).</div>
<div><i class="fa fa-check-circle"></i> <strong>Angiografia prin cateter</strong> (tehnica Seldinger) este o procedură mai invazivă, care implică introducerea unui cateter prin artera femurală (artera de la baza piciorului) și avansarea acestuia până în zona afectată, apoi injectarea colorantului prin cateter. Deși invaziv, acest tip de angiografie permite diagnosticarea și tratamentul simultan. </div>
<div><i class="fa fa-check-circle"></i> <strong>Analize de sânge.</strong> O mostră din sângele dumneavoastră poate fi utilizată pentru a măsura colesterolul și trigliceridele și pentru a verifica dacă aveți diabet.</div>

                                                        
                                                           
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Complicații</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p><strong>Ischemia critică a membrelor</strong> debutează cu răni deschise care nu se vindecă sau infecții la nivelul degetelor și labei piciorului. Din cauza fluxului scăzut de sânge, aceste leziuni sau infecții progresează și provoacă moartea țesutului (gangrena), uneori necesitând amputarea membrelor afectate.</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     
<p><strong>Accidentul vascular cerebral și infarctul miocardic.</strong> Ateroscleroza care cauzează semnele și simptomele bolii arteriale periferice nu se limitează doar la picioare. Depozitele de grăsimi se acumulează, de asemenea, în arterele care alimentează cu sânge inima și creierul</p>

                                                                
                                                              

                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-arterelor/gangrena-piciorului.jpg" alt="gangrena picior">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratament</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Tratamentul pentru boala arterială periferică are două obiective majore:</p>
<div><i class="fa fa-check-circle"></i> Ameliorarea sau eliminarea simptomelor, cum ar fi durerea la mers, astfel încât să vă puteți relua activitățile fizice obișnuite;</div>
<div><i class="fa fa-check-circle"></i> Oprirea progresiei aterosclerozei în întregul corp pentru a reduce riscul de infarct miocardic și accident vascular cerebral</div>


                                                            
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														<p>Este posibil să puteți atinge aceste obiective adoptând modificări ale stilului de viață, în special în stadiile incipiente ale bolii:</p>

                                                           
															
                                                        <div><i class="fa fa-check-circle"></i> <strong>Renunțați la fumat.</strong> Fumatul contribuie la constricția și deteriorarea arterelor și este un factor de risc semnificativ pentru dezvoltarea și agravarea bolii arteriale periferice. Dacă fumați, renunțarea este cel mai important lucru pe care îl puteți face pentru a reduce riscul de complicații. Dacă aveți probleme cu renunțarea pe cont propriu, adresați-vă medicului dumneavoastră despre opțiunile de renunțare la fumat, inclusiv medicamente care vă ajută să depășiți simptomele de sevraj.

</div>
                                                        <div><i class="fa fa-check-circle"></i> <strong>Faceți exerciții fizice regulat.</strong> Aceasta este o componentă cheie. Succesul în tratamentul bolii arteriale periferice este adesea măsurat în funcție de cât de departe puteți merge fără durere. Exercițiul adecvat vă ajută musculatura să utilizeze mai eficient oxigenul. Medicul vă poate ajuta să dezvoltați un plan de exerciții corect. 
</div>



													
															
                                                        <div><i class="fa fa-check-circle"></i> <strong>Mâncați sănătos.</strong> O dietă sănătoasă, cu conținut scăzut de grăsimi saturate, vă poate ajuta să controlați tensiunea arterială și nivelul colesterolului, care contribuie la ateroscleroza.


</div>

<h4 class="oswald">MEDICAMENTE</h4>
<div><i class="fa fa-check-circle"></i> <strong>Hipolipemiantele.</strong> Aceste medicamente, numite generic statine, sunt prescrise pacienților care au colesterolul crescut.
</div>
<div><i class="fa fa-check-circle"></i> <strong>Antihipertensivele.</strong> Dacă aveți hipertensiune arterială, medicul dumneavoastră vă va prescrie medicamente pentru a o reduce. </div>
<div><i class="fa fa-check-circle"></i> <strong>Hipoglicemiantele.</strong> Dacă aveți diabet și boală arterială periferică, devine și mai important să controlați nivelul glucozei din sânge (glicemia). </div>
<div><i class="fa fa-check-circle"></i> <strong>Antiagregantele.</strong> Deoarece boala arterială periferică este cauzată de scăderea fluxului de sânge, sunteți predispus la formarea de cheaguri în artere. Medicul dumneavoastră vă poate prescrie tratamentul zilnic cu aspirină sau un alt medicament, cum ar fi clopidogrel (Plavix).
</div>
<div><i class="fa fa-check-circle"></i> <strong>Simptomatice.</strong> Cilostazolul este un medicament care mărește fluxul sangvin către membre atât prin subțierea sângelui, cât și prin dilatarea arterelor. Acesta ajută în mod special la tratarea simptomelor de claudicație, mărind distanța de mers la care apare durerea. 
</div>
			

<h4 class="oswald">OPERAȚII</h4>
<div><i class="fa fa-check-circle"></i> <strong>Angioplastia.</strong> În această procedură, un tub mic (cateter) este avansat prin arborele vascular până la artera afectată. Acolo, un mic balon de pe vârful cateterului este umflat pentru redeschiderea arterei și aplatizarea plăcilor de aterom din peretele acesteia. De asemenea, medicul poate introduce un tub metalic, numit stent, în arteră pentru a o menține deschisă. 
</div>
<div><i class="fa fa-check-circle"></i> <strong>Bypass-ul.</strong> Medicul dumneavoastră poate crea un bypass folosind un vas dintr-o altă parte a corpului dumneavoastră sau un grefon fabricat din materiale sintetice. Această tehnică restabilește circulația sângelui prin ocolirea porțiunii din arteră blocată sau îngustată.</div>
<div><i class="fa fa-check-circle"></i> <strong>Terapia trombolitică.</strong> Dacă aveți un cheag de sânge care blochează o arteră, medicul dumneavoastră vă poate injecta un medicament care îl descompune.

</div>
		
                                                           
                                                           



                                                           <div class="item-media post-thumbnail alignwide bd-t mt-20">
                                                                <img src="images/bolile-arterelor/angioplastie-cu-balon.jpg" alt="angioplastie balon">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>durerea musculară la mers (claudicația) reprezintă cel mai precoce și comun simptom al bolii arteriale periferice


</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>renunțarea la fumat, adoptarea unei diete sănătoase și a unui program de exerciții fizice pot ameliora semnificativ simptomatologia în stadiile incipiente ale bolii 

</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>investigația de bază efectuată înainte de orice procedură chirurgicală, chiar și cele minim invazive, este angio-CT-ul


</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>metodele moderne de tratament endovascular nu necesită perioade lungi de imobilizare sau spitalizare și sunt, de obicei, efectuate în anestezie locală 


</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>