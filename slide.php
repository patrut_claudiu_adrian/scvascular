<section class="page_slider">
  <div class="flexslider">
    <ul class="slides">
      <li class="cs cover-image flex-slide"> <img src="images/slide01.jpg" alt="">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="intro_layers_wrapper">
                <div class="col-md-5">
                  <div class="intro_layers">
                    <div class="intro_layer" data-animation="fadeInRight">
                      <h1 style="font-weight:300; font-size:35px; font-family: 'Oswald', sans-serif;"> "O singură durere este ușor de suportat, cea a altora" </h1>
                      <h1 class="after-title" style="font-size:28px; font-family: 'Oswald', sans-serif;"> Rene Leriche </h1>
                    </div>
                    <div class="intro_layer" data-animation="fadeInUp">
                      <ul class="slider-list">
                        <li><a href="resurse-pacienti.htm">Resurse pentru pacienți</a></li>
                        <li><a href="contact.htm">Programează-te la o consultație</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</section>
<section class="ls teaser-box-section animate" data-animation="scaleAppear">
  <div class="container">
    <div class="row c-gutter-8">
      <div class="col-sm-4"> <a class="text-center py-35 box-shadow mb-20 teaser" href="#medici">
        <h5 class="fw-300 oswald"><i class="ico icon-heart fs-40 px-10"></i>Faceți cunoștință cu doctorii</h5>
        </a> </div>
      <div class="col-sm-4"> <a class="text-center py-35 box-shadow mb-20 cs teaser" href="contact.htm">
        <h5 class="fw-300 oswald"><i class="ico icon-helping fs-40 px-10"></i>Solicită o consultație</h5>
        </a> </div>
      <div class="col-sm-4"> <a class="text-center py-35 box-shadow mb-20 teaser" href="#locatii">
        <h5 class="fw-300 oswald"><i class="ico icon-location fs-40 px-10"></i>Unde ne găsiți</h5>
        </a> </div>
    </div>
  </div>
</section>