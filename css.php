<link rel="canonical" href="<?php $url=substr('https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'],0,-3); echo $url.'htm'; ?>" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148884497-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-148884497-1');
</script>

<link rel="shortcut icon" type="image/png" href="/images/fav.png"/>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/animations.css">
<link rel="stylesheet" href="/css/regular.min.css">
<link rel="stylesheet" href="/css/brands.min.css">
<link rel="stylesheet" href="/css/solid.min.css">
<link rel="stylesheet" href="/css/font-awesome.css">
<link rel="stylesheet" href="/css/main.css">
<script src="js/vendor/modernizr-custom.js"></script>
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,700&display=swap" rel="stylesheet">
<!--[if lt IE 9]>
		<script src="js/vendor/html5shiv.min.js"></script>
		<script src="js/vendor/respond.min.js"></script>
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<![endif]-->
	
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '165244683491183',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v4.0'
    });
  };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v4.0&appId=165244683491183"></script>
<meta property="fb:admins" content="2607924635917253"/>
<meta property="fb:admins" content="10216894303163782"/>
<meta property="fb:admins" content="2801186063233190"/>