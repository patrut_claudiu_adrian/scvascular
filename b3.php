<section class="page_title about padding-mobile cs s-pt-40 s-pb-40 cover-background2" style="background-image: url('images/bg/bg5.jpg');">
	<div class="container">
		<div class="row">


			<div class="col-md-6 text-left animate head-title" data-animation="fadeInUp">
				<div class="col-xl-9 col-md-12 pl-0 text-uppercase">
					<p>Educație in sănătate</p>
				</div>
				<div class="col-xl-8 col-md-12 pl-0">
					<h1 class="text-uppercase">Accesul vascular</h1>
				</div>
				<div class="col-xl-9 col-md-12 pl-0">
					<p>Dispozitivele de acces vascular permit un abord facil al sistemului circulator, eliminând necesitatea unor înțepături repetate ale vaselor de sânge.</p>
				</div>
			</div>

			<div class="col-md-6 text-center animate d-none d-xl-block" data-animation="fadeInUp">
				<div class="head-elements row">

					<div class="col-lg-3">

					</div>

					<div class="col-lg-3">
						<a href="accesul-vascular-pentru-chimioterapie.htm">
							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Accesul vascular pentru chimioterapie</h6>

							</div>
						</a>
					</div>






				</div>
				<div class="head-elements row">



					<div class="col-lg-3">
						<a href="">
							<div class="icon-box text-center box-shadow">



							</div>
						</a>
					</div>

					<div class="col-lg-3">



					</div>
					<div class="col-lg-3">
						<a href="accesul-vascular-pentru-hemodializa.htm">
							<div class="icon-box text-center hero-bg1 box-shadow">

								<h6 class="fw-300">Accesul vascular pentru hemodializă</h6>

							</div>
						</a>
					</div>


				</div>
			</div>
		</div>
	</div>
</section>