<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Ischemia critică a membrelor inferioare | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b1.5.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>


Ischemia critică reprezintă o obstrucție severă a arterelor membrelor inferioare care reduce semnificativ fluxul sanguin, provocând durere severă și chiar răni ale pielii. Durerea cauzată de ischemie poate să trezească o persoană pe timp de noapte din somn. Se localizează adesea în laba piciorului și poate fi ușurată temporar prin atârnarea piciorului la marginea patului sau prin efectuarea a câtorva pași prin cameră.

								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                            <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. George Patrut">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. George Pătruț</h5>
                                                        <p class="bl-dr">
                                                            “Ischemia critică este o afecțiune foarte severă și necesită un tratament complex de către un chirurg vascular. Această boală nu se va vindeca de la sine.”
                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-arterelor/ischemia-critica-a-membrelor-inferioare.jpg" alt="ischemia-critica-a-membrelor-inferioare.jpg">
                                    </div>

                                      <div class="entry-content">

                                        <p>Ischemia critică a membrelor inferioare reprezintă stadiul avansat al bolii arteriale periferice și este produsă prin îngroșare progresivă a pereților arterelor, cauzată de acumularea de colesterol. Această acumulare de colesterol, cunoscută și sub denumirea de ateroscleroză, îngreunează sau blochează fluxul sanguin, reducând circulația sângelui în picioare. 
</p>
                                      

                                        

                                        <h4 class="oswald">Factorii de risc</h4>
                                        <ul class="list-styled">
										<li>Fumatul</li>
<li>Diabetul zaharat</li>
<li>Obezitatea (un indice de masă corporală de peste 30)</li>
<li>Tensiune arterială crescută</li>
<li>Colesterolul ridicat</li>
<li>Înaintarea în vârstă, mai ales după 50 de ani</li>
<li>Istoric familial de boală arterială periferică, boli cardiace sau accident vascular cerebral</li>
                                        </ul>
										<p>Persoanele care fumează sau suferă de diabet au cel mai mare risc de a dezvolta boală arterială periferică și ischemie critică.

</p>

                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-arterelor/durere-picior.jpg" alt="durere-picior">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Simptome</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Semnele și simptomele ischemiei critice includ:


</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														
















                                                            <div><i class="fa fa-check-circle"></i> durere severă, însoțită de amorțeli în laba piciorului, la repaus</div>
                                                            <div><i class="fa fa-check-circle"></i> o scădere considerabilă a temperaturii labei piciorului față de restul corpului </div>
                                                            <div><i class="fa fa-check-circle"></i> răni, infecții sau ulcere care nu se vindecă sau se vindecă foarte lent </div>
															<div><i class="fa fa-check-circle"></i> gangrenă </div>
															<div><i class="fa fa-check-circle"></i> piele lucioasă, netedă și uscată pe laba piciorului sau degete  </div>
															 <div><i class="fa fa-check-circle"></i> îngroșarea unghiilor</div>
                                                            <div><i class="fa fa-check-circle"></i> pulsuri absente sub genunchi </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse02" class="collapsed">
                                                            <li>Diagnosticarea bolii</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Unele dintre testele la care medicul dumneavoastră poate apela pentru a diagnostica ischemia critică sunt:


                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">










<div><i class="fa fa-check-circle"></i> <strong>Examenul clinic.</strong> Medicul poate găsi semne ale bolii în cursul unui examen fizic, cum ar fi un puls slab sau absent sub o zonă îngustă a arterei, sunete (sufluri) pe artere și scăderea tensiunii arteriale în membrele afectate.</div>
<div><i class="fa fa-check-circle"></i> <strong>Indicele gleznă-braț (IGB)</strong> este un test comun folosit pentru a diagnostica boala. Testul compară tensiunea arterială la nivelul gleznei cu tensiunea arterială din braț. </div>
<div><i class="fa fa-check-circle"></i> <strong>Ecografia Doppler</strong> evaluează fluxul sanguin prin vasele de sânge și identifică arterele blocate sau îngustate.</div>
<div><i class="fa fa-check-circle"></i> <strong>Angiografia.</strong> Folosind un colorant special (material de contrast) injectat într-o venă de la mână, acest test va permite medicului să vizualizeze direct arterele folosind tehnici de imagistică numite angiografie prin rezonanță magnetică (Angio-RMN) sau  tomografie computerizată(Angio-CT).</div>
<div><i class="fa fa-check-circle"></i> <strong>Angiografia prin cateter</strong> (tehnica Seldinger) este o procedură mai invazivă, care implică introducerea unui cateter prin artera femurală (artera de la baza piciorului) și avansarea acestuia până în zona afectată, apoi injectarea colorantului prin cateter. Deși invaziv, acest tip de angiografie permite diagnosticarea și tratamentul simultan. </div>


                                                        
                                                           
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Complicații</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Ischemia critică progresează rapid, astfel că o mică rană pe unul dintre degete se poate suprainfecta și extinde, cuprinzând zonele învecinate. În lipsa tratamentului de restabilire a circulației țesuturile mor și se instalează gangrena extensivă, rezultatul fiind un picior care nu va mai putea fi salvat, necesitând o amputație majoră.
</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     


                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-arterelor/ischemia-critica-picior.jpg" alt="ischemia-critica-picior">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratament</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Tratamentul pentru ischemia critică poate fi destul de complex și individualizat, dar obiectivul general ar trebui întotdeauna să fie reducerea durerii și îmbunătățirea fluxului sanguin pentru salvarea piciorului. Prioritatea numărul unu este conservarea membrelor.


                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														<p>Tratamentele includ următoarele:</p>

                                                           
															
                                                        <div><i class="fa fa-check-circle"></i> <strong>Medicamente:</strong> pot fi prescrise mai multe medicamente pentru a preveni progresia bolii și pentru a reduce efectul factorilor de risc, cum ar fi hipertensiunea arterială, colesterolul ridicat și diabetul. Medicamentele care împiedică coagularea sau lupta împotriva infecțiilor pot fi, de asemenea, prescrise.

</div>
                                                        <div><i class="fa fa-check-circle"></i> <strong>Tratamente endovasculare:</strong> Aceste tratamente sunt cele mai puțin invazive și implică introducerea unui cateter cu balon în porțiunea bolnavă a arterei. Balonul este umflat și, pe măsură ce se umflă, deschide artera pentru un flux sanguin îmbunătățit. Poate fi introdus și un dispozitiv metalic, numit stent, pentru a menține artera deschisă. Alte tratamente includ aterectomia cu laser, în care bucăți mici de placă sunt vaporizate de vârful unei sonde laser și aterectomia direcțională, în care un cateter cu o lamă de tăiere rotativă este utilizat pentru a îndepărta fizic placa din arteră.

</div>


<div><i class="fa fa-check-circle"></i> <strong>Chirurgia arterială:</strong> Dacă blocajul arterial nu este favorabil terapiei endovasculare, este adesea recomandată intervenția chirurgicală - bypass-ul. Aceasta implică ocolirea zonei bolnave din arteră utilizând fie cu o venă de la pacient, fie o grefă sintetică. În cazuri rare, chirurgul poate să deschidă artera și să scoată placa, menținând artera utilizabilă. Ultima opțiune terapeutică este amputația - a unui deget de la picior, a unei părți a labei piciorului sau a piciorului în întregime, dacă infecția și gangrena au progresat atât de mult încât țesuturile respective nu mai pot fi salvate. </div>


<p>Deoarece tratamentul depinde de gravitatea bolii și de mulți parametri individuali, este esențial ca cineva cu ulcere sau dureri la nivelul picioarelor să consulte un chirurg vascular cât mai curând posibil. Cu cât mai devreme poate fi pus un diagnostic, cu atât cresc șansele de salvare a membrului.
</p>

													
															
                             
                                                           



                                                           <div class="item-media post-thumbnail alignwide bd-t mt-20">
                                                                <img src="images/bolile-arterelor/angioplastie-cu-balon.jpg" alt="angioplastie balon">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-6 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>ischemia critică este faza finală de evoluție a bolii arteriale periferice și se manifestă prin dureri constante și apariția rănilor


</p>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>tratamentul implică atât refacerea circulației cât și îndepărtarea chirurgicală a țesuturilor moarte și infectate 

</p>
								</div>

							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>