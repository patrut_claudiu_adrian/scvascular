<section id="locatii" class="ls animate date-contact" data-animation="scaleAppear">
    <div class="cc5">
	<div class="cover-image s-cover-left"></div>
    <div class="container">
      <div class="row align-items-center c-gutter-60">
        <div class="col-md-12 col-lg-6">
          <div class="item-media">
            <div class="embed-responsive"> <a href="contact.htm"> <img src="images/harta.jpg" alt="harta locatii"> </a> </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6">
          <div class="divider-30 divider-md-70 divider-xl-75"></div>
          <h3 class="mt-0 oswald"><span class="color-main">Unde ne găsiți? </span> <br>
            ImPULS</h3>
<div class="divider-30 divider-md-70 divider-xl-75"></div>
          <div class="row c-gutter-30">
            <div class="col-md-12 col-lg-6">
			<a href="" data-toggle="modal" data-target="#map1_modal">
              <div class="icon-box">
                <div class="media">
				
                  <div class="icon-styled color-main fs-24"> <i class="ico icon-location fs-40"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> <strong>Timisoara [ Harta ]</strong><br> Clinica Cardiotim</h6>
                    <p>Calea Buziasului Nr. 84</p>
                  </div>
				 
                </div>
              </div>
			   </a>
              <div class="divider-30 divider-lg-42"></div>
			  <a href="" data-toggle="modal" data-target="#map2_modal">
              <div class="icon-box">
                <div class="media">
                  <div class="icon-styled color-main fs-24"> <i class="ico icon-location fs-40"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> <strong>Arad [ Harta ]</strong><br> Clinica CardioTim </h6>
                    <p> Calea Timisorii Nr. 30</p>
                  </div>
                </div>
              </div>
			  </a>
            </div>
            <div class="divider-30 d-lg-none d-md-block"></div>
            <div class="col-md-12 col-lg-6">
			<a href="" data-toggle="modal" data-target="#map3_modal">
              <div class="icon-box">
                <div class="media">
                  <div class="icon-styled color-main fs-24"> <i class="ico icon-location fs-40"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> <strong>Deva [ Harta ]</strong><br> Derm Artis Clinic</h6>
                    <p> Derm Artis Clinic - Bvd. 22 Decembrie, Bloc 41</p>
                  </div>
                </div>
              </div>
			  </a>
              <div class="divider-30 divider-lg-42"></div>
              <div class="icon-box">
                <div class="media">
                  <div class="icon-styled color-main fs-24"> <i class="ico icon-phone-receiver fs-40"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> <strong>Telefoane medici:</strong> </h6>
                    <p> Dr. Manda: 0737 699 979<br>
Dr. Pătruț: 0754 099 761  </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="divider-50 divider-md-0 divider-xl-75"></div>
          </div>
        </div>
      </div>
    </div>
	
	</div>
  </section>