<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Sindromul posttrombotic | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b2.3.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>


Sindromul post-trombotic reprezintă spectrul tuturor simptomelor și consecințelor nefavorabile care rezultă în urma unui episod de tromboză venoasă profundă.





								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. George Pătruț">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. George Pătruț</h5>
                                                        <p class="bl-dr">
                                                            “Orice formă de insuficiență venoasă a membrelor inferioare fără o cauză clară reprezintă un potențial sindrom post-trombotic  nerecunoscut. ”




                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-venelor/sindromul-posttrombotic.jpg" alt="sindromul posttrombotic">
                                    </div>

                                    <div class="entry-content">

                           

<p>În urma unui episod trombotic, apar o serie de modificări în venele membrului afectat. Pe scurt, acestea includ:
</p>










<ul class="list-styled">
										<li>alterări ale circulației prin venele profunde, produse prin transformarea cheagurilor (persistența blocajului, recanalizarea parțială, sau fibroza, care duce la deteriorarea valvelor)</li>
<li>creșterea presiunii în venele subcutanate mici</li>
<li>creșterea presiunii și insuficiența venoasă consecutivă a venelor perforante</li>
<li>deteriorarea secundară a venelor superficiale</li>
<li>apariția tulburărilor trofice (de exemplu lipodermatoscleroza, eczemă, ulcer etc.)</li>
</ul>








                                                                 

                                        

                                        
                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-venelor/posttrombotic.jpg" alt="posttrombotic">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a>
                                                            <li>Simptomele sindromului post-trombotic avansat</li>
                                                        </a>

                                                        <div class="card-body">





<div><i class="fa fa-check-circle"></i> umflarea cronică (edem) a membrelor inferioare, cu dilatări de culoare albastru-cenușie ale vaselor din jurul gleznei (30-60% dintre pacienți)  </div>
                                                            <div><i class="fa fa-check-circle"></i> modificări ale pielii (colorație maronie, durificarea țesutului subcutanat)  </div>
															<div><i class="fa fa-check-circle"></i> ulcerații active sau semne de ulcere vindecate sub formă de cicatrici (15% dintre pacienți) </div>
															<div><i class="fa fa-check-circle"></i> durere la picioare în timpul mersului pe jos - claudicație venoasă </div><br>

                                                        </div>

                                                    </div>

                                                   
                                        
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a>
                                                            <li>Cât de frecvent apare sindromul post-trombotic după tromboza venoasă profundă?</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Tratamentul cu heparine cu greutate moleculară mică și purtarea ciorapilor compresivi cu tensiune moderată sau puternică (stadiul II sau III) reduc substanțial apariția simptomelor sindromului post-trombotic.</p><p>

Cu toate acestea, evoluția favorabilă a trombozei venoase, fără apariția sindromului post-trombotic, este totuși observată exclusiv în cazul trombozelor simple, apărute pe segmente venoase scurte. Un număr mare de tromboze se localizează pe mai multe segmente anatomice - în abdomen, la coapsă, sau gambă. Trombozele din segmentele femural sau pelvin evoluează mai frecvent spre sindrom post-trombotic. 






                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                    
                                                 
                              
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a>
                                                            <li>Ce fel de tratament este recomandat în sindromul post-trombotic avansat?</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Dacă este diagnosticat un sindrom post-trombotic avansat, se pot recomanda, în anumite cazuri, pe lângă purtarea obligatorie a ciorapilor elastici și proceduri chirurgicale specifice minim invazive care pot reduce simptomele:<p/>
<div><i class="fa fa-check-circle"></i> închiderea percutană a venelor perforante incompetente, care determină un reflux patologic din sistemul venos profund în cel superficial utilizând scleroterapia sau terapia cu laser intravascular</div>
<div><i class="fa fa-check-circle"></i> deschiderea venelor profunde afectate de episodul de tromboză, prin implantarea de stenturi venoase</div><br>

<p>Stabilirea deciziei de implantare a unui stent este precedată întotdeauna de efectuarea unei ecografii Doppler venoase și deseori a unei venografii CT sau RMN.


</p>
                                                        </div>

                                                    </div>

                                                    
                                                     <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-venelor/ciorapi-tromboza.jpg" alt="ciorapi tromboza">
                                        </div> 
                                                </div>

                                                

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>sindromul post-trombotic este o consecință serioasă a unui episod de tromboză venoasă profundă


</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>apare destul de frecvent, provocând discomfort, dureri și răni ale pielii pacientului




</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>poate fi prevenit prin inițierea rapidă a tratamentului anticoagulant și purtarea ciorapilor elastici





</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>în cazuri severe pot fi utilizate tehnici chirurgicale minim invazive pentru atenuarea simptomatologiei




</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>