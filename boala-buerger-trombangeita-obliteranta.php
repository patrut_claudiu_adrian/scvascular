<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Boala Buerger - trombangeita obliterantă | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b1.6.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>

Boala Buerger este o boală rară a arterelor și a venelor mici și medii. În boala Buerger - numită și tromboangeită obliterantă - vasele de sânge se inflamează și, în timp, sunt blocate cu cheaguri de sânge (trombi). Înfundarea vaselor de sânge cauzează, în cele din urmă, moartea pielii și țesuturilor și duce la infecții și la gangrenă. Boala se manifestă, la început, la mâini și picioare, în timp afectând zone din ce în ce mai mari ale brațelor și picioarelor.

								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                            <img src="images/echipa/doctor-marius-manda.jpg" alt="Dr. Marius Manda">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. Marius Manda</h5>
                                                        <p class="bl-dr">
                                                            “Practic toți cei diagnosticați cu boala lui Buerger fumează sau folosesc alte forme de tutun. Renunțarea la fumat este singura modalitate de a opri boala Buerger.”
                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-arterelor/boala-buerger.jpg" alt="trombangeita obliteranta">
                                    </div>

                                    <div class="entry-content">

                                        <p>Cauza exactă a bolii Buerger nu este cunoscută. În timp ce fumatul joacă un rol clar în dezvoltarea bolii, nu este clar cum se întâmplă acest lucru. Se crede că substanțele chimice din tutun pot irita stratul intern al vaselor de sânge, determinându-le să se inflameze. Se suspectează că unii oameni pot avea o predispoziție genetică la această boală. De asemenea, este posibil ca boala să fie cauzată de un răspuns autoimun, în care sistemul imunitar al organismului atacă greșit țesutul sănătos.
                                        </p>

                                           

                                        

                                        <h4 class="oswald">Factorii de risc</h4>
                                        <ul class="list-styled">
										
										




										<li><strong>Fumatul</strong> crește foarte mult riscul de boală Buerger, dar boala poate apărea și la persoanele care folosesc forme alternative de tutun, inclusiv trabucuri și tutun de mestecat</li>
<li><strong>Gingivita cronică</strong> - infecția pe termen lung a gingiilor - a fost legată de dezvoltarea bolii Buerger, deși legătura exactă nu este încă clară.</li>
<li><strong>Sexul masculin</strong> - boala Buerger este mult mai frecventă la bărbați decât la femei, posibil din cauza frecvenței mai mare a fumatului la bărbați.</li>
<li><strong>Vârsta</strong> - boala apare mai frecvent la persoanele tinere, cu vârsta sub 45 de ani.</li>
                                        </ul>

                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-arterelor/trombangeita-obliteranta.jpg" alt="trombangeita-obliteranta">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Simptome</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Simptomele bolii Buerger includ:
</p>
  <div><i class="fa fa-check-circle"></i> amorțeli la nivelul mâinilor sau picioarelor</div>
                                                            <div><i class="fa fa-check-circle"></i> mâinile sau picioarele devin palide, reci, cu zone de piele vineție</div>
                                                            <div><i class="fa fa-check-circle"></i> durere, care apare în picioare, brațe și palme la efort și dispare la repaus</div>
                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
	
                                                          
															<div><i class="fa fa-check-circle"></i> inflamație de-a lungul unei venele</div>
															<div><i class="fa fa-check-circle"></i> degetele de la mâini și picioare devin palide și dureroase când sunt expuse la frig (fenomen Raynaud)</div>
                                                            <div><i class="fa fa-check-circle"></i> plăgi ale pielii la nivelul degetelor, palmelor și picioarelor.</div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse02" class="collapsed">
                                                            <li>Diagnosticarea bolii</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Nicio analiză nu stabilește direct diagnosticul de boala Buerger. Acesta se pune prin excluderea tuturor bolilor care dau simptomatologie asemănătoare. În acest scop, medicul va efectua următoarele teste:</p>

  
                                                            
                                                        </div>

                                                    </div>

                                                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                       <div><i class="fa fa-check-circle"></i> <strong>Analize de sânge.</strong> Analizele de sânge efectuate pentru a căuta anumite substanțe pot exclude alte boli care pot provoca semne și simptome similare. De exemplu, testele de sânge pot ajuta la excluderea bolilor autoimune, cum ar fi sclerodermia sau lupusul, tulburările de coagulare a sângelui și diabetul.
</div>
                                                                <div><i class="fa fa-check-circle"></i> <strong>Testul lui Allen.</strong> Medicul poate efectua un test simplu, numit testul lui Allen, pentru a verifica fluxul sanguin prin arterele care transportă sângele în mâini. Inițial trebuie să strângeți pumnul puternic, pentru a goli palma de sânge, aceasta devenind palidă. Medicul va apăsa pe cele două artere de la încheietura mâinii, pentru a opri reintrarea sângelui în palmă. Apoi, veți deschide mâna și medicul eliberează presiunea pe o arteră, apoi pe cealaltă. Timpul de recolorare a mâinii indică starea generală de sănătate a arterelor. Recolorarea lentă a pielii palmei ridică suspiciunea de boală Buerger.
</div>
																<div><i class="fa fa-check-circle"></i> <strong>Angiografia.</strong> Prin angiografie se vizualizează starea arterelor. O angiografie poate fi făcută neinvaziv cu ajutorul scanării CT sau RMN, sau se poate face prin introducerea unui cateter într-o arteră. În timpul acestei proceduri, se injectează un colorant special care ajută la vizualizarea mai ușoară a blocajelor arterelor. Medicul poate să solicite angiografii atât ale brațelor cât și ale picioarelor pentru că boala Buerger afectează aproape întotdeauna mai mult de un membru.

</div>
                                                        
                                                           
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Complicații</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Dacă boala Buerger se agravează, fluxul sanguin scade, din cauza blocajelor din artere care fac dificil ca sângele să ajungă până la vârful degetelor. 
</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     

                                                                <p>Țesuturile care nu primesc sânge nu primesc oxigenul și substanțele nutritive de care au nevoie pentru a supraviețui  și mor, instalându-se gangrena. Semnele gangrenei includ pielea neagră sau albastră, pierderea sensibilității la atingere și un miros neplăcut din zona afectată. Gangrena este o afecțiune gravă care necesită de obicei amputație.</p>

                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-arterelor/degete-gangrena.jpg" alt="gangrena">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratament</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Deși nici un tratament nu poate vindeca boala Buerger, cel mai eficient mod de a opri evoluția bolii este renunțarea la toate produsele din tutun. Chiar și câteva țigări pe zi pot agrava boala. Medicul vă poate recomanda medicamente pentru a vă ajuta să vă lăsați de fumat. Va trebui să evitați produsele de înlocuire a nicotinei deoarece acestea furnizează nicotină, care activează boala Buerger. 

                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                            <h4 class="oswald">Alte tratamente</h4>
															<p>Alte metode de tratament existente pentru boala lui Buerger sunt mai puțin eficiente decât renunțarea la fumat. Opțiunile includ:
</p>
                                                        <div><i class="fa fa-check-circle"></i> Medicamente pentru dilatarea vaselor de sânge, îmbunătățirea fluxului sanguin sau dizolvarea cheagurilor de sânge;
</div>
                                                        <div><i class="fa fa-check-circle"></i> Stimularea măduvei spinării;</div>
														<div><i class="fa fa-check-circle"></i> Amputație, dacă apare o infecție sau o gangrena.

</div>
														
                                                           
                                                           



                                                 <div class="item-media post-thumbnail alignwide bd-t mt-20">
                                                                <img src="images/bolile-arterelor/renunta-la-fumat.jpg" alt="renunta la fumat">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>boala Buerger reprezintă o inflamație cronică a vaselor de sânge care determină înfundarea acestora prin formarea de cheaguri

</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>există o corelație clară între fumat și apariția bolii Buerger, toți pacienții diagnosticați cu această maladie folosesc tutun într-o formă sau alta 

</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>progresia naturală a bolii, în lipsa opririi fumatului, este către gangrena tuturor celor 4 membre, singurul tratament în acest caz fiind amputația

</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>nu există un tratament eficient pentru boala Buerger, altul decât oprirea consumului de tutun sub orice formă, inclusiv fumatul pasiv

</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>