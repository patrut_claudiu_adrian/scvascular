<?php
header('Content-Type: text/html; charset=utf-8');
include('database.php');
?>
<?php 
$id=$_GET['id'];

$q="SELECT * FROM `oferte` WHERE `id`='$id' ";
$res=mysqli_query($conn, $q);
$rows=mysqli_fetch_array($res);

$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .  
                $_SERVER['REQUEST_URI']; 
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>Blog | ImPULS</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include('css-blog.php') ?>
<style>
::placeholder {
  color: #ccc !important;
  opacity: 1;
}

:-ms-input-placeholder {
  color: #ccc !important;
}

::-ms-input-placeholder {
  color: #ccc !important;
}
.vene .col-xl-3{
	padding-left:10px;
	padding-right:10px;
}
</style>
</head>

<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

<?php include("modal.php") ?>

<div id="canvas">
  <div id="box_wrapper"> 
    <?php include("meniu.php") ?>
  </div>
<section class="ls s-py-20 pb-90 oswald" style="background:#f5f5f5">
				<div class="container">
					<div class="divider-30 divider-lg-50"></div>
					  <div class="row c-gutter-60">
						<main class="col-lg-7 col-xl-8">
							<article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">
								<div class="item-content">
									<header class="entry-header single-post text-center">
										<h1 class="entry-title"><?php echo $rows['titlu'];?></h1>
										<div class="entry-meta color-darkgrey">
											<span class="posted-on">
												<a>
												<time class="entry-date"><?php echo(date("d F Y", $rows['data'])); ?></time>
												</a>
											</span>
											<span class="byline">
												<span class="author vcard">
													<a class="url fn n"><?php echo $rows['autor']; ?></a>
												</span>
											</span>
										</div>
									</header>
									<div class="item-media post-thumbnail post-thumbnail1">
									<img src="admin/upload/<?php echo htmlspecialchars($rows['poza']);?>" alt=""><!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d8dd500c0eb06ac"></script> 
									</div>
									<div class="entry-content">
									<?php echo $rows['continut'];?>
									</div>
									

								</div>
	<div class="divider-30 divider-lg-50"></div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d8dd500c0eb06ac"></script>
<div class="addthis_inline_share_toolbox"></div> 
<div class="fb-comments" data-href="<?php echo $link;?>" data-mobile="mobile" data-order-by="reverse_time" data-width="" data-numposts="10"></div>
							</article>
						</main>
						<?php include("aside.php") ?>
					</div>
				</div>
			</section>
			<?php include("bottom-consult.php") ?>
			<?php include("footer.php") ?>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>