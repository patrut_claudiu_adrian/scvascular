<section class="page_title about padding-mobile cs s-pt-50 s-pb-40 cover-background2" style="background-image: url('images/bg/bg4.jpg');">
					<div class="container">
						<div class="row">


							<div class="col-md-6 text-left animate head-title" data-animation="fadeInUp">
							<div class="col-xl-9 col-md-12 pl-0 text-uppercase"><p>ImPULS</p></div>
								<div class="col-xl-8 col-md-12 pl-0"> <h1 class="text-uppercase" style="font-size:48px">Sindromul posttrombotic</h1> </div>
								<div class="col-xl-9 col-md-12 pl-0"><p>Orice formă de insuficiență venoasă a membrelor inferioare fără o cauză clară reprezintă un potențial sindrom post-trombotic  nerecunoscut.</p></div>
							</div>

<div class="col-md-6 text-center animate d-none d-xl-block" data-animation="fadeInUp">
								<div class="head-elements row">

								<div class="col-lg-3">
								<a href="insuficienta-venoasa-cronica-varice.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Insuficiența venoasă cronică (varicele) </h6>

									</div>
									</a>
								</div>

								<div class="col-lg-3">
								<a href="tromboza-venoasa-profunda.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Tromboza venoasă</h6>

									</div>
									</a>
								</div>

							
								
							


							</div>
							<div class="head-elements row">

							

								<div class="col-lg-3">
							<a href="">
									<div class="icon-box text-center box-shadow">

										

									</div>
									</a>
								</div>

								<div class="col-lg-3">
								<a href="sindromul-posttrombotic.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Sindromul posttrombotic</h6>

									</div>
									</a>
								</div>
								<div class="col-lg-3">
								<a href="ulcerul-venos.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Ulcerul venos</h6>

									</div>
									</a>
								</div>


							</div>
							</div>
						</div>
					</div>
				</section>