<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>Bolile arterelor | ImPULS</title>
<meta charset="utf-8">
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
<style>
::placeholder {
  color: #fff !important;
  opacity: 1;
}

:-ms-input-placeholder {
  color: #fff !important;
}

::-ms-input-placeholder {
  color: #fff !important;
}
</style>
</head>

<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

<?php include("modal.php") ?>

<div id="canvas">
  <div id="box_wrapper"> 
    <?php include("meniu.php") ?>
  </div>



			<?php include("b1.php") ?>

			<section class="ls s-py-20 pb-90" style="background:#fafafa">
				<div class="container">
					<div class="divider-80 divider-xl-70"></div>

					<div class="row c-mb-30">
						<div class="col-lg-12 blog-featured-posts">
							<div class="row justify-content-center">
							<div class="col-xl-4 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="ateroscleroza.htm">
												<img src="images/gallery/04.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="ateroscleroza.htm">
														Ateroscleroza
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Ateroscleroza (sau arterioscleroza) apare atunci când vasele de sânge care transportă oxigenul și substanțele nutritive din inimă spre restul corpului (arterele) devin groase și rigide ...</p>
											</div>
								
											
										</div>
									</article>
								</div>
								
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="boala-carotidiana.htm">
												<img src="images/gallery/02.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="boala-carotidiana.htm">
														Boala carotidiană
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Boala carotidiană apare atunci când depozitele de colesterol (plăcile) îngustează vasele care duc sânge creierului (arterele carotidei) ...</p>
											</div>
										
											
										</div>
									</article>

								</div>
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="boala-arteriala-periferica-a-membrelor-inferioare.htm">
												<img src="images/bolile-arterelor/boala-arteriala-periferica-a-membrelor-inferioare.jpg" alt="boala arteriala periferica a membrelor inferioare">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="boala-arteriala-periferica-a-membrelor-inferioare.htm" style="font-size:92%;">
														Boala arterială periferică
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Boala arterială periferică a membrelor inferioare (denumită și arteriopatie cronică obliterantă) este o problemă circulatorie frecventă ... </p>
											</div>
								

										</div>
									</article>
								</div>
								
							</div>
							<div class="row justify-content-center">
							<div class="col-xl-4 col-md-6">

									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="ischemia-critica-a-membrelor-inferioare.htm">
												<img src="images/bolile-arterelor/ischemia-critica-a-membrelor-inferioare.jpg" alt="ischemia critica membrelor inferioare">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="ischemia-critica-a-membrelor-inferioare.htm">
														Ischemia critică a membrelor inferioare
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Ischemia critică reprezintă o obstrucție severă a arterelor membrelor inferioare care reduce semnificativ fluxul sanguin, provocând durere severă și chiar răni ale pielii ...</p>
											</div>
										
											
										</div>
									</article>
								</div>
								<div class="col-xl-4 col-md-6">

									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="anevrismul-aortei-abdominale.htm">
												<img src="images/bolile-arterelor/anevrismul-aortei.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="anevrismul-aortei-abdominale.htm">
														Anevrismul aortei abdominale
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Un anevrism aortic abdominal reprezintă o zonă dilatată (mărită în diametru) în partea inferioară (abdominală) a aortei, vasul sanguin major care furnizează sânge ... </p>
											</div>
										
				
										</div>
									</article>
								</div>
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="boala-buerger-trombangeita-obliteranta.htm">
												<img src="images/gallery/05.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="boala-buerger-trombangeita-obliteranta.htm" style="font-size:92%;">
														Trombangeita obliterantă
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Boala Buerger este o boală rară a arterelor și a venelor mici și medii. În boala Buerger - numită și tromboangeită obliterantă - vasele de sânge se inflamează ...</p>
											</div>
										
											
										</div>
									</article>

								</div>
								
							</div>
							
					</div>

					

				</div>
			</section>
  
  
  
  <?php include("bottom_contact.php") ?>
  
  
  
  <?php include("footer.php") ?>
</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas --> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>