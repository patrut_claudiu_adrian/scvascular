<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Ateroscleroza | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b1.2.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>

Ateroscleroza (sau arterioscleroza) apare atunci când vasele de sânge care transportă oxigenul și substanțele nutritive din inimă spre restul corpului (arterele) devin groase și rigide, uneori restricționând fluxul de sânge către organele și țesuturi. Această rigidizare se produce prin acumularea de grăsimi, colesterol și alte substanțe în pereții arterelor, formându-se astfel placa de aterom.

								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                            <img src="images/echipa/doctor-marius-manda.jpg" alt="Dr. Marius Manda">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. Marius Manda</h5>
                                                        <p class="bl-dr">
                                                            “Deși ateroscleroza este adesea considerată o problemă a inimii, ea poate afecta arterele oriunde în corp.”
                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-arterelor/ateroscleroza.jpg" alt="ateroscleroza vene">
                                    </div>

                                    <div class="entry-content">

                                        <p>Ateroscleroza este o boală progresivă lentă, care poate începe încă din copilărie. Deși cauza exactă este necunoscută, ateroscleroza debutează cu deteriorarea sau rănirea stratului interior al unei artere, numit endoteliu. Leziunile pot fi cauzate de:
                                        </p>
                                        <ul class="list-styled">
										<li>Tensiune arterială crescută</li>
<li>Colesterol ridicat</li>
<li>Trigliceridele crescute</li>
<li>Fumatul</li>
<li>Rezistența la insulină, obezitatea sau diabetul</li>
<li>Inflamații</li></ul>
<p>De-a lungul timpului, colesterolul, calciul și alte produse celulare se acumulează în zona de rănire, peretele arterei se îngroașă, formându-se placa de aterom. Organele și țesuturile conectate la arterele blocate nu primesc suficient sânge pentru a funcționa corespunzător.
</p>
                                           

                                        

                                        <h4 class="oswald">Factorii de risc</h4>
<p>Ateroscleroza se dezvoltă în timp. Pe lângă îmbătrânire, factorii care cresc riscul de ateroscleroza includ:
</p>
                                        <ul class="list-styled">
										<li>Tensiune arterială crescută</li>
<li>Colesterol ridicat</li>
<li>Diabetul</li>
<li>Obezitatea</li>
<li>Fumatul</li>
<li>Un istoric familial de boală cardiacă timpurie</li>
<li>Sedentarismul</li>
<li>O dietă nesănătoasă</li>
                                        </ul>

                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-arterelor/AVC.jpg" alt="avc">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Simptome</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Ateroscleroza se dezvoltă treptat. Simptomele aterosclerozei depind de care artere sunt afectate. De exemplu:
</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														




                                                            <div><i class="fa fa-check-circle"></i> dacă aveți ateroscleroză în arterele inimii, este posibil să aveți simptome cum ar fi durere toracică sau presiune (angina pectorală);</div>
                                                            <div><i class="fa fa-check-circle"></i> dacă aveți ateroscleroză în arterele care vă duc sânge la creier, este posibil să aveți semne și simptome cum ar fi amorțeală bruscă sau slăbiciune la nivelul brațelor sau picioarelor, dificultăți de vorbire sau vorbire neclară și pierderea temporară a vederii la un ochi. Acestea semnalează un atac ischemic tranzitor (AIT), care, dacă este lăsat netratat, poate progresa la un accident vascular cerebral;</div>
                                                            <div><i class="fa fa-check-circle"></i> dacă aveți ateroscleroză în arterele din brațele și picioarele dumneavoastră, este posibil să aveți simptome de boală arterială periferică, cum ar fi durere atunci când mergeți pe jos (claudicație);</div>
															<div><i class="fa fa-check-circle"></i> dacă aveți ateroscleroză în arterele care furnizează sânge rinichilor, dezvoltați hipertensiune arterială sau insuficiență renală.</div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse02" class="collapsed">
                                                            <li>Diagnosticarea bolii</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>În timpul unui consult, medicul poate găsi semne de artere îngustate, mărite sau întărite, incluzând:
  <div><i class="fa fa-check-circle"></i> Un puls slab sau absent sub zona îngustată a arterei;</div>
                                                                <div><i class="fa fa-check-circle"></i> Scăderea tensiunii arteriale la nivelul membrelor afectate;</div>
																<div><i class="fa fa-check-circle"></i> Sunete (sufluri) pe arterele dumneavoastră, auzite folosind un stetoscop.
</div>
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                       <p>În funcție de rezultatele examenului fizic, medicul vă poate sugera unul sau mai multe teste diagnostice:
</p>

<div><i class="fa fa-check-circle"></i> <strong>Analizele de sânge</strong> pot detecta niveluri crescute de colesterol și zahăr din sânge</div>
<div><i class="fa fa-check-circle"></i> <strong>Ecografie Doppler</strong> poate să evalueze gradul de blocaj și viteza fluxului sanguin în artere</div>
<div><i class="fa fa-check-circle"></i> <strong>Indicele gleznă-braț</strong> compară tensiunea arterială de la gleznă cu tensiunea arterială din braț. O diferență anormală poate indica o boală vasculară periferică, care este de obicei cauzată de ateroscleroză</div>
<div><i class="fa fa-check-circle"></i> <strong>Electrocardiograma (ECG)</strong> poate dezvălui dovezi ale unui atac de cord anterior</div>
<div><i class="fa fa-check-circle"></i> <strong>Testul de efort</strong> este folosit pentru a culege informații despre cât de bine funcționează inima în timpul activității fizice</div>
<div><i class="fa fa-check-circle"></i> <strong>Cateterizarea cardiacă și angiografia</strong> pot arăta dacă arterele coronare sunt îngustate sau blocate. Un colorant lichid este injectat în arterele inimii printr-un tub lung și subțire (cateter) care este introdus printr-o arteră, de obicei din picior și avansat până la arterele din inimă</div>
<div><i class="fa fa-check-circle"></i> <strong>Alte teste imagistice</strong> - scanarea prin tomografie computerizată (CT) sau angiografia cu rezonanță magnetică (IRM) vizualizează direct îngustarea arterelor, anevrismele și depunerile de calciu din pereții arterelor.</div>
                                                        
                                                           
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Complicații</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Complicațiile aterosclerozei depind de arterele afectate:</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     

                                                                <div><i class="fa fa-check-circle"></i>  <strong>Boală coronariană </strong> se produce când ateroscleroza îngustează arterele inimii și provoacă dureri toracice (angină), atac de cord sau insuficiență cardiacă
</div>
                                                                <div><i class="fa fa-check-circle"></i>  <strong>Boală carotidiană </strong> apare când ateroscleroza afectează arterele care irigă creierul și se manifestă printr-un atac ischemic tranzitor sau un accident vascular cerebral
</div>
                                                                <div><i class="fa fa-check-circle"></i>  <strong>Boală arterială periferică </strong> se dezvoltă când ateroscleroza îngustează arterele din brațe sau picioare și produce durere la efort fizic, sau moartea țesutului (gangrena)
</div>
                                                                <div><i class="fa fa-check-circle"></i>  <strong>Boala renală cronică </strong> se manifestă când ateroscleroza determină îngustarea arterelor care duc sânge la rinichi, provocând insuficiență renală
</div>
                                                                <div><i class="fa fa-check-circle"></i>  <strong>Anevrismul </strong> reprezintă o umflatură patologică în peretele arterei, putând apărea oriunde în organism 
</div>

                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-arterelor/infarct.jpg" alt="infarct inima">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratament</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Modificările stilului de viață, cum ar fi adoptarea unei alimentații sănătoase și exerciții fizice regulate, sunt adesea cel mai adecvat tratament pentru ateroscleroză. Uneori pot fi recomandate medicamente sau proceduri chirurgicale.
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                            <h4 class="oswald">MEDICAMENTE</h4>
															
                                                        <div><i class="fa fa-check-circle"></i> <strong>Medicamente pentru colesterol</strong> reduc colesterolul LDL, colesterolul "rău" și cresc colesterolul HDL, colesterolul "bun"</div>
                                                        <div><i class="fa fa-check-circle"></i> <strong>Medicamente antiplachetare</strong>, cum ar fi aspirina, reduc probabilitatea ca în arterele îngustate să se formeze un cheag</div>
														<div><i class="fa fa-check-circle"></i> <strong>Medicamente beta-blocante</strong> sunt frecvent utilizate pentru boala coronariană; ele scad ritmul cardiac și tensiunea arterială
</div>
                                                        <div><i class="fa fa-check-circle"></i> <strong>Inhibitorii enzimei de conversie a angiotensinei</strong> scad tensiuniea arteriale și produc alte efecte benefice asupra arterelor inimii
</div>
														<div><i class="fa fa-check-circle"></i> <strong>Blocante ale canalelor de calciu</strong> reduc tensiunea arterială și uneori sunt folosite pentru a trata angina pectorală
</div>
                                                        <div class="mb-90"><i class="fa fa-check-circle"></i> <strong>Diureticele</strong> reduc tensiunea arterială
</div>
														
														
                                                           
                                                           



                                                           <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                                                <img src="images/bolile-arterelor/medicamente.jpg" alt="medicamente">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar text-white">
								<div class="col-md-3 col-sm-12">
									<h1 class="text-white fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>ateroscleroza se dezvoltă prin acumularea de grăsimi, colesterol și alte substanțe în pereții arterelor, formându-se astfel placa de aterom
</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="text-white fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>fumatul, diabetul, hipertensiunea și colesterolul crescut sunt principalii factori de risc pentru progresia aterosclerozei 
</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="text-white fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>netratată, ateroscleroza determină complicații importante, cum ar fi infarctul miocardic, accidentul vascular cerebral sau gangrena membrelor 
</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="text-white fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>tratamentul constă în adoptarea unui stil de viață sănătos, medicamente care corectează factorii de risc și intervenții chirurgicale specifice fiecărei zone din corp afectate
</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>