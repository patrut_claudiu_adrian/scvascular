<section class="page_title about padding-mobile cs s-pt-50 s-pb-40 cover-background2">
	<div class="container">
		<div class="row">


			<div class="col-lg-11 col-md-12 text-left animate head-title" data-animation="fadeInUp">
				<div class="col-xl-9 col-md-12 pl-0 text-uppercase">
					<p>Educatie in sanatate</p>
				</div>
				<div class="col-xl-8 col-md-12 pl-0">
					<h1 class="text-uppercase">ReferralMD's Healthcare IT News and Marketing Blog</h1>
				</div>
				<div class="col-xl-9 col-md-12 pl-0">
					<p>Join millions of readers and discover comprehensive content about the business side of information technology, marketing guides, news and reviews for healthcare professionals.</p><br>
				</div>
			</div>

			
		</section>