<?php include("database.php") ?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <title>Blog | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css-blog.php") ?>
    <style>
        ::placeholder {
            color: #ccc !important;
            opacity: 1;
        }

        :-ms-input-placeholder {
            color: #ccc !important;
        }

        ::-ms-input-placeholder {
            color: #ccc !important;
        }

        .vene .col-xl-3 {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
</head>

<body>
<!--[if lt IE 9]>
<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
<![endif]-->
<?php include("modal.php") ?>
<div id="canvas">
    <div id="box_wrapper">
        <?php include("meniu.php") ?>
    </div>
    <section class="ls s-py-20 pb-90" style="background:#f5f5f5">
        <div class="container">
            <div class="divider-30 divider-lg-50"></div>
            <div class="row c-gutter-60">
                <main class="col-lg-8 col-xl-8">
                    <?php
                    $q = 'SELECT * FROM `oferte` WHERE `activ`=1 ORDER BY ID DESC';
                    $res_rez = mysqli_query($conn, $q);
                    for ($i = 0, $iMax = mysqli_num_rows($res_rez); $i < $iMax; $i++) {
                        $row_con = mysqli_fetch_array($res_rez); ?>
                        <?php
                        $titlux = $row_con['titlu'];

                        $normalizeChars = array(
                            'Š' => 'S',
                            'š' => 's',
                            'Ð' => 'Dj',
                            'Ž' => 'Z',
                            'ž' => 'z',
                            'À' => 'A',
                            'Á' => 'A',
                            'Ã' => 'A',
                            'Ä' => 'A',
                            'Å' => 'A',
                            'Æ' => 'A',
                            'Ç' => 'C',
                            'È' => 'E',
                            'É' => 'E',
                            'Ê' => 'E',
                            'Ë' => 'E',
                            'Ì' => 'I',
                            'Í' => 'I',
                            'Ï' => 'I',
                            'Ñ' => 'N',
                            'Ń' => 'N',
                            'Ò' => 'O',
                            'Ó' => 'O',
                            'Ô' => 'O',
                            'Õ' => 'O',
                            'Ö' => 'O',
                            'Ø' => 'O',
                            'Ù' => 'U',
                            'Ú' => 'U',
                            'Û' => 'U',
                            'Ü' => 'U',
                            'Ý' => 'Y',
                            'Þ' => 'B',
                            'ß' => 'Ss',
                            'à' => 'a',
                            'á' => 'a',
                            'ã' => 'a',
                            'ä' => 'a',
                            'å' => 'a',
                            'æ' => 'a',
                            'ç' => 'c',
                            'è' => 'e',
                            'é' => 'e',
                            'ê' => 'e',
                            'ë' => 'e',
                            'ì' => 'i',
                            'í' => 'i',
                            'ï' => 'i',
                            'ð' => 'o',
                            'ñ' => 'n',
                            'ń' => 'n',
                            'ò' => 'o',
                            'ó' => 'o',
                            'ô' => 'o',
                            'õ' => 'o',
                            'ö' => 'o',
                            'ø' => 'o',
                            'ù' => 'u',
                            'ú' => 'u',
                            'û' => 'u',
                            'ü' => 'u',
                            'ý' => 'y',
                            'þ' => 'b',
                            'ÿ' => 'y',
                            'ƒ' => 'f',
                            'ă' => 'a',
                            'î' => 'i',
                            'â' => 'a',
                            'ș' => 's',
                            'ț' => 't',
                            'Ă' => 'A',
                            'Î' => 'I',
                            'Â' => 'A',
                            'Ș' => 'S',
                            'Ț' => 'T',
                        );

                        $titlux = strtr($titlux, $normalizeChars);
                        $titlux = strtolower($titlux);

                        $titlux = preg_replace('/[^A-Za-z0-9\-]/', '-', $titlux);
                        $titlux = substr_replace($titlux, "", -1);

                        ?>
                        <article class="text-md-left vertical-item content-padding post type-post status-publish format-standard has-post-thumbnail oswald">
                            <div class="item-media post-thumbnail post-thumbnail1">
                                <a href="articol.php?id=<?php echo htmlspecialchars($row_con['id']); ?>">
                                    <img src="admin/upload/<?php echo htmlspecialchars($row_con['poza']); ?>" alt="<?php echo htmlspecialchars($row_con['titlu']); ?>">
                                </a>
                            </div>
                            <div class="item-content">
                                <header class="entry-header">
                                    <h3 class="entry-title">
                                        <a href="articol.php?id=<?php echo htmlspecialchars($row_con['id']); ?>" rel="bookmark">
                                            <?php echo htmlspecialchars($row_con['titlu']); ?>
                                        </a>
                                    </h3>
                                </header>
                                <div class="entry-content">
                                    <?php echo substr($row_con['continut'], 0, 550); ?> ...
                                    <div class="entry-meta">
                                        <a>
                                            <time class="entry-date published updated" datetime="2018-03-18T15:15:12+00:00"><?php echo(date("d F Y", $row_con['data'])); ?>
                                            </time>
                                        </a>
                                        <span class="byline">
											<span class="author vcard">
											<a><?php echo htmlspecialchars($row_con['autor']); ?></a>
											</span>
										</span>
                                        <a class="read_more btn btn-outline-darkgrey small fw-400" href="<?php echo htmlspecialchars($row_con['id']); ?>-<?php echo $titlux; ?>">
                                            Citeste articolul
                                        </a>

                                    </div>
                                </div>
                        </article>
                    <?php } ?>
                    <?php if (0) { ?>
                        <nav class="navigation pagination oswald" role="navigation">
                            <h2 class="screen-reader-text">Posts navigation</h2>
                            <div class="nav-links">
                                <a class="prev page-numbers" href="">
                                    <i class="fa fa-chevron-left"></i>
                                    <span class="screen-reader-text">Previous page</span>
                                </a>
                                <span class="page-numbers current">
										<span class="meta-nav screen-reader-text">Page </span>1</span>
                                <a class="page-numbers" href="">

                                    <span class="meta-nav screen-reader-text">Page </span>2</a>

                                <a class="page-numbers" href="">
                                    <span class="meta-nav screen-reader-text">Page </span>3</a>
                                <a class="next page-numbers" href="">
                                    <span class="screen-reader-text">Next page</span>
                                    <i class="fa fa-chevron-right"></i>
                                </a>
                            </div>
                        </nav>
                    <?php } ?>
                </main>
                <?php include("aside.php") ?>
            </div>
        </div>
    </section>
    <?php include("bottom_contact.php") ?>
    <?php include("footer.php") ?>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
<script src="js/compressed.js"></script>
<script src="js/main.min.js"></script>
</body>
</html>