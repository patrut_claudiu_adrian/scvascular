<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>Bolile venelor | ImPULS</title>
<meta charset="utf-8">
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
<style>
::placeholder {
  color: #fff !important;
  opacity: 1;
}

:-ms-input-placeholder {
  color: #fff !important;
}

::-ms-input-placeholder {
  color: #fff !important;
}
.vene .col-xl-3{
	padding-left:10px;
	padding-right:10px;
}
</style>
</head>

<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

<?php include("modal.php") ?>

<div id="canvas">
  <div id="box_wrapper"> 
    <?php include("meniu.php") ?>
  </div>



			<?php include("b2.php") ?>

			<section class="ls s-py-20 pb-90" style="background:#fafafa">
				<div class="container">
					<div class="divider-80 divider-xl-70"></div>

					<div class="row c-mb-30">
						<div class="col-lg-12 blog-featured-posts">
							<div class="row vene justify-content-center">
								<div class="col-xl-3 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="insuficienta-venoasa-cronica-varice.htm">
												<img src="images/bolile-venelor/insuficieta-venoasa.jpg" alt="insuficieta venoasa">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="insuficienta-venoasa-cronica-varice.htm">
														Varicele hidrostatice
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Numărul de persoane care suferă de insuficiență venoasă a membrelor inferioare este în continuă creștere. Motivul principal este stilul de viață sedentar ... </p>
											</div>
								

										</div>
									</article>
								</div>
								<div class="col-xl-3 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="ulcerul-venos.htm">
												<img src="images/bolile-venelor/ulcerul-venos.jpg" alt="ulcerul venos">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="ulcerul-venos.htm">
														Ulcerul venos
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>
Ulcerul venos al membrelor inferioare este o leziune care apare cel mai frecvent în regiunea gleznei. Lăsat netratat nu se vindecă, se suprainfectează  ... </p>
											</div>
								

										</div>
									</article>
								</div>
								<div class="col-xl-3 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="tromboza-venoasa-profunda.htm">
												<img src="images/bolile-venelor/tromboza-venoasa.jpg" alt="tromboza venoasa">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="tromboza-venoasa-profunda.htm">
														Tromboza venoasă profundă
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Tromboza venoasă profundă apare atunci când se formează un cheag de sânge (tromb) în una sau mai multe vene profunde din corp, de obicei în picioare ...</p>
											</div>
										
											
										</div>
									</article>

								</div>
								<div class="col-xl-3 col-md-6">
									<article class="vertical-item content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="sindromul-posttrombotic.htm">
												<img src="images/bolile-venelor/sindromul-posttrombotic.jpg" alt="sindromul posttrombotic">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title text-uppercase">
													<a href="sindromul-posttrombotic.htm">
														Sindromul posttrombotic
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Sindromul post-trombotic reprezintă spectrul tuturor simptomelor și consecințelor nefavorabile care rezultă în urma unui episod de tromboză venoasă profundă ... </p>
											</div>
								

										</div>
									</article>
								</div>
								
								
							</div>
							
							
					</div>

					

				</div>
			</section>
  
  
  
  <?php include("bottom_contact.php") ?>
  
  
  
  <?php include("footer.php") ?>
</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas --> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>