$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Verifica campurile necompletate!");
    } else {
        // everything looks good!
	
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var name = $("#nume").val();
    var email = $("#email").val();
    var phone = $("#telefon").val();
    var city = $("#localitate").val();
	var date = $("#data").val();
	var message = $("#detalii").val();

	


 
	var medic = $("#medic").val();
	var msg_subject = 'Programare consultatie scvascular - ' + $("#medic").val();

    $.ajax({
        type: "POST",
        url: "assets/php/form-process.php",
        data: "&name=" + name + "&email=" + email + "&msg_subject=" + msg_subject + "&message=" + message + "&phone=" + phone + "&city=" + city + "&date=" + date + "&medic=" + medic,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Programare trimisa! Veti fi contactat in curand!")
}

function formError(){
    $("#contactForm").removeClass().addClass('bounceIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
   if(valid){
        var msgClasses = "alert shake animated alert-success mt-10 mb-10";
    } else {
        var msgClasses = "alert alert-danger mt-10 mb-10";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}