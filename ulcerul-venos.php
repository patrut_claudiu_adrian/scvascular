<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Ulcerul venos | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b2.4.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>



Ulcerul venos al membrelor inferioare este o leziune care apare cel mai frecvent în regiunea gleznei. Lăsat netratat nu se vindecă, se suprainfectează și are o tendință de extindere. De obicei, apare la pacienții care au probleme cu venele din tinerețe - inflamație a venelor, tromboză sau vene varicoase.





								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. George Pătruț">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. George Pătruț</h5>
                                                        <p class="bl-dr">
                                                            “Venele varicoase neglijate și netratate determină adesea apariția unor răni pe suprafața pielii piciorului, fără tendință de cicatrizare, denumite ulcere venoase.”





                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-venelor/ulcerul-venos.jpg" alt="ulceru venos">
                                    </div>

                                    <div class="entry-content">

                           

<p>Boala venoasă cronică nu este doar o problemă cosmetică. Venele varicoase neglijate pot favoriza dezvoltarea unor răni greu de vindecat la nivelul picioarelor - ulcerul venos al membrului inferior. O astfel de leziune este foarte neplăcută și împovărătoare pentru pacient și rudele sale apropiate.</p><p>

  Cel mai frecvent, rana este localizată pe suprafața anterioară a piciorului, chiar deasupra gleznei. De obicei, este destul de dureroasă. Este de asemenea o sursă de exudat masiv - lichid purulent. Exudatul reprezintă o problemă serioasă de igienă pentru pacient. După o scurtă perioadă de timp, pansamentele devine umede, miros neplăcut și trebuie schimbate. </p><p>Nu ar trebui să uităm de aspectele serioase legate de sănătate - exsudatele masive conduc la o pierdere substanțială de proteine și electroliți din organismul pacientului, ceea ce împiedică în plus procesul de vindecare a rănii.

</p>
                            

                                        

                                        
                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-venelor/ulcer-varice.jpg" alt="ulcer varice">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a>
                                                            <li>Pot fi tratate eficient ulcerele venoase?</li>
                                                        </a>

                                                        <div class="card-body">





<p>Absolut. Majoritatea acestor răni pot fi tratate folosind pansamente specializate și feși elastice de compresie plasate în mod adecvat pe piciorul afectat. Din păcate, tratamentul necesită răbdare și consecvență atât din partea pacientului, cât și a medicului. Lipsa de consecvență este cel mai frecvent motiv pentru eșecul tratamentului.</p><p>

Merită să știți că cu cât rana este mai mare și mai veche, tratamentul este mai lung și efectul cosmetic este mai precar. În mod analog, cu cât tratamentul este mai precoce,  cu atât sunt mai bune și mai rapide efectele acestuia.<p>


                                                        </div>

                                                    </div>

                                                   
                                        
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a>
                                                            <li>Sunt necesare intervenții chirurgicale pentru vindecarea ulcerului?</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Ulcerul venos este expresia clinică, vizibilă, a unei alte boli - insuficiența venoasă. În cazul majorității pacienților, pansamentele speciale și benzile de compresie elastică folosite repetat și pe timp îndelungat determină vindecarea rănii. Dar cauza care a produs-o rămâne prezentă, și, frecvent, după perioade variabile de timp, apar alte leziuni ulcerative. Ghidurile moderne de tratament recomandă terapia concomitentă a venelor bolnave.</p><p>

În trecut, procedura chirurgicală consta în îndepărtarea chirurgicală a venei superficiale insuficiente (vena safenă mare sau vena safenă mică). În prezent, se aplică proceduri minim invazive - terapia cu laser intravenos sau obliterația chimică, adică scleroterapia. </p><p>

Trebuie menționate noile posibilități de tratament pentru pacienții cu ulcere recurente, care au drept cauză leziuni post-trombotice în venele profunde ale coapsei și / sau pelvisului. În cazul acestor pacienți, poate fi obținută o mai bună vindecare, sau reducerea șanselor de recurență a ulcerului prin introducerea unui stent special în vena afectată.





                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                    
                                                 
                              
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a>
                                                            <li>Este grefa de piele utilă în tratamentul ulcerelor venoase?</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>La unii pacienți putem scurta timpul de tratament prin utilizarea unei grefe de piele. Rana, cu toate acestea, trebuie pregătită în mod adecvat pentru a accepta pielea transplantată. Nu trebuie uitat că și zona din care se recoltează pielea sănătoasă trebuie îngrijită prin pansamente repetate, până la vindecare. Din păcate, există situații în care unele grefe nu sunt acceptate.



</p>
  <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-venelor/grefa-de-piele.jpg" alt="grefa de piele">
                                        </div>
                                                        </div>

                                                    </div>

                                                    
                                                   
                                                </div>

                                                

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-4 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>Ulcerul venos poate fi tratat eficient, dar sunt necesare răbdare și consecvență din partea pacientului, a rudelor sale cele mai apropiate și a echipei medicale



</p>
								</div>
								<div class="col-md-4 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>Terapia compresivă reprezintă elementul cel mai important al tratamentului ulcerului venos





</p>
								</div>
								<div class="col-md-4 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>Tratarea eficace a bolii care a produs rana - insuficiența venoasă - este cheia prevenirii reapariției ulcerului venos





</p>
								</div>

							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>