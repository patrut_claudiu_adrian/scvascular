<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ImPULS</title>

<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
<style>
::placeholder {
 color: #000 !important;
 opacity: 1;
}

:-ms-input-placeholder {
 color: #000 !important;
}

::-ms-input-placeholder {
 color: #000 !important;
}
select {
-moz-appearance:none;
-webkit-appearance:none;
appearance:none;
}
</style>
</head>
<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

<?php include("modal.php") ?>
<div id="canvas">
  <div id="box_wrapper">
    <?php include("meniu.php") ?>
  </div>

  <section class="ls">
    <div class="container">
      <div class="row">
        <div class="divider-40 divider-lg-100"></div>
        <div class="col-lg-6  animate" data-animation="pullDown">
          <h3 class="title_block pb-25"><span>Informații</span> Contact</h3>
          <div class="row">
            <div class="col-sm-12">
              <div class="icon-box">
                <div class="media">
                  <div class="icon-styled color-main fs-40"> <i class="ico icon-phone-receiver"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> Telefoane medici: </h6>
                    <p>
                      <b>Dr. George Pătruț</b> - Tel: 0754 099 761 </p>
                  </div>
                </div>
              </div>
              <div class="icon-box">
                <div class="media">
                  <div class="icon-styled color-main fs-40"> <i class="ico icon-email"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> Email: </h6>
                    <p> georgepatrut@scvascular.ro</p>
                  </div>
                </div>
              </div>
              <div class="icon-box">
                <div class="media">
                  <div class="icon-styled color-main fs-40"> <i class="ico icon-location"></i> </div>
                  <div class="media-body">
                    <h6 class="fw-300"> Cabinete </h6>
                      <p> <b>Timișoara,:</b> Centrul Medical Tadlife - Strada Tosca, nr 30A,<br>telefon 0754.099.761</p>
                      <p><b>Arad:</b> Clinica Medical Centrum - blv. Decebal, nr. 12, telefon 0747.569.124</p>
                      <p><b>Deva:</b> Derm Artis Clinic, Bvd. 22 Decembrie, Bloc 41, telefon 0774.495.143</p>
                      <p><b>Oradea:</b> Clinica Bioinvest - strada Episcop Roman Ciorogariu, nr 62, telefon 0259.412.295</p>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="divider-40 d-lg-none d-md-block"></div>
        <div class="padding-mobile-small col-lg-6 cs csc5 px-70 py-66 animate" data-animation="scaleAppear">
          <form id="contactForm" role="form" data-toggle="validator" class="contact-form c-mb-10 c-gutter-10">
              <input type="hidden" name="medic" value="Dr. George Pătruț">
              
            <div class="row">
              <div class="col-sm-12">
                <h4 class="title_block"><span>Programări</span> online</h4>
                <div class="form-group has-placeholder">
                  <label for="name">Nume Prenume *<span class="required">*</span></label>
                  <input type="text" required size="30" value="" name="nume" id="nume" class="form-control" placeholder="Nume Prenume">
                </div>
                <div class="form-group has-placeholder">
                  <label for="email">Email<span class="required">*</span></label>
                  <input type="email" required size="30" value="" name="email" id="email" class="form-control" placeholder="Email">
                </div>
                
                  <div class="form-group has-placeholder">
                  <label for="telefon">Telefon *<span class="required">*</span></label>
                  <input type="text" required size="30" value="" name="telefon" id="telefon" class="form-control" placeholder="Telefon">
                </div>
                
                <div class="form-group has-placeholder">
                  <label for="data">Data<span class="required">*</span></label>
                  <input required size="30" value="" name="data" id="data" class="form-control" placeholder="Data (zi/luna/an)">
                </div>
                
                <!--<div class="form-group has-placeholder">
                  <label for="medic">Medic *<span class="required">*</span></label>
				  <select name="medic" id="medic" class="form-control" required>
									<option value="-1" selected disabled>Alege medicul</option>
									<option class="level-0" value="Dr. George Pătruț">Dr. George Pătruț</option>
									<option class="level-0" value="Dr. Marius Manda">Dr. Marius Manda</option>
								</select>
	
                </div>-->
                
                <div class="form-group has-placeholder">
                  <label for="localitate">Localitate consult *<span class="required">*</span></label>
                  <select name="localitate" id="localitate" class="form-control" required>
                        <option value="-1" selected disabled>Localitate consult</option>
                        <option class="level-0" value="Timisoara">Timisoara</option>
                        <option class="level-0" value="Arad">Arad</option>
                        <option class="level-0" value="Deva">Deva</option>
                        <option class="level-0" value="Oradea">Oradea</option>
                    </select>
                </div>
                
                <div class="form-group has-placeholder">
                  <label for="detalii">Ce problemă aveți?</label>
				  <textarea name="detalii" id="detalii" class="form-control" required placeholder="Ce problemă aveți?"></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
				 <div id="msgSubmit" class="hidden" role="alert"></div>
                  <button type="submit" id="submit" name="contact_submit mt-30" class="btn contact_submit">Programează-te</button>
				  
				  
              <div class="clearfix"></div>   
                </div>
              </div>
            </div>
          </form>
        </div>
        <!--.col-* -->
        
        <div class="divider-100 divider-lg-130"></div>
      </div>
    </div>
  </section>
  <?php include("footer.php") ?>
</div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script> 
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
<script type="text/javascript" src="assets/js/form-validator.min.js"></script>  
<script type="text/javascript" src="assets/js/contact-form-script.js"></script>
</body>
</html>