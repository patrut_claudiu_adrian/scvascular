<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Tromboza venoasă profundă | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b2.2.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>

Tromboza venoasă profundă apare atunci când se formează un cheag de sânge (tromb) în una sau mai multe vene profunde din corp, de obicei în picioare. Boala se manifestă cu dureri în gambe și coapse și umflarea picioarelor, dar pot exista și forme fără simptome.




								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                             <img src="images/echipa/doctor-marius-manda.jpg" alt="Dr. Marius Manda">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. Marius Manda</h5>
                                                        <p class="bl-dr">
                                                            “Cheagurile formate în picioare se pot desprinde și ajunge în plămâni, cauzând o complicație foarte gravă - embolia pulmonară.”



                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele trombozei venoase</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-venelor/tromboza-venoasa.jpg" alt="picior umflat">
                                    </div>

                                    <div class="entry-content">

                           

<p>Cheagurile de sânge se formează în venele profunde <strong>în orice situație care împiedică sângele să circule sau să se coaguleze în mod normal:</strong></p>





<ul class="list-styled">
										<li>leziuni ale venelor</li>
<li>intervenții chirurgicale</li>
<li>anumite medicamente</li>
<li>mișcarea limitată</li>
</ul>








                                        </p>                              

                                        

                                        <h4 class="oswald">Factorii de risc</h4>
										<p>Mulți factori vă pot crește riscul de a dezvolta tromboză venoasă profundă. Cu cât aveți mai mulți, cu atât este mai mare riscul. Factorii de risc includ:
</p>
                                        <ul class="list-styled">
										
     
     
     
     
     
     
     
     












										<li>existența unei tulburări de coagulare a sângelui</li>
<li>imobilizarea prelungită la pat</li>
<li>leziuni sau intervenții chirurgicale</li>
<li>sarcina</li>
<li>obezitatea</li>
<li>fumatul</li>
<li>terapie hormonală (contraceptive orale, terapie de substituție hormonală)</li>
<li>cancerul
</li>

<li>insuficiența cardiacă</li>
<li>boli inflamatorii intestinale</li>
<li>istoric personal sau familial de tromboză venoasă profundă sau embolie pulmonară</li>
<li>vârsta peste 60 de ani</li>
                                        </ul>
                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-venelor/tromboza-venoasa-profunda.jpg" alt="tromboza-venoasa-profunda">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a>
                                                            <li>Care sunt simptomele?</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Semnele și simptomele trombozei venoase profunde pot include:










</p>
<div><i class="fa fa-check-circle"></i> Umflarea piciorului afectat;  </div>
                                                            <div><i class="fa fa-check-circle"></i> Durere mai ales în gambă  </div>
															<div><i class="fa fa-check-circle"></i> Pielea devine roșie </div>
															<div><i class="fa fa-check-circle"></i> Senzație de căldură în piciorul afectat. </div><br>
<p>Tromboza venoasă profundă se poate manifesta și fără simptome vizibile.</p>

                                                        </div>

                                                    </div>

                                                   
                                        
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a>
                                                            <li>Complicațiile trombozei venoase profunde</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p><strong>Embolia pulmonară</strong><br>
O embolie pulmonară apare atunci când un vas de sânge din plămâni este blocat de un cheag de sânge (tromb) plecat dintr-o altă parte a corpului, de obicei din picior. Embolia pulmonară poate pune viața în pericol.  </p> <p>Este important să urmăriți semnele și simptomele unei embolii pulmonare și să solicitați asistență medicală dacă acestea apar:




                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     







<div><i class="fa fa-check-circle"></i> Dificultate de respirație brusc instalată;</div>
                                                            <div><i class="fa fa-check-circle"></i> Dureri toracice sau disconfort care se agravează atunci când respirați adânc sau când tușiți; </div>
                                                            <div><i class="fa fa-check-circle"></i> Amețeli sau leșin; </div>
															 <div><i class="fa fa-check-circle"></i> Puls rapid; </div>
															  <div><i class="fa fa-check-circle"></i> Tuse cu sânge</div>

<br>
<p><strong>Sindromul postflebitic</strong><br>
O complicație obișnuită care poate apărea după tromboza venoasă profundă este cunoscută sub numele de sindrom postflebitic, numită și sindrom post-trombotic. Deteriorarea venelor produsă de cheagul de sânge le face incapabile să preia fluxul sanguin în zonele afectate, ceea ce poate provoca:









                                                               
                                                            </p>

                                                                <div><i class="fa fa-check-circle"></i> Umflarea persistenta a picioarelor (edem);</div>
                                                            <div><i class="fa fa-check-circle"></i> Dureri dacă stați în picioare perioade prelungite de timp;</div>
                                                            <div><i class="fa fa-check-circle"></i> Modificări de culoare ale pielii; </div>
															 <div><i class="fa fa-check-circle"></i> Infecții ale pielii. </div>

                                                              

                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-venelor/embolia-pulmonara.jpg" alt="embolia pulmonara">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                              
                                                </div>
<div class="card">
                                                    <div role="tab">
                                                        <a>
                                                            <li>Diagnosticarea trombozei venoase
</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Punctul de pornire pentru diagnosticarea exactă a trombozei venoase îl reprezintă o examinare clinică amănunțită - evaluarea simptomelor pacientului și decelarea semnelor de boală. Confirmarea diagnosticului se realizează printr-o ecografie Doppler a sistemului venos, efectuată de un ultrasonografist cu experiență, care lucrează în principal în domeniul flebologiei. 











</p>


                                                        </div>

                                                    </div>

                                                   
                                        
                                                </div>
                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratamentul trombozei venoase</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Tratamentul trombozei venoase profunde are ca scop prevenirea creșterii în dimensiuni a cheagurilor și desprinderea acestora de pe pereții venelor, cauză a emboliei pulmonare. Opțiunile de tratament pentru tromboza venoasă profundă includ:

</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     






<div><i class="fa fa-check-circle"></i> <strong>medicamentele anticoagulante</strong>, care pot fi injectate sau administrate sub formă de pilule, scad capacitatea de coagulare a sângelui</div>
                                                            <div><i class="fa fa-check-circle"></i> <strong>medicamentele fibrinolitice</strong> topesc rapid cheagurile, dar pot provoca sângerări grave și sunt în general rezervate cazurilor severe </div>
                                                            <div><i class="fa fa-check-circle"></i> <strong>filtrele de venă cavă</strong> împiedică migrarea în plămâni a cheagurilor care se desprind din venele de la picioare </div>
															<div><i class="fa fa-check-circle"></i> <strong>ciorapii elastici compresivi</strong> previn umflarea picioarelor </div>




                                                                
                                                              

                                                          
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                                

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>umflarea picioarelor, durerea în gambe și roșeața și căldura locală reprezintă manifestările clinice ale trombozei venoase profunde


</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>embolia pulmonară este complicația cea mai gravă și are potențial letal



</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>diagnosticul de certitudine se stabilește prin ecografie Doppler




</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>tratamentul anticoagulant și compresia elastică externă sunt obligatorii la pacienții cu tromboză venoasă profundă 




</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>