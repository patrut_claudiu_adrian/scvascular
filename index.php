<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" style=" scroll-behavior: smooth;">
<!--<![endif]-->
<head>
    <title>Chirurgie vasculara Timisoara, Arad, Deva | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
</head>
<body>
<!--[if lt IE 9]>
<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
<![endif]-->
<?php include("modal.php") ?>
<div id="canvas">
    <div id="box_wrapper">
        <?php include("meniu.php") ?>
    </div>
    <?php include("slide.php") ?>
    <section id="welcome" class="ls">
        <div class="container">
            <div class="row">
                <div class="divider-30 d-none d-xl-block"></div>
                <div class="col-md-12">
                    <div class="row align-items-center">
                        <div class="col-lg-7">
                            <div class="divider-50 d-lg-block"></div>
                            <h2 class="width-80 mt-0 oswald"><span class="color-main">Consultații și tratamente </span> moderne</h2>
                            <p class="after-title subtitle"> Vă oferim consultații și tratamente moderne pentru bolile arterelor, venelor și chirurgia de acces vascular. </p>
                            <div id="accordion01" role="tablist">
                                <div class="card">
                                    <div class="card-header" role="tab" id="collapse01_header">
                                        <h4><a data-toggle="collapse" href="#collapse01" aria-expanded="true" aria-controls="collapse01"> Proceduri și intervenții minim
                                                invazive </a></h4>
                                    </div>
                                    <div id="collapse01" class="collapse show" role="tabpanel" aria-labelledby="collapse01_header" data-parent="#accordion01">
                                        <div class="card-body" style="text-align:justify"> Efectuăm proceduri chirurgicale clasice și
                                            intervenții moderne minim invazive de chirurgie vasculară pentru a vă trata afecțiunile arterelor - vasele care duc sângele de la inimă
                                            în corp, venelor - vasele care transportă sângele din corp înapoi la inimă, cât și pentru a realiza operații de acces vascular.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="collapse02_header">
                                        <h4><a class="collapsed" data-toggle="collapse" href="#collapse02" aria-expanded="false" aria-controls="collapse02"> Perioadă scurta de
                                                spitalizare </a></h4>
                                    </div>
                                    <div id="collapse02" class="collapse" role="tabpanel" aria-labelledby="collapse02_header" data-parent="#accordion01">
                                        <div class="card-body" style="text-align:justify"> Majoritatea acestor intervenții nu necesită o perioadă de spitalizare lungă, nu se fac cu
                                            anestezie generală și nu determină dureri postoperatorii intense. Aceasta înseamnă că vă puteți relua activitățile dvs. normale imediat
                                            sau la câteva zile după operație.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="collapse03_header">
                                        <h4><a class="collapsed" data-toggle="collapse" href="#collapse03" aria-expanded="false" aria-controls="collapse03"> Mai aproape de
                                                dumneavoastra </a></h4>
                                    </div>
                                    <div id="collapse03" class="collapse" role="tabpanel" aria-labelledby="collapse03_header" data-parent="#accordion01">
                                        <div class="card-body" style="text-align:justify"> Efectuând consultații în mai multe orașe din regiunile de vest și sud-vest ale țării,
                                            Venim în întâmpinarea nevoilor dumneavoastră în specialitatea chirurgie vasculară, asigurându-vă un
                                            acces rapid și profesionist la consultații și tratament.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="divider-80 d-lg-block"></div>
                        </div>
                        <div class="col-lg-5 to-bottom-image"><img src="images/sistemul-vascular.jpg" alt="sistemul vascular"></div>
                        <div class="divider-80 d-lg-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="courses" class="ls animate" data-animation="fadeInUp">
        <div class="cc5">
            <div class="row">
                <div class="col-md-12">
                    <div class="courses-slider owl-carousel" data-autoplay="true" data-loop="true" data-responsive-lg="5" data-responsive-md="4" data-responsive-sm="2"
                         data-responsive-xs="2" data-nav="false" data-dots="false" data-margin="5">
                        <div class="courses-item ">
                            <div class="media courses">
                                <div><img src="images/boli/boala-arteriala-periferica-a-membrelor-inferioare.jpg" alt="boala arterială periferică a membrelor inferioare"></div>
                                <div class="media-body cs">
                                    <div class="courses-header">
                                        <div class="plan-name">
                                            <h4>Boala arterială periferică </h4>
                                        </div>
                                    </div>
                                    <p class="description-lessons fs-14 mt-48">Boala arterială periferică a membrelor inferioare (denumită și arteriopatie cronică obliterantă) este
                                        o problemă circulatorie frecventă ... </p>
                                    <div class="button-section"><a href="boala-arteriala-periferica-a-membrelor-inferioare.htm" class="btn btn-outline-maincolor">Citeste tot</a> <a
                                                href="contact.htm" class="btn btn-maincolor">Consultatie</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="courses-item ">
                            <div class="media courses">
                                <div><img src="images/boli/boala-carotidiana.jpg" alt="boala carotidiana"></div>
                                <div class="media-body cs">
                                    <div class="courses-header">
                                        <div class="plan-name">
                                            <h4>Boala carotidiană<br>
                                                <br>
                                            </h4>
                                        </div>
                                    </div>
                                    <p class="description-lessons fs-14 mt-48">Boala carotidiană apare atunci când depozitele de colesterol îngustează vasele care duc sânge
                                        creierului (arterele carotidei) ... </p>
                                    <div class="button-section"><a href="boala-carotidiana.htm" class="btn btn-outline-maincolor">Citeste tot</a> <a href="contact.htm"
                                                                                                                                                     class="btn btn-maincolor">Consultatie</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="courses-item ">
                            <div class="media courses">
                                <div><img src="images/boli/anevrismul-aortei.jpg" alt="Anevrismul aortei abdominale"></div>
                                <div class="media-body cs">
                                    <div class="courses-header">
                                        <div class="plan-name">
                                            <h4>Anevrismul aortei abdominale</h4>
                                        </div>
                                    </div>
                                    <p class="description-lessons fs-14 mt-48">Un anevrism aortic abdominal reprezintă o zonă dilatată (mărită în diametru) în partea inferioară a
                                        aortei, vasul sanguin major care furnizează sânge ...</p>
                                    <div class="button-section"><a href="anevrismul-aortei-abdominale.htm" class="btn btn-outline-maincolor">Citeste tot</a> <a href="contact.htm"
                                                                                                                                                                class="btn btn-maincolor">Consultatie</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="courses-item ">
                            <div class="media courses">
                                <div><img src="images/boli/ateroscleroza.jpg" alt="ateroscleroza"></div>
                                <div class="media-body cs">
                                    <div class="courses-header">
                                        <div class="plan-name">
                                            <h4>Ateroscleroza (arterioscleroza)<br>
                                                <br>
                                            </h4>
                                        </div>
                                    </div>
                                    <p class="description-lessons fs-14 mt-48">Ateroscleroza (sau arterioscleroza) apare atunci când vasele de sânge care transportă oxigenul și
                                        substanțele nutritive din inimă spre restul corpului (arterele) devin groase și rigide ...</p>
                                    <div class="button-section"><a href="ateroscleroza.htm" class="btn btn-outline-maincolor">Citeste tot</a> <a href="contact.htm"
                                                                                                                                                 class="btn btn-maincolor">Consultatie</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="courses-item ">
                            <div class="media courses">
                                <div><img src="images/boli/tromboza-venoasa-profunda.jpg" alt="tromboza venoasa profunda timisoara"></div>
                                <div class="media-body cs">
                                    <div class="courses-header">
                                        <div class="plan-name">
                                            <h4>Tromboza venoasă profundă</h4>
                                        </div>
                                    </div>
                                    <p class="description-lessons fs-14 mt-48">Tromboza venoasă profundă apare atunci când se formează un cheag de sânge (tromb) în una sau mai
                                        multe vene profunde din corp, de obicei în picioare ... </p>
                                    <div class="button-section"><a href="tromboza-venoasa-profunda.htm" class="btn btn-outline-maincolor">Citeste tot</a> <a href="contact.htm"
                                                                                                                                                             class="btn btn-maincolor">Consultație</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="blog" class="ls s-py-70 s-py-xl-111">
        <div class="container">
            <div class="row">
                <div class="divider-30 d-none d-xl-block"></div>
                <div class="col-md-12">
                    <div class="text-center mb-55">
                        <h3 class="oswald"><span class="color-main">ImPULS</span><br>
                            Info Pacienți</h3>
                        <p class="subtitle width-100 width-xl-60">Fiecare intervenție chirurgicală comportă avantaje, riscuri și posibile complicații. Vă puteți informa despre
                            acestea, cât și despre ceea ce trebuie să faceți când veți părăsi unitatea medicală, din materialele alăturate. </p>
                    </div>
                    <div class="row justify-content-center blog-featured-posts">
                        <div class="col-lg-5 col-md-6 animate" data-animation="fadeInUp">
                            <article class="content-padding post type-post status-publish format-standard has-post-thumbnaills box-shadow grey-bg">
                                <div class="item-media post-thumbnail"><img src="images/pacienti/informatii-preoperatorii.jpg"
                                                                            alt="medic informații preoperatorii chirurgie vasculara">
                                    <div class="media-links"><a class="abs-link" title="" href="#"></a></div>
                                </div>
                                <div class="item-content grey-bg">
                                    <header class="entry-header">
                                        <h4 class="entry-title plan-name oswald">Informații Preoperatorii</h4>
                                    </header>
                                    <p class="oswald bold">Aflați cum va decurge operația dumneavoastră, pas cu pas.</p>
                                    <div class="entry-content oswald" style="text-align:left;">
                                        <ul class="list-bordered">
                                            <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-aorto-iliaca.pdf" target="_blank">Angioplastia cu balon și
                                                    stentarea aorto-iliaca</a></li>
                                            <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-femoro-poplitee.pdf" target="_blank">Angioplastia cu balon și
                                                    stentarea femoro-poplitee</a></li>
                                            <li><a href="informatii-preoperatorii/angioplastia-cu-balon-si-stentarea-arterelor-gambiere.pdf" target="_blank">Angioplastia cu balon
                                                    și stentarea arterelor gambiere</a></li>
                                            <li><a href="informatii-preoperatorii/bypass-ul-aorto-femural.pdf" target="_blank">Bypass-ul aorto-femural</a></li>
                                            <li><a href="informatii-preoperatorii/bypass-ul-femuro-popliteu-gambier.pdf" target="_blank">Bypass-ul femuro-popliteu / gambier </a>
                                            </li>
                                            <li><a href="informatii-preoperatorii/endarterectomia-carotidiana.pdf" target="_blank">Endarterectomia carotidiană</a></li>
                                            <li><a href="informatii-preoperatorii/tratamentul-chirurgical-deschis-al-anevrismului-de-aorta-abdominala.pdf" target="_blank">Tratamentul
                                                    chirurgical deschis al anevrismului de aortă abdominală</a></li>
                                            <li><a href="informatii-preoperatorii/tratamentul-endovascular-al-anevrismului-de-aorta-abdominala.pdf" target="_blank">Tratamentul
                                                    endovascular al anevrismului de aortă abdominală</a></li>
                                            <li><a href="informatii-preoperatorii/tratamentul-chirurgical-al-varicelor.pdf" target="_blank">Tratamentul chirurgical al varicelor</a>
                                            </li>
                                            <li><a href="informatii-preoperatorii/scleroterapia-venelor-reticulare.pdf" target="_blank">Scleroterapia venelor reticulare</a></li>
                                            <li><a href="informatii-preoperatorii/accesul-vascular-pentru-hemodializa.pdf" target="_blank">Accesul vascular pentru hemodializă</a>
                                            </li>
                                            <li><a href="informatii-preoperatorii/accesul-vascular-pentru-chimioterapie.pdf" target="_blank">Accesul vascular pentru
                                                    chimioterapie</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="divider-60 d-block d-md-none d-xl-none"></div>
                        <div class="col-lg-5 col-md-6 animate" data-animation="fadeInUp">
                            <article class="content-padding post type-post status-publish format-standard has-post-thumbnaills grey-bg box-shadow">
                                <div class="item-media post-thumbnail"><img src="images/pacienti/informatii-postoperatorii.jpg"
                                                                            alt="medic Informații postoperatorii chirurgie vasculara">
                                    <div class="media-links"><a class="abs-link" title="" href=""></a></div>
                                </div>
                                <div class="item-content grey-bg">
                                    <header class="entry-header">
                                        <h4 class="entry-title plan-name oswald"> Informații Postoperatorii</h4>
                                    </header>
                                    <p class="oswald bold">Aflați ce trebuie să faceți după externarea din spital.</p>
                                    <div class="entry-content oswald">
                                        <ul class="list-bordered">
                                            <li><a href="instructiuni-postoperatorii/angioplastia-endovasculara.pdf" target="_blank">Angioplastia endovasculară</a></li>
                                            <li><a href="instructiuni-postoperatorii/bypass-ul-suprainghinal.pdf" target="_blank">Bypass-ul suprainghinal</a></li>
                                            <li><a href="instructiuni-postoperatorii/bypass-ul-infrainghinal.pdf" target="_blank">Bypass-ul infrainghinal</a></li>
                                            <li><a href="instructiuni-postoperatorii/endarterectomia-carotidiana.pdf" target="_blank">Endarterectomia carotidiană</a></li>
                                            <li><a href="instructiuni-postoperatorii/ablatia-varicelor.pdf" target="_blank">Ablația varicelor</a></li>
                                            <li><a href="instructiuni-postoperatorii/scleroterapia.pdf" target="_blank">Scleroterapia</a></li>
                                            <li><a href="instructiuni-postoperatorii/accesul-vascular-pentru-hemodializa.pdf" target="_blank">Accesul vascular pentru
                                                    hemodializă</a></li>
                                            <li><a href="instructiuni-postoperatorii/accesul-vascular-pentru-chimioterapie.pdf" target="_blank">Accesul vascular pentru
                                                    chimioterapie</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                <div class="divider-10 divider-lg-10 divider-xl-40"></div>
            </div>
        </div>
    </section>
    <section id="locatii" class="ls animate" data-animation="scaleAppear">
        <div class="cc5">
            <div class="cover-image s-cover-left"></div>
            <div class="container">
                <div class="row align-items-center c-gutter-60">
                    <div class="col-md-12 col-lg-6">
                        <div class="item-media">
                            <div class="embed-responsive"><a href="contact.htm"> <img src="images/harta.jpg" alt="harta locatii"> </a></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 date-contact">
                        <div class="divider-30 divider-md-70 divider-xl-75"></div>
                        <h3 class="mt-0 oswald"><span class="color-main">Unde ne găsiți? </span> <br>
                            ImPULS</h3>
                        <div class="divider-30 divider-md-70 divider-xl-75"></div>
                        <div class="row c-gutter-30">
                            <div class="col-md-12 col-lg-6">
                                <a href="" data-toggle="modal" data-target="#map1_modal">
                                    <div class="icon-box">
                                        <div class="media">

                                            <div class="icon-styled color-main fs-24"><i class="ico icon-location fs-40"></i></div>
                                            <div class="media-body">
                                                <h6 class="fw-300"><strong>Timisoara [ Harta ]</strong><br> Centrul Medical Tadlife</h6>
                                                <p>Strada Tosca, nr 30A,<br>telefon 0754.099.761<br>&nbsp;</p>
                                            </div>

                                        </div>
                                    </div>
                                </a>
                                <div class="divider-30 divider-lg-42"></div>
                                <a href="" data-toggle="modal" data-target="#map2_modal">
                                    <div class="icon-box">
                                        <div class="media">
                                            <div class="icon-styled color-main fs-24"><i class="ico icon-location fs-40"></i></div>
                                            <div class="media-body">
                                                <h6 class="fw-300"><strong>Arad [ Harta ]</strong><br> Clinica Medical Centrum</h6>
                                                <p>blv. Decebal, nr. 12,<br>telefon 0747.569.124</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                            <div class="divider-30 d-lg-none d-md-block"></div>
                            
                            <div class="col-md-12 col-lg-6">
                                <a href="" data-toggle="modal" data-target="#map4_modal">
                                    <div class="icon-box">
                                        <div class="media">
                                            <div class="icon-styled color-main fs-24"><i class="ico icon-location fs-40"></i></div>
                                            <div class="media-body">
                                                <h6 class="fw-300"><strong>Oradea [ Harta ]</strong><br> Clinica Bioinvest</h6>
                                                <p>strada Episcop Roman Ciorogariu, nr 62,<br>telefon 0259.412.295</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="divider-30 divider-lg-42"></div>
                                <a href="" data-toggle="modal" data-target="#map3_modal">
                                    <div class="icon-box">
                                        <div class="media">
                                            <div class="icon-styled color-main fs-24"><i class="ico icon-location fs-40"></i></div>
                                            <div class="media-body">
                                                <h6 class="fw-300"><strong>Deva [ Harta ]</strong><br> Derm Artis Clinic</h6>
                                                <p>Bvd. 22 Decembrie, Bloc 41, telefon 0774.495.143</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="divider-0 divider-md-0 divider-xl-75"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="divider-100 divider-md-0 divider-xl-0"></div>
    <section class="ls s-py-70 s-py-xl-141 animate" data-animation="fadeInUp" style="background:#f5f5f5"><a id="medici"></a>
        <div class="container">
            <div class="row c-gutter-135 mobile-padding-normal">
                <div class="col-md-12">
                    <div class="text-center mb-45">
                        <h3 class="oswald"><span class="color-main">medicii</span><br>
                            ImPULS</h3>
                        <p class="subtitle width-100 width-xl-60">Suntem aici pentru a vă asculta, pentru a înțelege cum boala dumneavoastră vă afectează viața și pentru a vă
                            îngriji folosind cele mai noi și eficiente tehnici chirurgicale</p>
                    </div>
                    <div class="row">


                        <div class="divider-20 divider-xl-80"></div>

                        <div class="offset-md-3 col-md-6 plr150 mobile-padding-normal text-center">

                            <div class="box-shadow content-padding text-center team">
                                <div class="item-media">
                                    <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. Pătruț George">
                                </div>

                                <!--
                                <div class="item-content">
                                    <h5 class="mb-0 oswald">
                                        Dr. George Pătruț
                                    </h5>

                                    <p class="color-main font-weight-normal oswald">
                                        Chirurg Vascular
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </p>



                                </div>
                                !-->

                            </div>
                            <h4 class="color-main teamh4 mt-20 fw-500 oswald mb-10">Dr. George Pătruț</h4>
                            <h6 class="color-dark mt-0 oswald font-weight-normal">Chirurg Vascular</h6>
                        </div>

                    </div>
                </div>
                <div class="divider-5 d-lg-block d-xl-5"></div>
            </div>
        </div>
    </section>
    <section id="information-block" class="ds s-pt-xl-90 s-pb-xl-94 s-pt-60 s-pb-60">
        <div class="container">
            <div class="row c-gutter-50">
                <div class="divider-10 divider-lg-10 divider-xl-5"></div>
                <div class="col-md-9 col-lg-6 col-sm-12 animate" data-animation="fadeInUp">
                    <h3 class="after-title oswald">Doriți <span>o consultație?</span></h3>
                    <p class="subtitle">Suntem aici pentru a vă asculta, pentru a înțelege cum boala dumneavoastră vă afectează viața și pentru a vă îngriji folosind cele mai noi
                        și eficiente tehnici chirurgicale. </p>
                    <div class="mt-45">
                        <a href="contact.htm" class="btn btn-outline-darkgrey small fw-400">Programează-te online</a>
                    </div>
                    <div class="divider-20 divider-lg-20 divider-xl-5"></div>
                </div>
            </div>
        </div>
    </section>
    <?php include("footer.php") ?>
</div>
</div>
<script src="js/compressed.js"></script>
<script src="js/main.min.js"></script>
</body>
</html>