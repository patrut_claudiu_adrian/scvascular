<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Boala carotidiană | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b1.3.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>


Boala carotidiană apare atunci când depozitele de colesterol (plăcile) îngustează vasele care duc sânge creierului (arterele carotidei). Blocajul crește riscul de accident vascular cerebral, o urgență medicală care apare atunci când alimentarea cu sânge a creierului este întreruptă sau redusă în mod serios. Accidentul vascular cerebral privează creierul de oxigenul necesar. În câteva minute celulele creierului încep să moară.

								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                            <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. George Patrut">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. George Pătruț</h5>
                                                        <p class="bl-dr">
                                                            “Accidentul vascular cerebral este principala cauză de dizabilități permanente în Europa.”
                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                       <img src="images/bolile-arterelor/boala-carotidiana.jpg" alt="carotide">
                                    </div>

                                    <div class="entry-content">

                                        <p>Boala arterei carotide este cauzată de acumularea de plăci de aterom în arterele care duc sânge în creier. Plăcile de aterom sunt aglomerări de colesterol, calciu, țesut fibros și alte resturi celulare care se acumulează în pereții arterei. Acest proces se numește ateroscleroză.</p><p>

Arterele carotide care sunt afectate de ateroscleroză devin rigide și îngustate. Arterele carotide îngustate au dificultăți în a furniza oxigen și substanțe nutritive structurilor vitale din  creier care sunt responsabile pentru funcționarea zilnică a organismului.

                                        </p>
                                      

                                        

                                        <h4 class="oswald">Factorii de risc</h4>
                                        <ul class="list-styled">
										<li>Fumatul</li>
<li>Diabetul zaharat</li>
<li>Obezitatea (un indice de masă corporală de peste 30)</li>
<li>Tensiune arterială crescută</li>
<li>Colesterolul ridicat</li>
<li>Înaintarea în vârstă, mai ales după 50 de ani</li>
<li>Istoric familial de boală arterială periferică, boli cardiace sau accident vascular cerebral</li>
                                        </ul>

                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-arterelor/AVC.jpg" alt="avc">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Simptome</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>În stadiile incipiente, boala arterei carotide nu produce deseori semne sau simptome. Maladia poate trece neobservată până când este suficient de avansată pentru a vă lipsi creierul de sânge, determinând:

</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														









                                                            <div><i class="fa fa-check-circle"></i> Amorțeală sau slăbiciune instalată brusc la nivelul feței sau membrelor, de multe ori pe o singură parte a corpului</div>
                                                            <div><i class="fa fa-check-circle"></i> Dificultăți bruște de vorbire și înțelegere </div>
                                                            <div><i class="fa fa-check-circle"></i> Tulburări de vedere la unul sau ambii ochi, instalate brusc </div>
															<div><i class="fa fa-check-circle"></i> Amețeli sau pierderea echilibrului </div>
															<div><i class="fa fa-check-circle"></i> Durere de cap bruscă, severă, fără cauză cunoscută  </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse02" class="collapsed">
                                                            <li>Diagnosticarea bolii</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Medicul vă va întreba despre istoricul dumneavoastră medical și vă va face o examinare fizică amănunțită. Examenul fizic include, în general, căutarea unui sunet ca un foșnet (suflu) pe artera carotidă la nivelul gâtului, un sunet care este caracteristic unei artere îngustate. După aceea, medicul poate recomanda:

                                                               
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">





<div><i class="fa fa-check-circle"></i> <strong>Ecografia doppler,</strong> pentru a evalua fluxul sanguin și presiunea în arterele carotide;</div>
<div><i class="fa fa-check-circle"></i> <strong>Tomografia computerizată (CT) sau rezonanța magnetică nucleară (RMN),</strong> pentru a căuta dovezi de accident vascular cerebral sau alte anomalii;</div>
<div><i class="fa fa-check-circle"></i> <strong>Angiografia CT sau angiografia RMN,</strong> care oferă imagini suplimentare ale fluxului sanguin în arterele carotide. Un colorant de contrast este injectat într-o venă, iar o scanare CT sau RMN colectează imagini ale gâtului și creierului.</div>

                                                        
                                                           
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Complicații</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Boala carotidiană provoacă aproximativ 10-20% din accidentele vasculare cerebrale. Un accident vascular cerebral este o urgență medicală care se poate solda cu leziuni cerebrale permanente și infirmitate. În cazurile severe, un accident vascular cerebral poate fi fatal.</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                     
<p>Boala carotidiană poate determina un accident vascular cerebral prin:</p>

                                                                <div><i class="fa fa-check-circle"></i>  Scăderea fluxului sanguin. O artera carotidă poate deveni atât de îngustată prin ateroscleroză, încât nu ajunge suficient sânge în anumite zone ale creierului;

</div>
                                                                <div><i class="fa fa-check-circle"></i>  Ruptura plăcilor de aterom. O bucată din placă se poate rupe și poate fi transportată de sânge în arterele mai mici din creier. Fragmentul plăcii poate rămâne blocat în una din aceste artere mai mici, întrerupând alimentarea cu sânge a unei părți din creier;

</div>
                                                                <div><i class="fa fa-check-circle"></i>  Formarea unui cheag de sânge în arteră. Unele plăci sunt predispuse la fisurare. Corpul dumneavoastră reacționează ca la o tăietură a pielii și trimite celulele sanguine care declanșează procesul de coagulare în zona. Rezultatul poate fi un cheag foarte mare care blochează sau încetinește fluxul de sânge către creier, provocând un accident vascular cerebral.

</div>
                                                              

                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0 mt-20">
                                                                <img src="images/bolile-arterelor/paralizia.jpg" alt="paralizia creier">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratament</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Scopul tratamentului bolii arterei carotide este de a preveni accidentul vascular cerebral. Tratamente specifice sunt recomandate în funcție de gradul de blocaj al arterelor carotide.

                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
														<p>Dacă blocajele sunt ușoare până la moderate, medicul poate recomanda:</p>

                                                           
															
                                                        <div><i class="fa fa-check-circle"></i> <strong>Modificarea stilului de viață</strong> pentru a încetini progresia aterosclerozei. Recomandările pot include renunțarea la fumat, scăderea în greutate, consumul de alimente sănătoase, reducerea sării din alimentație și exercițiul fizic practicat în mod regulat;
</div>
                                                        <div><i class="fa fa-check-circle"></i> <strong>Medicamente pentru controlul tensiunii arteriale sau scăderea colesterolului.</strong> Medicul vă poate recomanda, de asemenea, administrarea unei doze zilnice de <strong>aspirină</strong> sau a altor medicamente pentru subțierea sângelui, cu scopul de a preveni formarea cheagurilor de sânge.
</div>


<p>Dacă blocajul este sever sau dacă ați avut deja un accident ischemic tranzitor sau un accident vascular cerebral, medicul vă poate recomanda eliminarea blocajului din arteră. Opțiunile includ:</p>

													
															
                                                        <div><i class="fa fa-check-circle"></i> <strong>Endarterectomia carotidiană,</strong> tratamentul cel mai frecvent pentru boala carotidiană severă. După ce face o incizie pe partea din față a gâtului, chirurgul deschide artera carotidă afectată și îndepărtează plăcile. 

</div>
                                                        <div><i class="fa fa-check-circle"></i> <strong>Angioplastia carotidiană cu balon și stentarea,</strong> în cazul în care blocajul este prea dificil pentru o endarterectomie clasică sau aveți alte boli care fac chirurgia prea riscantă. Un balon mic atașat de un cateter este avansat până în zona de îngustare. Balonul este umflat pentru a lărgi artera și este introdus un tub metalic (stent) pentru a o menține deschisă.

</div>

														
                                                           
                                                           



                                                           <div class="item-media post-thumbnail alignwide bd-t mt-20">
                                                                <img src="images/bolile-arterelor/interventie.jpg" alt="interventie">
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>boala carotidiană apare atunci când depozitele de colesterol îngustează vasele care duc sânge creierului, crescând riscul de accident vascular cerebral

</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>accidentul vascular cerebral este o urgență medicală care se poate solda cu leziuni cerebrale permanente și infirmitate, deseori fiind fatal 
</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>scopul tratamentului bolii arterei carotide este de a preveni accidentul vascular cerebral prin îndepărtarea plăcilor de aterom 

</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>tratamentul cel mai frecvent utilizat este endarterectomia carotidiană; angioplastia cu balon se indică doar în situații speciale.

</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>