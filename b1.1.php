<section class="page_title about padding-mobile cs s-pt-50 s-pb-40 cover-background2">
					<div class="container">
						<div class="row">


							<div class="col-md-6 text-left animate head-title" data-animation="fadeInUp">
							<div class="col-xl-9 col-md-12 pl-0 text-uppercase"><p>ImPULS</p></div>
								<div class="col-xl-8 col-md-12 pl-0"> <h1 class="text-uppercase">Anevrismul aortei abdominale</h1> </div>
								<div class="col-xl-9 col-md-12 pl-0"><p>Deoarece aorta este principalul vas de sânge al organismului, un anevrism aortic abdominal rupt poate provoca sângerări care pot pune viața în pericol.</p></div>
							</div>

<div class="col-md-6 text-center animate d-none d-xl-block" data-animation="fadeInUp">
								<div class="head-elements row">

								<div class="col-lg-3">
								<a href="ateroscleroza.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Ateroscleroza</h6>

									</div>
									</a>
								</div>

								<div class="col-lg-3">
								<a href="boala-carotidiana.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Boala carotidiană</h6>

									</div>
									</a>
								</div>

								<div class="col-lg-3">
								<a href="boala-arteriala-periferica-a-membrelor-inferioare.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Boala arterială periferică</h6>

									</div>
								</div>
								<div class="col-lg-3">
								<a href="">
									<div class="icon-box text-centerbox-shadow">

										

									</div>
									</a>
								</div>


							</div>
							<div class="head-elements row">

								<div class="col-lg-3">
								<a href="">
									<div class="icon-box text-center box-shadow">

										

									</div>
									</a>
								</div>

								<div class="col-lg-3">
								<a href="ischemia-critica-a-membrelor-inferioare.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Ischemia critică</h6>

									</div>
								</div>

								<div class="col-lg-3">
								<a href="anevrismul-aortei-abdominale.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Anevrismul aortei</h6>

									</div>
									</a>
								</div>
								<div class="col-lg-3">
								<a href="boala-buerger-trombangeita-obliteranta.htm">
									<div class="icon-box text-center hero-bg1 box-shadow">

										<h6 class="fw-300">Trombangeita obliterantă (boala Buerger)
 </h6>

									</div>
									</a>
								</div>


							</div>
							</div>
						</div>
					</div>
				</section>