<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>Anevrismul aortei abdominale | ImPULS</title>
    <meta charset="utf-8">
    <!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php include("css.php") ?>
        <style>
            ::placeholder {
                color: #fff !important;
                opacity: 1;
            }
            
            :-ms-input-placeholder {
                color: #fff !important;
            }
            
            ::-ms-input-placeholder {
                color: #fff !important;
            }
        </style>
</head>

<body>
    <!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

    <?php include("modal.php") ?>

        <div id="canvas">
            <div id="box_wrapper">
                <?php include("meniu.php") ?>
            </div>

            <?php include("b1.1.php") ?>
                <section style="background:#f9f9f9" class="pt-80">
                    <div class="container">
                        <div class="col-lg-12">
                            <header class="entry-header single-post pb-50">
                                <div class="row">

                                    <div class="col-md-6">

                                        <span>

Un anevrism aortic abdominal reprezintă o zonă dilatată (mărită în diametru) în partea inferioară (abdominală) a aortei, vasul sanguin major care furnizează sânge întregului organism. În funcție de mărimea și rata de creștere ale anevrismului, tratamentul poate varia de la supraveghere atentă până la intervenție chirurgicală de urgență.

								</span>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">

                                            <div class="col-12">
                                                <div class="row c-gutter-50">

                                                    <div class="col-md-4">
                                                        <div>
                                                            <img src="images/echipa/doctor-george-patrut.jpg" alt="Dr. George Patrut">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <div class="divider-30 divider-md-0 divider-xl-0"></div>
                                                        <h5 class="mt-0 divider-top-bottom1 oswald">Dr. George Pătruț</h5>
                                                        <p class="bl-dr">
                                                            “Deoarece aorta este principalul vas de sânge al organismului, un anevrism aortic abdominal rupt poate provoca sângerări care pot pune viața în pericol.”
                                                        </p>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </header>
                        </div>
                    </div>
                </section>
                <section class="ls s-py-50 c-gutter-60">
                    <div class="container">

                        <div class="divider-80 divider-xl-70"></div>
                        <main class="offset-lg-2 col-lg-8">
                            <article class="vertical-item post type-post status-publish format-standard has-post-thumbnail">

                                <div class="item-content">

                                    <div class="text-center mb-20">
                                        <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">Cauzele bolii</h4>
                                    </div>
                                    <div class="item-media post-thumbnail alignwide bd-t mb-80">
                                        <img src="images/bolile-arterelor/anevrismul-aortei5.jpg" alt="anevrismul aortei abdominale">
                                    </div>

                                    <div class="entry-content">

                                        <p>Cele mai multe anevrisme aortice apar în porțiunea aortei care se află în abdomen. Deși cauzele exacte ale acestor dilatații sunt necunoscute, un număr de factori pot juca un rol:
                                        </p>
                                        <ul class="list-styled">
                                            <li><strong>Ateroscleroza</strong> apare când grăsimile și alte substanțe se acumulează în pereții unui vas de sânge, slăbindu-le rezistența și permițând dilatarea anevrismală</li>
                                            <li><strong>Hipertensiunea arterială</strong> poate mări riscul de anevrism aortic abdominal, deoarece pereții trebuie să suporte o presiune crescută a sângelui</li>
                                            <li><strong>Bolile inflamatorii ale vaselor</strong> de sânge slăbesc structura pereților aortei</li>
                                            <li><strong>Infecțiile pe traiectul aortei</strong>, bacteriene sau fungice, pot provoca rar anevrisme</li>
                                            <li><strong>Traumatismele</strong>, cum ar fi un accident de mașină, pot leza pereții și predispune la formarea unui anevrism</li>
                                            <li><strong>Ereditatea</strong> - în unele cazuri, anevrismele aortice abdominale ar putea fi ereditare</li>

                                        </ul>

                                        <h4 class="oswald">Factorii de risc</h4>

                                        <ul class="list-styled">
                                            <li><strong>Vârsta.</strong> Anevrismele aortice abdominale apar cel mai frecvent la persoanele în vârstă de 65 de ani și peste;</li>
                                            <li><strong>Fumatul</strong> este un factor de risc major pentru dezvoltarea aterosclerozei și crește puternic riscul de ruptură al pereților aortei</li>
                                            <li><strong>Sexul masculin.</strong> Bărbații dezvolta anevrisme abdominale aortice mult mai des decât femeile.</li>
                                            <li><strong>Istoricul familial.</strong> Persoanele cu istoric familial de anevrisme aortice abdominale prezintă un risc crescut de a avea această afecțiune;</li>
                                            <li><strong>Alte anevrisme.</strong> Persoanele care au un anevrism localizat într-un alt vas de sânge mare, cum ar fi artera din spatele genunchiului sau porțiunea aortei din piept, au un risc mai mare de a dezvolta un anevrism aortic abdominal.
                                            </li>
                                        </ul>

                                        <div class="text-center mb-50 mt-50">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">ASPECTE IMPORTANTE DESPRE BOALĂ</h4>
                                        </div>

                                        <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                            <img src="images/bolile-arterelor/durere-abdominala.jpg" alt="femeie durere abdominala.jpg">
                                        </div>
                                        <ol>
                                            <div role="tablist">
                                                <div class="card">
                                                    <div role="tab">
                                                        <a data-toggle="collapse" href="#collapse01" class="collapsed">
                                                            <li>Simptome</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Anevrismele aortice abdominale cresc adesea încet și, de obicei, fără simptome, făcându-le dificil de detectat. Unele anevrisme nu se vor rupe niciodată. Multe sunt mici și rămân mici, altele se extind rapid. </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse01" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                            <p>Când un anevrism aortic abdominal crește în dimensiuni, unii pacienți pot observa:</p>
                                                            <div><i class="fa fa-check-circle"></i> O senzație de pulsație în zona ombilicului (buricului)</div>
                                                            <div><i class="fa fa-check-circle"></i> Durere constantă și profundă în abdomen sau pe lateralele abdomenului</div>
                                                            <div><i class="fa fa-check-circle"></i> Dureri de spate, în zona lombară</div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse01">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse02" class="collapsed">
                                                            <li>Diagnosticarea bolii</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Anevrismele aortice abdominale se descoperă adesea în timpul unui consult pentru un alt motiv. De exemplu, în timpul unui examen de rutină, medicul poate simți o masă pulsantă în abdomen. Anevrismele aortice sunt de asemenea adesea descoperite în timpul testelor medicale de rutină, cum ar fi radiografia toracică sau ecografia inimii sau abdomenului, deseori făcute pentru un alt motiv.
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse02" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                            <p>Dacă medicul suspectează un anevrism aortic, testele specializate îl pot confirma:

                                                                <div><i class="fa fa-check-circle"></i> <strong>Ecografia abdominală</strong>, testul cel mai frecvent utilizat pentru diagnosticarea anevrismelor aortice abdominale, este nedureroasă, neinvazivă, poate măsura diametrul dilatației, dar nu poate preciza cu exactitate cât de sus și de jos se întinde aceasta</div>
                                                                <div><i class="fa fa-check-circle"></i> <strong>Tomografia computerizată (CT) și rezonanța magnetică nucleară (RMN)</strong> folosesc un colorant special injectat în sânge pentru a vizualiza exact atât diametrul dilatației cât și extinderea sa superioară și inferioară</div>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse02">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse03" class="collapsed">
                                                            <li>Complicații</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Ruperea unuia sau mai multora dintre straturile peretelui aortei (disecție aortica) sau ruperea întregului perete (anevrism rupt) sunt principalele complicații ale anevrism aortic abdominal. Un anevrism aortic rupt poate duce la sângerări interne amenințătoare de viață. În general, cu cât este mai mare anevrismul și cu cât crește mai repede în dimensiuni, cu atât este mai mare riscul de rupere.</p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse03" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                            <p>Semnele și simptomele unui anevrism aortic rupt pot include:

                                                                <div><i class="fa fa-check-circle"></i> Durerea bruscă, intensă și persistentă a abdomenului sau a spatelui, care poate fi descrisă ca o senzație de rupere;</div>
                                                                <div><i class="fa fa-check-circle"></i> Durerea care radiază în spate sau picioare;</div>
                                                                <div><i class="fa fa-check-circle"></i> Transpirații intense;</div>
                                                                <div><i class="fa fa-check-circle"></i> Amețeală;</div>
                                                                <div><i class="fa fa-check-circle"></i> Greață;</div>
                                                                <div><i class="fa fa-check-circle"></i> Vărsături;</div>
                                                                <div><i class="fa fa-check-circle"></i> Scăderea tensiunii arteriale;</div>
                                                                <div><i class="fa fa-check-circle"></i> Puls rapid.</div>
                                                            </p>
                                                            <p>O altă complicație a anevrismelor aortice este riscul de formare a cheagurilor de sânge în zona dilatată (sacul anevrismal). În cazul în care un astfel de cheag de sânge se desprinde, este purtat de fluxul de sânge și blochează un alt vas în altă parte din corp, putând provoca dureri și ischemie la nivelul picioarelor, degetelor de la picioare, rinichilor sau organelor abdominale.</p>
                                                            <div class="item-media post-thumbnail alignwide bd-t mb-0">
                                                                <img src="images/bolile-arterelor/anevrismul-aortei-abdominale.jpg" alt="anevrism rupt">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="read-a" data-toggle="collapse" href="#collapse03">[ Citeste tot ]</li></a>
                                                </div>

                                                <div class="card">
                                                    <div role="tab">

                                                        <a data-toggle="collapse" href="#collapse04" class="collapsed">
                                                            <li>Tratament</li>
                                                        </a>

                                                        <div class="card-body">
                                                            <p>Scopul tratamentului este prevenirea rupturii anevrismului. În general, opțiunile de tratament sunt monitorizarea medicală sau intervenția chirurgicală. Decizia medicului depinde de mărimea anevrismului aortic și de cât de repede crește în dimensiuni.
                                                            </p>
                                                        </div>

                                                    </div>

                                                    <div id="collapse04" class="collapse" role="tabpanel">
                                                        <div class="card-body">
                                                            <h4 class="oswald">Monitorizarea medicală</h4>
                                                            <p>Dacă anevrismul este mic și nu produce simptome, medicul poate recomanda monitorizarea medicală, care include consultații periodice și tratarea altor afecțiuni medicale care ar putea accelera dilatarea aortei. Este posibil ca medicul să efectueze teste de imagistică periodic pentru a verifica dimensiunea anevrismului. Frecvența testelor imagistice depinde de mărimea acestuia, dacă crește și cât de repede crește în dimensiuni.
                                                            </p>
                                                            <h4 class="oswald">Intervenții chirurgicale</h4>
                                                            <p>Intervenția chirurgicală este în general recomandată dacă anevrismul este de 5 cm sau mai mare. Medicii pot recomanda și o intervenție chirurgicală dacă anevrismul crește rapid, sau produce simptome, cum ar fi dureri abdominale.

                                                                <div><i class="fa fa-check-circle"></i> <strong>Chirurgia abdominală deschisă</strong> implică înlăturarea secțiunii dilatate a aortei și înlocuirea acesteia cu un tub sintetic (grefon)</div>
                                                                <div><i class="fa fa-check-circle"></i> <strong>Chirurgia endovasculară</strong> este o procedură mai puțin invazivă și presupune introducerea în anevrism a unui tub din material sintetic întărit de un schelet metalic - endogrefă. Endogrefa întărește secțiunea slăbită a aortei pentru a preveni ruptura anevrismului. Timpul de recuperare este în general mult mai scurt cu această procedură decât cu intervenția chirurgicală abdominală deschisă, dar chirurgia endovasculară nu poate fi făcută la aproximativ 30% dintre persoanele cu anevrism. </div>

                                                            </p>
                                                        </div>
														     <div class="item-media post-thumbnail alignwide bd-t mt-20">
                                                                <img src="images/bolile-arterelor/chirurgia-endovasculara.jpg" alt="chirurgia endovasculara">
                                                            </div> 
                                                    </div>
													
                                                    <a class="read-a" data-toggle="collapse" href="#collapse04">[ Citeste tot ]</li></a>
                                                </div>

                                            </div>

                                        </ol>

                                    </div>

                                </div>

                            </article>

                        </main>

                        <div class="divider-80 divider-xl-70"></div>
						  <div class="text-center mb-10 mt-100">
                                            <h4 class="special-heading text-center oswald" style="text-transform:uppercase; font-size:35px;">SUMAR</h4>
                                        </div>
						<div class="row sumar">
								<div class="col-md-3 col-sm-12">
									<h1 class="color-main fw-500"><i class="ico icon-circle fs-8"></i>01</h1>
									<p>un anevrism aortic abdominal reprezintă o zonă dilatată (mărită în diametru) în partea inferioară a aortei, vasul sanguin major care furnizează sânge întregului organism</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>02</h1>
									<p>deși când sunt mici nu produc simptome, creșterea în dimensiuni determină dureri în abdomen și în spate; ruptura anevrismului reprezintă o complicație majoră, cu potențial letal </p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>03</h1>
									<p>diagnosticarea inițială și monitorizarea se fac prin ecografie abdominală; pentru tratamentul chirurgical este necesară examinarea prin computer tomograf (CT)
</p>
								</div>
								<div class="col-md-3 col-sm-12">
									<div class="divider-30 divider-md-0 divider-xl-0"></div>
									<h1 class="color-main fw-500 mt-0"><i class="ico icon-circle fs-8"></i>04</h1>
									<p>intervenția chirurgicală este recomandată dacă anevrismul este de 5 cm sau mai mare, crește rapid, sau produce simptome; anevrismul poate fi tratat atât prin chirurgie deschisă cât și endovascular</p>
								</div>
							</div>
                    </div>

        </div>
		
        </section>

        <?php include("bottom-consult.php") ?>
            <?php include("footer.php") ?>
                </div>
                </div>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
                <script src="js/compressed.js"></script>
                <script src="js/main.min.js"></script>
</body>

</html>