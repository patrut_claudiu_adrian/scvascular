<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<title>ImPULS</title>
<meta charset="utf-8">
<!--[if IE]>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php include("css.php") ?>
<style>
::placeholder {
  color: #fff !important;
  opacity: 1;
}

:-ms-input-placeholder {
  color: #fff !important;
}

::-ms-input-placeholder {
  color: #fff !important;
}
</style>
</head>

<body>
<!--[if lt IE 9]>
		<div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a class="color-main">upgrade your browser</a> to improve your experience.</div>
	<![endif]-->

<?php include("modal.php") ?>

<div id="canvas">
  <div id="box_wrapper"> 
    <?php include("meniu.php") ?>
  </div>



			<section class="page_title about padding-mobile cs s-pt-100 s-pb-94 cover-background2">
					<div class="container">
						<div class="row">


							<div class="col-md-12 text-center animate" data-animation="fadeInUp">
								<h2 class="pb-0">Resurse pentru pacienti</h2>
								<p>Bolile arterelor</p>
							</div>


						</div>
					</div>
				</section>

			<section class="ls s-py-20" style="background:#f5f5f5">
				<div class="container">
					<div class="divider-80 divider-xl-70"></div>

					<div class="row c-mb-30">
						<div class="col-lg-12 blog-featured-posts">
							<div class="row justify-content-center">
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/01.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Boala arterială periferică
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Boala arterială periferică (denumită și arteriopatie cronică obliterantă) este o problemă circulatorie frecventă în care arterele îngustate reduc fluxul de sânge către membre ... </p>
											</div>
								<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>

										</div>
									</article>
								</div>
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/02.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Boala carotidiană
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Boala carotidiană apare atunci când depozitele de colesterol (plăcile) îngustează sau înfundă vasele de sânge care duc sânge creierului și capului (arterele carotidei) ...</p>
											</div>
										
											<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>
										</div>
									</article>

								</div>
								<div class="col-xl-4 col-md-6">

									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="anevrismul-aortei-abdominale.htm">
												<img src="images/gallery/03.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="anevrismul-aortei-abdominale.htm">
														Anevrismul aortei abdominale
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Un anevrism aortic abdominal reprezintă o zonă dilatată (mărită în diametru) în partea inferioară (abdominală) a aortei, vasul sanguin major care furnizează sânge întregului organism ... </p>
											</div>
										
											<div class="entry-meta">
												
												<a href="anevrismul-aortei-abdominale.htm" class="btn btn-secondary">Citest mai departe</a>
											</div>
										</div>
									</article>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/04.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Ateroscleroza
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Ateroscleroza (sau arterioscleroza) apare atunci când vasele de sânge care transportă oxigenul și substanțele nutritive din inimă spre restul corpului (arterele) devin groase și rigide ...</p>
											</div>
								
											<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>
										</div>
									</article>
								</div>
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/05.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Boala Buerger - trombangeita obliterantă
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Boala Buerger este o boală rară a arterelor și a venelor mici și medii. În boala Buerger - numită și tromboangeită obliterantă - vasele de sânge se inflamează ...</p>
											</div>
										
											<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>
										</div>
									</article>

								</div>
								<div class="col-xl-4 col-md-6">

									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/03.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Sindromul compartimentului cronic de efort
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Sindromul compartimentului cronic de efort reprezintă o patologie a mușchilor și nervilor membrelor, indusă de efortul fizic, care provoacă durere ...</p>
											</div>
										
											<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>
										</div>
									</article>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/01.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Tromboza venoasă profundă
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>Tromboza venoasă profundă apare atunci când se formează un cheag de sânge (tromb) în una sau mai multe vene profunde din corp, de obicei în picioare ...</p>
											</div>
								
											<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>
										</div>
									</article>
								</div>
								<div class="col-xl-4 col-md-6">
									<article class="vertical-item text-center content-padding padding-small post type-post status-publish format-standard has-post-thumbnail animate" data-animation="fadeInUp">
										<div class="item-media post-thumbnail">
											<a href="#">
												<img src="images/gallery/02.jpg" alt="">
											</a>
										</div>
										<div class="item-content">
											<header class="entry-header">
												<h4 class="entry-title">
													<a href="#">
														Disecția aortei
													</a>
												</h4>


											</header>
											

											<div class="entry-content">
												<p>O disecție a aortei este o afecțiune foarte gravă în care stratul interior al aortei, vasul de sânge cel mai mare din organism ...</p>
											</div>
										
											<div class="entry-meta">
												
												<a href="#" class="btn btn-secondary">Citest mai departe</a>
											</div>
									</article>

								</div>
								
							</div>
						</div>
					</div>

					

				</div>
			</section>
  
  
  
  
  <?php include("footer.php") ?>
</div>
<!-- eof #box_wrapper -->
</div>
<!-- eof #canvas --> 
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?callback=templateInitGoogleMap&amp;key=AIzaSyC0pr5xCHpaTGv12l73IExOHDJisBP2FK4"></script>
<script src="js/compressed.js"></script> 
<script src="js/main.min.js"></script>
</body>
</html>